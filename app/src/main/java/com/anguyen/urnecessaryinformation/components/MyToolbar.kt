package com.anguyen.urnecessaryinformation.components

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.WindowInsetsCompat
import com.anguyen.urnecessaryinformation.commons.insetListener
import com.anguyen.urnecessaryinformation.commons.rootView
import com.google.android.material.appbar.MaterialToolbar

class MyToolbar(context: Context, attrs: AttributeSet): MaterialToolbar(context, attrs) {
    lateinit var mCoor: View
//    init {
//        val coorLayout = (context as Activity).findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
//        apply {
//            viewTreeObserver.addOnGlobalLayoutListener {
//                if (coorLayout is CoordinatorLayout) {
//                    coorLayout.insetListener { _, windowInsets ->
//                        val newHeight = this.height + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                        this.layoutParams = AppBarLayout.LayoutParams(
//                            AppBarLayout.LayoutParams.MATCH_PARENT,
//                            newHeight
//                        )
//                    }
//                }
//            }
//
//            onNavigationClick { (context as ComponentActivity).backPressed() }
//        }
//    }

//    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
//        mCoor = (context as Activity).findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
//        super.onLayout(changed, left, top, right, bottom)
//    }

//    override fun addOnLayoutChangeListener(listener: OnLayoutChangeListener?) {
//        if (mCoor is CoordinatorLayout) {
//            mCoor.insetListener { _, windowInsets ->
//                val newHeight = measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                layoutParams = LayoutParams(measuredWidth, newHeight)
//            }
//            //return
//        }
//        super.addOnLayoutChangeListener(listener)
//    }

//    override fun addView(child: View?) {
//        mCoor = child?.parent as View
//        if (mCoor is CoordinatorLayout) {
//            mCoor.insetListener { _, windowInsets ->
//                val newHeight = measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                layoutParams = Toolbar.LayoutParams(measuredWidth, newHeight)
//            }
//            //return
//        }
//        super.addView(child)
////    }
//
//    override fun onAttachedToWindow() {
//        super.onAttachedToWindow()
//        mCoor = (context as Activity).findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
//        //mCoor = child?.parent?.parent as View
//        if (mCoor is CoordinatorLayout) {
//            mCoor.insetListener { _, windowInsets ->
//                val params = layoutParams
//                params.height = layoutParams.height + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                layoutParams = params
//            }
//            //return
//        }
//        //layoutParams = Toolbar.LayoutParams(Toolbar.LayoutParams.MATCH_PARENT, )
//    }

    override fun onViewAdded(child: View?) {
        //mCoor = (context as Activity).findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
        mCoor = child?.rootView?.rootView!!
        if (mCoor is CoordinatorLayout) {
            mCoor.insetListener { _, windowInsets ->
                layoutParams.height = measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
                //layoutParams = Toolbar.LayoutParams(measuredWidth, newHeight)
            }
            //return
        }
        super.onViewAdded(child)
    }
}