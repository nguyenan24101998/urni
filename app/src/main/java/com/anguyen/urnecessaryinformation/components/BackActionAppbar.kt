package com.anguyen.urnecessaryinformation.components

import android.app.Activity
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.get
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.backPressed
import com.anguyen.urnecessaryinformation.commons.getColorById
import com.anguyen.urnecessaryinformation.commons.insetListener
import com.anguyen.urnecessaryinformation.commons.onNavigationClick
import com.anguyen.urnecessaryinformation.commons.rootView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar

class BackActionAppbar(context: Context, attrs: AttributeSet): AppBarLayout(context, attrs){
    private var mAppbar: AppBarLayout
    private var mToolbar: MaterialToolbar?

    var title: String? = ""
        get() { return mToolbar?.subtitle.toString()}
        set(value) {
            field = value
            mToolbar?.subtitle = value

            invalidate()
            requestLayout()
        }

    init {
        inflate(context, R.layout.custom_appbar_merge_toobar, this)

        mAppbar = findViewById(R.id.field_set_appbar)
        mToolbar = findViewById(R.id.field_set_toolbar)

        context.obtainStyledAttributes(attrs, R.styleable.BackActionAppbar, 0, 0).apply {
            try {
                //title = getString(R.styleable.BackActionAppbar_title)

                mAppbar.setBackgroundColor(getColor(R.styleable.BackActionAppbar_backgroundColor,
                    context.getColorById(R.color.colorAccent)))

                mToolbar?.apply {
                    //layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, mAppbar.height)
                    subtitle = getString(R.styleable.BackActionAppbar_title)

                    setSubtitleTextColor(getColor(R.styleable.BackActionAppbar_titleColor,
                        context.getColorById(R.color.colorWhite)))

                    setBackgroundColor(getColor(R.styleable.BackActionAppbar_backgroundColor,
                        context.getColorById(R.color.colorAccent)))
                }
            } finally {
                recycle()
            }
        }

        //mAppbar.setBackgroundColor((mToolbar?.background as ColorDrawable).color)
//        val coorLayout = mAppbar.rootView()
////        val a = coorLayout::class.java.typeName
////        Log.d("a", a.toString())
//        mToolbar?.apply {
//            viewTreeObserver.addOnGlobalLayoutListener {
//                if (coorLayout is CoordinatorLayout) {
//                    coorLayout.insetListener { _, windowInsets ->
//                        val newHeight = this.height + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                        this.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, newHeight)
//                    }
//                }
//            }
//
//            onNavigationClick { (context as ComponentActivity).backPressed() }
//        }
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        when (child?.id) {
            R.id.field_set_appbar -> super.addView(child, index, params)
            R.id.field_set_toolbar -> super.addView(child, index, params)
            else -> mAppbar.addView(child, index, params)
        }
    }
}