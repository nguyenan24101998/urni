package com.anguyen.urnecessaryinformation.components

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.LinearLayout
import androidx.core.view.updatePadding
import com.anguyen.urnecessaryinformation.commons.toPx

class FillFormLayout(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {
    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        updatePadding(40.toPx(), 30.toPx(), 40.toPx(), 30.toPx())
    }
}