package com.anguyen.urnecessaryinformation.components

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import com.anguyen.urnecessaryinformation.commons.openKeyBoard
import com.google.android.material.textfield.TextInputEditText

class AutoFocusEditText(context: Context, attrs: AttributeSet): TextInputEditText(context, attrs){
    init {
        requestFocus()
        //(context as Activity).openKeyBoard(this)
    }
}