package com.anguyen.urnecessaryinformation.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.backPressed
import com.anguyen.urnecessaryinformation.commons.getColorById
import com.anguyen.urnecessaryinformation.commons.onNavigationClick
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar

class CollapsingAppbar(context: Context, attrs: AttributeSet): AppBarLayout(context, attrs){
    private var mAppbar: AppBarLayout
    private var mToolbar: MaterialToolbar?

    var title: String? = ""
        get() { return mToolbar?.subtitle.toString()}
        set(value) {
            field = value
            mToolbar?.subtitle = value

            invalidate()
            requestLayout()
        }

    init {
        inflate(context, R.layout.custom_appbar_merge_toobar, this)

        mAppbar = findViewById(R.id.field_set_appbar)
        mToolbar = findViewById(R.id.field_set_toolbar)

        context.obtainStyledAttributes(attrs, R.styleable.BackActionAppbar, 0, 0).apply {
            try {
                //title = getString(R.styleable.BackActionAppbar_title)

                mAppbar.setBackgroundColor(getColor(R.styleable.BackActionAppbar_backgroundColor,
                    context.getColorById(R.color.colorAccent)))

                mToolbar?.apply {
                    subtitle = getString(R.styleable.BackActionAppbar_title)

                    setSubtitleTextColor(getColor(R.styleable.BackActionAppbar_titleColor,
                        context.getColorById(R.color.colorWhite)))

                    setBackgroundColor(getColor(R.styleable.BackActionAppbar_backgroundColor,
                        context.getColorById(R.color.colorAccent)))
                }
            } finally {
                recycle()
            }
        }

        mToolbar?.onNavigationClick { (context as ComponentActivity).backPressed() }
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        when (child?.id) {
            R.id.field_set_appbar -> super.addView(child, index, params)
            R.id.field_set_toolbar -> super.addView(child, index, params)
            else -> mAppbar.addView(child, index, params)
        }
    }
}