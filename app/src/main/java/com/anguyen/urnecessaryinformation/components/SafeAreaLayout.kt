package com.anguyen.urnecessaryinformation.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.children
import com.anguyen.urnecessaryinformation.commons.insetListener
import com.google.android.material.appbar.AppBarLayout


//Make a space from status bar
// Apply the insets as padding to the view. Here we're setting all of the
// dimensions, but apply as appropriate to your layout. Set all 0 to prevent
// layout is stretched
class SafeAreaLayout(context: Context, attrs: AttributeSet): CoordinatorLayout(context, attrs) {
    var isLayoutCompleted = false

//    init {
//        insetListener { _, windowInsets ->
//            //val insetTop = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//
//            children.forEach { child ->
//                if (child is BackActionAppbar || child is AppBarLayout) {
//                    //it.updatePadding(0, insetTop, 0, 0)
//                    //it.marginTop = insetTop
//                    val newHeight = child.height + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                    child.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, newHeight)
//                }
//            }
//        }
//    }

//    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
//        //h = this@SafeAreaLayout.height
//
//        this@SafeAreaLayout.insetListener { _, windowInsets ->
//            children.forEach { child ->
//                if (isLayoutCompleted && child is BackActionAppbar || child is AppBarLayout) {
//                    val newHeight = child.measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                    child.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, newHeight)
//                }
//            }
//        }
//        isLayoutCompleted = true
//        super.onLayout(changed, l, t, r, b)
//    }
    override fun onLayoutChild(child: View, layoutDirection: Int) {
        this@SafeAreaLayout.insetListener { _, windowInsets ->
            if (child is BackActionAppbar || child is AppBarLayout) {
                val newHeight = child.measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
                child.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, newHeight)
            }
        }
        super.onLayoutChild(child, layoutDirection)
    }

//    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
//        this@SafeAreaLayout.insetListener { _, windowInsets ->
//            if (child is BackActionAppbar || child is AppBarLayout) {
//                val newHeight = child.measuredHeight + windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
//                child.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, newHeight)
//            }
//        }
//        super.addView(child, index, params)
//    }
}