package com.anguyen.urnecessaryinformation.components

import android.content.Context
import android.util.AttributeSet
import androidx.activity.ComponentActivity
import androidx.appcompat.widget.AppCompatButton
import com.anguyen.urnecessaryinformation.commons.backPressed
import com.anguyen.urnecessaryinformation.commons.onClick

class BackButton(context: Context, attrs: AttributeSet): AppCompatButton(context, attrs) {
    init {
        contentDescription = "No description"
        onClick { (context as ComponentActivity).backPressed() }
    }
}