package com.anguyen.urnecessaryinformation.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.ArticlesMainViewAdapter
import com.anguyen.urnecessaryinformation.commons.onRefresh
import com.anguyen.urnecessaryinformation.commons.internetWarningDialog
import com.anguyen.urnecessaryinformation.commons.verticalWithDivider
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.ArticlePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.ArticlePresenterImp
import com.anguyen.urnecessaryinformation.views.ArticleView
import kotlinx.android.synthetic.main.fragment_articles.*
import java.io.Serializable

class ArticlesFragment(
    private val mArticleType: Int = 0,
    private val mOnArticlesListScroll: OnArticlesListScroll? = null
) : Fragment(), ArticleView, Serializable {

    private var mNetworkErrorDialog: AlertDialog? = null

    private var mAdapter: ArticlesMainViewAdapter? = null
    private var mPresenter: ArticlePresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout_comment_item for this fragment
        return inflater.inflate(R.layout.fragment_articles, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
    }

    override fun onResume() {
        super.onResume()

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    private fun initView(){
        mNetworkErrorDialog = internetWarningDialog(requireContext())

        if(recycle_news == null){
            return
        }

        recycle_news.apply {
            verticalWithDivider(requireContext())

            addOnScrollListener(object: RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    mOnArticlesListScroll?.onArticlesListScroll()
                }
            })
            visibility = View.GONE //Hide when data's being loaded
        }

        if(refresh_articles != null){
            refresh_articles.onRefresh {
                if(recycle_news != null){
                    recycle_news.visibility = View.GONE
                    progress_articles_loading.visibility = View.VISIBLE
                    initData()
                }
            }
        }
    }

    private fun initData() {
        mPresenter = ArticlePresenterImp(requireContext(), this)
        mPresenter?.getArticles(mArticleType)
    }

    override fun onGetArticlesSuccess(articles: List<Article>?) {
        if(recycle_news == null){
            return
        }

        if(refresh_articles != null){
            refresh_articles.isRefreshing = false
        }

        if(mAdapter != null){
            mAdapter?.notifyDataSetChanged()
        }else{
            mAdapter = ArticlesMainViewAdapter(requireContext(), articles!!)
            recycle_news.apply {
                adapter = mAdapter
                viewTreeObserver.addOnGlobalLayoutListener {
                    if(progress_articles_loading != null){
                        progress_articles_loading.visibility = View.GONE
                        this.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun onGetArticlesFailure(message: String?) {
        if(refresh_articles != null){
            refresh_articles.isRefreshing = false
        }
        Log.d("articles $mArticleType", message!!)
    }

    override fun showInternetError() {
        if(refresh_articles != null){
            refresh_articles.isRefreshing = false
        }
        mNetworkErrorDialog?.show()
    }

    interface OnArticlesListScroll{
        fun onArticlesListScroll()
    }
}