package com.anguyen.urnecessaryinformation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.onClick
import kotlinx.android.synthetic.main.fragment_pick_image_dialog.*

class PickImageDialogFragment(
    private val mOnItemClick: OnDialogItemClickListener
) : DialogFragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout_comment_item for this fragment
        return inflater.inflate(R.layout.fragment_pick_image_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout_choose_from_gallery.onClick {
            mOnItemClick.setOnDialogOptionClick(0)
        }

        layout_choose_from_camera.onClick {
            mOnItemClick.setOnDialogOptionClick(1)
        }

        txt_img_pick_dialog_cancel.onClick { dismiss() }
    }

}

