package com.anguyen.urnecessaryinformation.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.activities.RegisterActivity
import com.anguyen.urnecessaryinformation.activities.UNIAuthActivity
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.AuthSelectorPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.AuthSelectorPresenterImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.SignInCallbackResult
import com.anguyen.urnecessaryinformation.views.AuthSelectorView
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.fragment_auth_selector.*

class AuthSelectorFragment(
    private val mLoadingDialog: SimpleArcDialog?,
    private val mSignInCallbackResult: SignInCallbackResult<User>
    //private val mOnSignedInCompleted: OnSignedInCompleted
) : BottomSheetDialogFragment(), AuthSelectorView {

    private var mNetworkErrorDialog: AlertDialog? = null
    private var mEmailConflictErrorDialog: AlertDialog? = null

    private var isLoginUniClicked = false
    private var isLoginGoogleClicked = false
    private var isLoginFbClicked = false

    private lateinit var mPresenter: AuthSelectorPresenter
    private val mFacebookCallbackManager = CallbackManager.Factory.create()
    private var mGoogleSignInIntent: Intent? = null

    companion object{
        var isUniAccountSignedIn = false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout_comment_item for this fragment
        return inflater.inflate(R.layout.fragment_auth_selector, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }

        if(isUniAccountSignedIn){
            dismiss()
            isUniAccountSignedIn = false

        }
    }

    private fun initData(){
        mPresenter = AuthSelectorPresenterImp(
            mFacebookCallbackManager, context!!, this, this
        )
        mGoogleSignInIntent = mPresenter.getGoogleApiClient().signInIntent
    }

    private fun initView(){
        mNetworkErrorDialog = internetWarningDialog(context!!)

        btn_uni_acc_login.onClick {
            isLoginUniClicked = true
            startActivity(UNIAuthActivity.getLaunchIntent(context!!))
        }

        btn_gg_login.onClick {
            isLoginGoogleClicked = true
            startActivityForResult(mGoogleSignInIntent, Default.CODE_GOOGLE_SIGN_IN_RESULT)
        }

        btn_facebook_login.onClick {
            isLoginFbClicked = true
            mPresenter.signInByFacebookAccount()
        }

        txt_toRegister.onClick {
            startActivity(RegisterActivity.getLaunchIntent(context!!))
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data)
        Log.d("signInByFacebook", "$requestCode $resultCode $data")

        if(requestCode == Default.CODE_GOOGLE_SIGN_IN_RESULT){
            val result = GoogleSignIn.getSignedInAccountFromIntent(data)!!
            try {
                val account = result.getResult(ApiException::class.java)
                Log.d("signInByGoogle", account.account.toString())
                mPresenter.signInByGoogleAccount(account)

            }catch(ex: ApiException) {
                ex.printStackTrace()
                onSignInFailure(context!!.getString(R.string.error_server))
            }
        }
    }

    override fun onSignInSuccess(user: User?) {
        //mLoadingDialog?.show()
        mSignInCallbackResult.onSuccess(user)
        dismiss()
    }

    override fun onSignInCanceled() {
        //mLoadingDialog?.dismiss()
        mSignInCallbackResult.onSignInCanceled()
    }

    override fun onSignInFailure(message: String?) {
        mLoadingDialog?.dismiss()

        if(message == null){ // Email conflict error
            if(mEmailConflictErrorDialog == null){
                mEmailConflictErrorDialog = MaterialAlertDialogBuilder(context!!).apply {
                    setTitle(R.string.error_title)
                    setMessage(R.string.error_email_conflict_message)
                    setIcon(R.drawable.ic_emoji_sad)
                    setPositiveButton(R.string.positive_button_choose_another_acc) { _, _ ->
                        startActivity(mGoogleSignInIntent)
                    }
                    setNegativeButton(R.string.general_negative_button){ dialog, _ ->
                        dialog.dismiss()
                    }
                }.create()
            }
            mEmailConflictErrorDialog?.show()
        }else{
            mSignInCallbackResult.onFailure(message)
        }
    }

    override fun showInternetError() {
        //mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}