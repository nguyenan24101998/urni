package com.anguyen.urnecessaryinformation.fragments

interface OnDialogItemClickListener {
    fun setOnDialogOptionClick(position: Int)
}