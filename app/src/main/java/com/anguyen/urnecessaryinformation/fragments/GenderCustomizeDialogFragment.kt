package com.anguyen.urnecessaryinformation.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.anguyen.urnecessaryinformation.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class GenderCustomizeDialogFragment(private val genderEdt: EditText?) : BottomSheetDialogFragment() {

    private var mGenderText = ""
    private lateinit var mGenders: Array<String>
    private var mChosenPos = 0

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog)
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout_comment_item for this fragment
        return inflater.inflate(R.layout.fragment_gender_customize, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        genderEdt?.setText(mGenderText)
    }

}