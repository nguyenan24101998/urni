package com.anguyen.urnecessaryinformation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.onClick
import kotlinx.android.synthetic.main.fragment_temp_unit.*

class TempUnitFragment(
    private val mOnItemClick: OnDialogItemClickListener
) : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_temp_unit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        txt_unit_c.onClick {
            mOnItemClick.setOnDialogOptionClick(0)
            dismiss()
        }

        txt_unit_f.onClick {
            mOnItemClick.setOnDialogOptionClick(1)
            dismiss()
        }

        txt_unit_cancel.onClick { dismiss() }
    }

}