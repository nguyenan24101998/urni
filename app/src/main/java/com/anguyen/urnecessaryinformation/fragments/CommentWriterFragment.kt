package com.anguyen.urnecessaryinformation.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_comment_writter.*

class CommentWriterFragment(
    private val mCommentSend: CommentListener
): BottomSheetDialogFragment() {

    private var isCommentEmpty = true
    private lateinit var mInputManager: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.UnDraggableBottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for context!! fragment
        return inflater.inflate(R.layout.fragment_comment_writter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView(){
        mInputManager = context!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        mInputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        edt_comment.apply {
            onTextChanged {
                isCommentEmpty = if(it.isEmpty() || it == ""){
                    img_send_comment.setTint(context!!, R.color.colorLightGrey)
                    true
                }else{
                    img_send_comment.setTint(context!!, R.color.colorGeneral)
                    false
                }
            }
        }

        img_send_comment.onClick {
            if(!isCommentEmpty){
                mCommentSend.onCommentSendClicked(edt_comment.text.toString())
                mInputManager.hideSoftInputFromWindow(edt_comment.windowToken, 0)
                dismiss()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        edt_comment.setText("")
    }

    interface CommentListener{
        fun onCommentSendClicked(content: String?)
    }

}