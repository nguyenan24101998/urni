package com.anguyen.urnecessaryinformation.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default
import com.anguyen.urnecessaryinformation.models.DailyWeatherCondition
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_weather_daily_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class DailyConditionAdapter(
    private val mContext: Context,
    private var mConditions: List<DailyWeatherCondition>,
): RecyclerView.Adapter<DailyConditionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext).inflate(
            R.layout.layout_weather_daily_item,
            parent,
            false
        ))
    }

    override fun getItemCount() = mConditions.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val condition = mConditions[position]

        val date = Date(condition.updateAt*1000)
        val today = Date()
        holder.time.text = if(date.date == today.date){
            Default.STR_TODAY
        }else{
            SimpleDateFormat(Default.VN_DATE_FORMAT2, Locale.getDefault()).format(date)
        }

        val icon = "http://openweathermap.org/img/wn/${condition.commonCondition?.get(0)?.icon}@2x.png"
        Glide.with(mContext).load(icon).into(holder.img)
        holder.humidity.text = "${condition.humidity.toInt()}${Default.STR_PERCENT_UNIT}"
        holder.tempDay.text = "${condition.temp?.day?.toInt()}${Default.STR_TEMP_UNIT}"
        holder.tempNight.text = "${condition.temp?.night?.toInt()}${Default.STR_TEMP_UNIT}"
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val time = itemView.txt_time!!
        val img = itemView.img_weather_status!!
        val humidity = itemView.txt_humidity!!
        val tempDay = itemView.txt_temp_day!!
        val tempNight = itemView.txt_temp_night!!
    }
}