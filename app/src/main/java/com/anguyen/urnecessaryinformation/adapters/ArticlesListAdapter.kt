package com.anguyen.urnecessaryinformation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.models.Article

class ArticlesListAdapter(
    private val mContext: Context,
    private val mArticles: ArrayList<Article>
):ArticlesMainViewAdapter(mContext, mArticles){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(mContext).inflate(
            R.layout.layout_article_item,
            parent,
            false
        ))
    }

    fun removeItem(position: Int){
        mArticles.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(position: Int, item: Article){
        mArticles.add(position, item)
        notifyItemInserted(position)
    }
}