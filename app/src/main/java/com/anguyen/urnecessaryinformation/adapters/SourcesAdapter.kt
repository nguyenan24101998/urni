package com.anguyen.urnecessaryinformation.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.onClick
import com.anguyen.urnecessaryinformation.models.Source
import kotlinx.android.synthetic.main.layout_source_item.view.*

class SourcesAdapter(private val mContext: Context,
                     private var mSources: List<Source>
):RecyclerView.Adapter<SourcesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(
                R.layout.layout_source_item,
                parent,
                false
            ))
    }

    override fun getItemCount() = mSources.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val source = mSources[position]
        holder.name.text = source.name
        holder.layout.onClick {
            mContext.startActivity(Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(source.url) })
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val name = itemView.txt_source_name!!
        val layout = itemView.layout_source_item!!
    }

}