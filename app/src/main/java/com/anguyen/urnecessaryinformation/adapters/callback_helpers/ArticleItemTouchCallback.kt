package com.anguyen.urnecessaryinformation.adapters.callback_helpers

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.adapters.ArticlesMainViewAdapter

class ArticleItemTouchCallback(
    private val callbackItemTouch: CallbackItemTouch
): ItemTouchHelper.Callback() {
    override fun isLongPressDragEnabled() = true
    override fun isItemViewSwipeEnabled() = true

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int{
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START
        //val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        callbackItemTouch.itemTouchOnMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        callbackItemTouch.onSwiped(viewHolder, viewHolder.adapterPosition)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if(actionState == ItemTouchHelper.ACTION_STATE_DRAG){
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            ColorDrawable(Color.RED).apply {
                setBounds(0, viewHolder.itemView.top,
                    (viewHolder.itemView.left + dX).toInt(), viewHolder.itemView.bottom)
                draw(c)
            }
        }else{
            val foregroundView = (viewHolder as ArticlesMainViewAdapter.ViewHolder).layoutForeground
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder?,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if(actionState != ItemTouchHelper.ACTION_STATE_DRAG){
            val foregroundView = (viewHolder as ArticlesMainViewAdapter.ViewHolder).layoutForeground
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        val foregroundView = (viewHolder as ArticlesMainViewAdapter.ViewHolder).layoutForeground
        //Set color of item which user swipes
//        foregroundView.background = ContextCompat.getDrawable(foregroundView.context,
//            R.drawable.bg_start_screen)

        //Clear view of item which user swipes
        getDefaultUIUtil().clearView(foregroundView)
    }
}