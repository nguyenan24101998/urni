package com.anguyen.urnecessaryinformation.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.activities.ArticleContentActivity
import com.anguyen.urnecessaryinformation.commons.Default
import com.anguyen.urnecessaryinformation.commons.onClick
import com.anguyen.urnecessaryinformation.models.Article
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_article_item.view.*

open class ArticlesMainViewAdapter(
    private val mContext: Context,
    private var mArticles: List<Article>
):RecyclerView.Adapter<ArticlesMainViewAdapter.ViewHolder>(){

    override fun getItemViewType(position: Int) = if(position == 0) 1 else 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  if(viewType == 1){
            ViewHolder(
                LayoutInflater.from(mContext).inflate(
                    R.layout.layout_first_article, parent, false
                )
            )
        }else{
            ViewHolder(
                LayoutInflater.from(mContext).inflate(
                    R.layout.layout_article_item, parent, false
                )
            )
        }
    }

    override fun getItemCount() = mArticles.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val article = mArticles[position]

        if (article.title.contains(Default.REMOVED_STATUS)) { // Article is removed
            return
        }

        holder.apply {
            Glide.with(mContext).load(article.image).into(image)

            title.apply {
                var isTitleCollapsed = false

                text = article.title

                viewTreeObserver.addOnGlobalLayoutListener {
                    if(holder.title.lineCount > 3 && !isTitleCollapsed){
                        var index = 0
                        var collapseTitle = ""
                        while (index < 3){
                            val start = this.layout.getLineStart(index)
                            val end = this.layout.getLineEnd(index)
                            collapseTitle += this.text.substring(start, end)
                            ++index
                        }
                        this.text = "$collapseTitle..."
                        isTitleCollapsed = true
                    }
                }
            }

            source.text = article.source?.name

            layout.onClick {
                title.setTextColor(ContextCompat.getColor(mContext, R.color.colorHintDefault))

                val bundle = Bundle()
                bundle.putSerializable(ArticleContentActivity.SELECTED_ARTICLE, article)
                mContext.startActivity(ArticleContentActivity.getLaunchIntent(mContext, bundle))
            }
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val layout = itemView.layout_article_item!!
        val image = itemView.img_news!!
        val title = itemView.txt_title!!
        val source = itemView.txt_source!!

        //val layoutBackground = itemView.layout_background!!
        val layoutForeground = itemView.layout_foreground!!
    }

}