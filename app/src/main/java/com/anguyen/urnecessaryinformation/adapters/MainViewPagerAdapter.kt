package com.anguyen.urnecessaryinformation.adapters

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.anguyen.urnecessaryinformation.fragments.ArticlesFragment

class MainViewPagerAdapter(
    supportFragment: FragmentManager,
    private val mSetOnArticlesListScroll: ArticlesFragment.OnArticlesListScroll
): FragmentStatePagerAdapter(supportFragment, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    override fun getItem(position: Int) = ArticlesFragment(position, mSetOnArticlesListScroll)
    override fun getCount() = 10
}