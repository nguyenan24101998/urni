package com.anguyen.urnecessaryinformation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.models.Comment
import com.anguyen.urnecessaryinformation.presenters.ArticleContentPresenter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_comment_item.view.*

class CommentsAdapter(private val mContext: Context,
                      private var mComments: List<Comment>,
                      private var mPresenter: ArticleContentPresenter
): RecyclerView.Adapter<CommentsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(
                R.layout.layout_comment_item,
                parent,
                false
            ))
    }

    override fun getItemCount() = mComments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val comments = mComments[position]

        holder.content.text = comments.content
        mPresenter.getCommentsCreator(comments.creatorId){ creator ->
            if(creator != null){
                Glide.with(mContext).load(creator.avatarUrl).into(holder.avatar)
                holder.username.text = creator.username
            }
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val avatar = itemView.cir_img_cmt_avatar!!
        val username = itemView.txt_cmt_username!!
        val content = itemView.txt_cmt_content!!
    }
}