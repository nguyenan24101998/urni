package com.anguyen.urnecessaryinformation.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default
import com.anguyen.urnecessaryinformation.models.WeatherCondition
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_hourly_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class HourlyConditionAdapter(
    private val mContext: Context,
    private var mConditions: List<WeatherCondition>,
): RecyclerView.Adapter<HourlyConditionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext).inflate(
            R.layout.layout_hourly_item,
            parent,
            false
        ))
    }

    override fun getItemCount() = mConditions.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val condition = mConditions[position]

        holder.time.text = SimpleDateFormat(
            Default.VN_TIME_FORMAT, Locale.getDefault()).format(Date(condition.updateAt*1000)
        )
        val icon = "http://openweathermap.org/img/wn/${condition.commonCondition?.get(0)?.icon}@2x.png"
        Glide.with(mContext).load(icon).into(holder.img)
        holder.humidity.text = "${condition.humidity.toInt()}${Default.STR_PERCENT_UNIT}"
        holder.temp.text = "${condition.temp.toInt()}${Default.STR_TEMP_UNIT}"
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val time = itemView.txt_time!!
        val img = itemView.img_weather_status!!
        val humidity = itemView.txt_humidity!!
        val temp = itemView.txt_temp!!
    }
}