package com.anguyen.urnecessaryinformation.adapters.callback_helpers

import androidx.recyclerview.widget.RecyclerView

interface CallbackItemTouch {
    fun itemTouchOnMove(oldPosition: Int, newPosition: Int)
    fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int)
}