package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Article

interface SearchArticleView: BaseView {
    fun onGetArticlesSuccess(articles: List<Article>?)
    fun onGetArticlesFailure(message: String?)
}