package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Article

interface ArticleView: BaseView {
//    fun onGetCurrentConditionSuccess(currentCondition: WeatherCondition?)
//    fun onGetCurrentConditionFailure(message: String?)

    fun onGetArticlesSuccess(articles: List<Article>?)
    fun onGetArticlesFailure(message: String?)
}