package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.models.User

interface WeatherView: BaseView {
    fun showLocationError()
    fun showGPSEnableRequest()
    fun onGetWeatherConditionSuccess(data: RespondWeatherCondition?)
    fun onGetWeatherConditionFailure(message: String?)
}