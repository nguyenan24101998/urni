package com.anguyen.urnecessaryinformation.views

import android.content.Intent
import com.anguyen.urnecessaryinformation.models.User

interface AuthSelectorView: BaseView {
    fun onSignInSuccess(user: User?)
    fun onSignInCanceled()
    fun onSignInFailure(message: String?)
}