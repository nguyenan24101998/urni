package com.anguyen.urnecessaryinformation.views

interface TwitterLoginView {
    fun onSignInTwitterSuccess(view: String?)
    fun onSignInTwitterFailure(message: String?)
}