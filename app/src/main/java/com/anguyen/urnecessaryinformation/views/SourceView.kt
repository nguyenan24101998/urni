package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Source

interface SourceView:BaseView {
    fun onGetSourcesSuccess(sources: List<Source>?)
    fun onGetSourcesFailure(message: String?)
}