package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.User

interface UrNIAuthView: BaseView {
    fun onSignInSuccess(userDetail: User?)
    fun onSignInFail(message: String?)
    fun onEmptyFieldsError()
}