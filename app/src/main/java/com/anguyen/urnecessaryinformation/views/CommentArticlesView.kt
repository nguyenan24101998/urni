package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Article

interface CommentArticlesView: BaseView {
    fun onGetCommentArticleSuccess(articles: List<Article>?)
    fun onDeleteCommentsSuccess()
    fun clearCommentArticleSuccess()
    fun onRequestFailure(message: String?)
    //fun onGetCommentArticleFailure(message: String?)
    //fun onDeleteCommentsFailure(message: String?)
}