package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.User

interface RegisterView: BaseView {

    fun onSignUpSuccess(userDetail: User?)
    fun onSignUpFail(message: String?)

    fun showUsernameError(count: Int)

    fun onPhoneValid()
    fun showPhoneNumberFormatError()

    fun onEmailValid()
    fun showEmailError()

    fun onPasswordLengthValid(count: Int)
    fun showPasswordLengthError(count: Int)

    fun onPasswordsAreMatched()
    fun showPasswordsMatchingError()

}