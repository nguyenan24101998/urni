package com.anguyen.urnecessaryinformation.views

interface ProfileView: BaseView {
    fun onChangePasswordSuccess()
    fun onChangePasswordFailure(message: String?)

    fun showTokenExpireError(message: String?)

    fun onSignOutSuccess()
    fun onSignOutFailure(message: String?)

    fun onChangeAvatarSuccess(url: String?)
    fun onChangeAvatarFailure(message: String?)
}