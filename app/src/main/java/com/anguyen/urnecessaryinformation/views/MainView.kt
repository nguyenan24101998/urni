package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.models.User

interface MainView: BaseView {
    fun onAutoSigningInSuccess(user: User?)
    fun onAutoSigningInFailure(message: String?)
    fun showTokenExpireError(message: String?)

    fun showLocationError()
    fun showGPSEnableRequest()
    fun onGetWeatherConditionSuccess(data: RespondWeatherCondition?)
    fun onGetWeatherConditionFailure(message: String?)
}