package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Comment

interface ArticleContentView: BaseView {
    fun onCheckSavedSuccess(wasSaved: Boolean?)
    fun onCheckSavedFailure(message: String?)

    fun onSaveSuccess()
    fun onSaveFailure(message: String?)

    fun onUnSaveSuccess()
    fun onUnSaveFailure(message: String?)

    fun onAddCommentSuccess(comment: Comment?)
    fun onAddCommentFailure(message: String?)

    fun onGetCommentsSuccess(comments: List<Comment>?)
    fun onGetCommentsFailure(message: String?)
}