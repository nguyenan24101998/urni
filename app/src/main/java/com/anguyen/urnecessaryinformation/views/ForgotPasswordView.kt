package com.anguyen.urnecessaryinformation.views

interface ForgotPasswordView: BaseView {
    fun showEmptyError()
    fun onChangePasswordSuccess()
    fun onChangePasswordFailure(message: String?)
}