package com.anguyen.urnecessaryinformation.views

interface EditProfileView: BaseView{
    fun onEditSuccess()
    fun onEditFailure(message: String?)
//    fun onChangeAvatarSuccess(url: String?)
//    fun onChangeAvatarFailure(message: String?)
    fun onDeleteAccountSuccess()
    fun onDeleteAccountFailure(message: String?)
}