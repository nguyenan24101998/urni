package com.anguyen.urnecessaryinformation.views

import com.anguyen.urnecessaryinformation.models.Article

interface SavedArticlesView {
    fun onGetSavedArticleSuccess(articles: List<Article>?)
    fun onDeleteArticleSuccess()
    fun onClearSavedArticleSuccess()

    fun onRequestFailure(message: String?)
    //fun onDeleteArticleFailure(message: String?)
    //fun onClearSavedArticleFailure(message: String?)
}