package com.anguyen.urnecessaryinformation.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FileUrl(
    @Expose
    @SerializedName("fileURL")
    var fileURL: String
)