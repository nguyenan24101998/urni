package com.anguyen.urnecessaryinformation.models

import com.google.gson.annotations.SerializedName

data class RespondData<T>(
    @SerializedName("data")
    var data: T? = null
)