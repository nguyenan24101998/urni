package com.anguyen.urnecessaryinformation.models

import androidx.room.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RespondNews(
    @SerializedName("articles") // Json key name
    var articles: List<Article>? = null
)

@Entity(tableName = "saved-article")
open class Article(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("url")
    var url: String = "",

    @ColumnInfo(name = "source")
    @SerializedName("source")
    var source: Source? = Source(),

    @SerializedName("sourceName")
    var sourceName: String? = source?.name,

    @ColumnInfo(name = "author")
    @SerializedName("author")
    var author: String = "",

    @ColumnInfo(name = "title")
    @SerializedName("title")
    var title: String = "",

    @ColumnInfo(name = "description")
    @SerializedName("description")
    var description: String = "",

    @ColumnInfo(name = "urlToImage")
    @SerializedName("urlToImage")
    var image: String = "",

    @ColumnInfo(name = "publishedAt")
    @SerializedName("publishedAt")
    var publishedAt: String = "",

    @SerializedName("content")
    var content: String = "",

): Serializable

data class Comment(

    @SerializedName("objectId")
    var id: String = "",

    @SerializedName("created")
    var created: Long = 0L,

    @SerializedName("updated")
    var updated: Long = 0L,

    @SerializedName("content")
    var content: String = "",

    @SerializedName("ownerId")
    var creatorId: String = "",

    @SerializedName("articleId")
    var articleId: String = "",

    ): Serializable

data class RespondSources(
    @SerializedName("sources") // Json key name
    var sources: List<Source>? = null
)

data class Source(
    @SerializedName("id")
    var id: String = "",

    @SerializedName("name")
    var name: String = "",

    @SerializedName("url")
    var url: String = ""

): Serializable

data class PostThread<T>(
    @SerializedName("thread")
    var thread: T? = null
)

class SourceConverter{
    companion object{
        @JvmStatic
        @TypeConverter
        fun fromObject(data: String): Source?{
            return Gson().fromJson(data, Source::class.java) //Convert to JSONObject to use
        }
        @JvmStatic
        @TypeConverter
        fun fromString(data: Source?): String?{
            return Gson().toJson(data)
        }
    }
}

//class CommentConverter{
//    @TypeConverter
//    fun fromString(data: String): List<Comment>?{
//        return Gson().fromJson(data, object: TypeToken<List<Comment>>(){ }.type)
//    }
//
//    @TypeConverter
//    fun fromComment(data: List<Comment>?): String?{
//        return Gson().toJson(data)
//    }
//}
//
//class DateConverter {
//    @TypeConverter
//    fun toDate(dateLong: Long?): Date? {
//        return dateLong?.let { Date(it) }
//    }
//
//    @TypeConverter
//    fun fromDate(date: Date?): Long? {
//        return date?.time
//    }
//}