package com.anguyen.urnecessaryinformation.models

import com.anguyen.urnecessaryinformation.commons.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(
    @Expose
    @SerializedName("objectId")
    var userId: String = "",

    @Expose
    @SerializedName("user-token")
    var userToken: String = "",

    @Expose
    @SerializedName("fullName")
    var fullName: String = "",

    @Expose
    @SerializedName("username")
    var username: String = "",

    @Expose
    @SerializedName("phoneNumber")
    var phoneNumber: String = "",

    @Expose
    @SerializedName("confirmPhoneNumber")
    var phoneNumberConfirming: Boolean = false,

    @Expose
    @SerializedName("email")
    var email: String = "",

    @Expose
    @SerializedName("avatar")
    var avatarUrl: String = "",

    @Expose
    @SerializedName("password")
    var password: String = "",

    @Expose(serialize = false, deserialize = false)
    var repeatPassword: String = "",

    @Expose
    @SerializedName("gender")
    var gender: String = "",

    @SerializedName("type")
    var type: String = "",

    @Expose(serialize = false, deserialize = true)
    @SerializedName("lastLogin")
    var lastLogin: Long = 0L, // BackendLess required

    @Expose(serialize = false, deserialize = true)
    @SerializedName("created")
    var created: Long = 0L, // BackendLess required

    @Expose(serialize = false, deserialize = true)
    @SerializedName("updated")
    var updated: Long = 0L, // BackendLess required

    @SerializedName("id")
    var providerId: String = "",

): Serializable {
    fun isInfoValid() = isUsernameValid(username) and isPhoneNumberValid(phoneNumber) and isEmailValid(email)

    fun isPasswordValid() = isPasswordValid(password) and arePasswordsSame(password, repeatPassword)

    private fun getComponents() = listOf(
        this.fullName, this.username, this.phoneNumber, this.email, this.gender
    )

    fun areFieldsEmpty() = getComponents().any { it.isEmpty() }
}