package com.anguyen.urnecessaryinformation.models

import android.location.Location
import android.location.LocationManager
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MyLocation(
    var lat: Double,
    var lon: Double
):Serializable{
    fun convertToLocation(): Location{
        return Location(LocationManager.GPS_PROVIDER).apply {
            latitude = lat
            longitude = lon
        }
    }
}

data class RespondWeatherCondition(
    var location: MyLocation? = null,

    @SerializedName("current")
    var current: WeatherCondition? = null,

    @SerializedName("hourly")
    var hourly: List<WeatherCondition>? = null,

    @SerializedName("daily")
    var daily: List<DailyWeatherCondition>,
): Serializable

open class BaseCondition (
    @SerializedName("dt")
    var updateAt: Long = 0L,

    @SerializedName("sunrise")
    var sunrise: Long = 0L,

    @SerializedName("sunset")
    var sunset: Long = 0L,

    @SerializedName("pressure")
    var pressure: Double = 0.0,

    @SerializedName("humidity")
    var humidity: Double = 0.0,

    @SerializedName("dew_point")
    var dew_point: Double = 0.0,

    @SerializedName("uvi")
    var uvi: Double = 0.0,

    @SerializedName("clouds")
    var clouds: Double = 0.0,

    @SerializedName("visibility")
    var visibility: Double = 0.0,

    @SerializedName("wind_speed")
    var wind: Double = 0.0,

    @SerializedName("weather")
    var commonCondition: List<CommonInfo>? = null

): Serializable

data class WeatherCondition(
    @SerializedName("temp")
    var temp: Double = 0.0,

    @SerializedName("feels_like")
    var realFeel: Double = 0.0,
): BaseCondition()

data class CommonInfo(
    @SerializedName("main")
    var main: String = "",

    @SerializedName("description")
    var description: String = "",

    @SerializedName("icon")
    var icon: String = "",
): Serializable

data class DailyWeatherCondition(
    @SerializedName("temp")
    var temp: DailyTemp? = null,
): BaseCondition()

data class DailyTemp(
    @SerializedName("day")
    var day: Double = 0.0,

    @SerializedName("night")
    var night: Double = 0.0,
): Serializable