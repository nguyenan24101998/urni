package com.anguyen.urnecessaryinformation.models

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServiceErrorBody<T>(
    @Expose
    @SerializedName(value = "code")
    var code: T? = null,
    @Expose
    @SerializedName(value = "message")
    var message: String = "",
    @Expose
    @SerializedName(value = "errorData")
    var errorData: JsonObject? = null
): Serializable