package com.anguyen.urnecessaryinformation.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.UrNIAuthPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.UNIAuthPresenterImp
import com.anguyen.urnecessaryinformation.views.UrNIAuthView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_uni_auth.*

class UNIAuthActivity: AppCompatActivity(), UrNIAuthView {

    private lateinit var mPresenter: UrNIAuthPresenter
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null

    private val tag = "UrFixerAuth Successful"

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context) = Intent(from, UNIAuthActivity::class.java)

        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context, bundle: Bundle) = Intent(from, UNIAuthActivity::class.java).apply {
            putExtras(bundle)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uni_auth)

        mPresenter = UNIAuthPresenterImp(this, this)
        initView()
    }

    override fun onStart() {
        super.onStart()
        val phoneNumber = intent?.extras?.getString(RegisterActivity.USER_PHONE_NUMBER)
        if(phoneNumber != null){
            edt_login.setText(phoneNumber)
        }
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    override fun onPause() {
        super.onPause()
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    private fun initView(){
        mLoadingDialog = initLoadingDialog()
        mNetworkErrorDialog = internetWarningDialog(this)

        edt_login.onTextChanged { mPresenter.onPhoneNumberChange(it) }
        edt_password.onTextChanged { mPresenter.onPasswordChange(it) }

        btn_sign_in.onClick {
            mLoadingDialog?.show()
            mPresenter.onSignInButtonClicked()
        }
        //btn_back.onClick { onBackPressed() }

        txt_forgot_password.onClick { startActivity(ForgotPasswordActivity.getLaunchIntent(this)) }
        
        btn_sign_up.onClick { startActivity(RegisterActivity.getLaunchIntent(this)) }
    }

    override fun onSignInSuccess(userDetail: User?) {
        Log.d(tag, userDetail?.userId!!)
        AuthSelectorFragment.isUniAccountSignedIn = true
        onBackPressed()
        finish()
    }

    override fun onSignInFail(message: String?) {
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.app_name), message!!)
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

    override fun onEmptyFieldsError() {
        mLoadingDialog?.dismiss()
        showEmptyFieldsRequired(this)
    }
}