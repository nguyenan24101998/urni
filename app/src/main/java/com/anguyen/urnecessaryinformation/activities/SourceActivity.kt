package com.anguyen.urnecessaryinformation.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.SourcesAdapter
import com.anguyen.urnecessaryinformation.commons.initLoadingDialog
import com.anguyen.urnecessaryinformation.commons.onClick
import com.anguyen.urnecessaryinformation.commons.onQueryRequest
import com.anguyen.urnecessaryinformation.commons.showErrorDialog
import com.anguyen.urnecessaryinformation.commons.verticalWithDivider
import com.anguyen.urnecessaryinformation.models.Source
import com.anguyen.urnecessaryinformation.presenters.SourcePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.SourcePresenterImp
import com.anguyen.urnecessaryinformation.views.SourceView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_source.*
import kotlinx.android.synthetic.main.layout_search_appbar.*

class SourceActivity : AppCompatActivity(), SourceView {

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context): Intent {
            return Intent(from, SourceActivity::class.java)
        }
    }

    private lateinit var mPresenter: SourcePresenter
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null

    private var mSearchResults = ArrayList<Source>()
    private var mAllSources: List<Source>? = null
    private var mSources: ArrayList<Source>? = null

    private var mAdapter: SourcesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_source)

        mLoadingDialog = initLoadingDialog()
        mLoadingDialog?.show()
        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    private fun initData(){
        mPresenter = SourcePresenterImp(this, this)
        mPresenter.getSources()
    }

    private fun initView(){
        recycle_sources.verticalWithDivider(this)

        //SetupSearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        search_source.apply {
            //set info
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            //set hint
            queryHint = getString(R.string.hint_search)
            //set on searching handler
            onQueryRequest(
                onQueryTextChange = { setupRecyclerView(it) },
                onQueryTextSubmit = { this.clearFocus() }
            )
        }
    }

    private fun setupRecyclerView(keyword: String?){
        mSearchResults = ArrayList() //Clear old result

        if(keyword?.isEmpty()!!){
            mSearchResults.addAll(mAllSources!!)
        }else{
            if(mAllSources != null){
                mAllSources?.forEach { element ->
                    if (element.name.contains(keyword, true)){
                        mSearchResults.add(element)
                    }
                }
            }
        }
        onGetSourcesSuccess(mSearchResults)
    }

    override fun onGetSourcesSuccess(sources: List<Source>?) {
        if(mAllSources == null){  //Backup all articles
            mAllSources = sources
        }
        mSources = ArrayList() //Init empty list
        mSources?.addAll(sources!!) //Update list's items

        if(mSources?.size == 0){
            recycle_sources.visibility = View.GONE
            txt_search_empty.visibility = View.VISIBLE
        }else{
            mAdapter = SourcesAdapter(this, mSources!!)
            recycle_sources.adapter = mAdapter

            recycle_sources.visibility = View.VISIBLE
            txt_search_empty.visibility = View.GONE
        }

        mLoadingDialog?.dismiss()
    }

    override fun onGetSourcesFailure(message: String?) {
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.app_name), message!!)
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}