package com.anguyen.urnecessaryinformation.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.presenters.ForgotPasswordPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.ForgotPasswordPresenterImp
import com.anguyen.urnecessaryinformation.views.ForgotPasswordView
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity(), ForgotPasswordView {

    private lateinit var mPresenter: ForgotPasswordPresenter
    private var mNetworkErrorDialog: AlertDialog? = null

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context) = Intent(from, ForgotPasswordActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    private fun initData(){
        mPresenter = ForgotPasswordPresenterImp(this, this)
    }

    private fun initView(){
        mNetworkErrorDialog = internetWarningDialog(this)

        btn_request_change_pass.onClick {
            edt_login.clearFocus()
            mPresenter.sendChangePasswordRequest(edt_login.text.toString().trim())
        }
    }

    override fun onChangePasswordSuccess() {
        showChangePasswordReply(this){ backPressed() }
    }

    override fun onChangePasswordFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun showEmptyError() {
        showEmptyFieldsRequired(this)
    }

    override fun showInternetError() {
        mNetworkErrorDialog?.show()
    }
}