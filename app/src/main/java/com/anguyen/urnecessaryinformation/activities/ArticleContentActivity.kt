package com.anguyen.urnecessaryinformation.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.CommentsAdapter
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.anguyen.urnecessaryinformation.fragments.CommentWriterFragment
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.Comment
import com.anguyen.urnecessaryinformation.models.Source
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.ArticleContentPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.ArticleContentPresenterImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.SignInCallbackResult
import com.anguyen.urnecessaryinformation.views.ArticleContentView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_article_content.*
import kotlinx.android.synthetic.main.layout_article_controller.*
import kotlinx.android.synthetic.main.layout_article_controller.edt_comment

class ArticleContentActivity : AppCompatActivity(), ArticleContentView,
    CommentWriterFragment.CommentListener, SignInCallbackResult<User> {

    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null
    private var mArticle: Article? = null

    private var isSaveClicked = false
    private var isCommentWritingClicked = false

    private var mPresenter: ArticleContentPresenter? = null
    private lateinit var mAuthDialog: AuthSelectorFragment
    private var mCommentAdapter: CommentsAdapter? = null
    private val mComments = ArrayList<Comment>()

    private lateinit var mCommentDialog: CommentWriterFragment
    private var isGetCommentsData = false

    companion object{
        const val SELECTED_ARTICLE = "selected article"

        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context, bundle: Bundle): Intent{
            return Intent(from, ArticleContentActivity::class.java).apply {
                putExtras(bundle)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_content)
        mNetworkErrorDialog = internetWarningDialog(this)
    }

    override fun onStart() {
        super.onStart()
        if(!isNetworkConnected(this)){
            mNetworkErrorDialog?.show()
        }else{
            mNetworkErrorDialog?.dismiss()
            initData()
            initView()
        }
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }

        if(mPresenter == null){
            return
        }

        if(isCommentWritingClicked && mPresenter?.wasUserSignedIn()!!){
            mCommentDialog.show(supportFragmentManager, "CommentWriter")
            isCommentWritingClicked = false
        }
    }

    private fun initData(){
        var articleBundle = intent?.extras?.getSerializable(SELECTED_ARTICLE)

        if(articleBundle == null || articleBundle !is Article){
//            showErrorDialog(this, R.string.error_title, R.string.error_server)
//            return
            articleBundle = Article(
                url="https://tinhte.vn/thread/jbl-authentics-dong-loa-all-in-one-nho-gon-dam-chat-co-dien-vua-ra-mat-tai-ifa2023.3712633/",
                source = Source(name = "Tinhte.vn")
            )
        }

        mArticle = articleBundle
        mPresenter = ArticleContentPresenterImp(this, this)
        mPresenter?.checkIfArticleWasSaved(mArticle?.url!!)

        mCommentDialog = CommentWriterFragment(this)
        mAuthDialog = AuthSelectorFragment(mLoadingDialog, this)
    }


    private fun initView(){
        mLoadingDialog = initLoadingDialog()
        //mLoadingDialog?.show()

        layout_of_web_view.minimumHeight = metrics().height

        top_appbar_of_article.hideWhenCollapsed(toolbar_of_article)

        web_article_content.apply {
            webViewClient = object: WebViewClient(){
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    content_loader.visibility = View.GONE
                }
            }

            loadUrl(mArticle?.url!!)
        }

        //appbar.title = mArticle?.source?.name!!
        txt_title_source.text = mArticle?.source?.name!!

        recycler_comments.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            visibility = View.GONE
        }

        layout_comment_loader.visibility = View.GONE

        edt_comment.onClick {
            isCommentWritingClicked = true

            if(mPresenter?.wasUserSignedIn()!!){
                mCommentDialog.show(supportFragmentManager, "CommentWriter")
            }else{
                showSignInRequired(this, getString(R.string.txt_warning_sign_in_require), mAuthDialog)
            }
        }

        txt_save.onClick {
            if(!isSaveClicked && mArticle != null){
                mPresenter?.saveArticle(mArticle as Article)
            }else{
                mPresenter?.unSaveArticle(mArticle as Article)
            }
        }

        txt_comment.onClick { scroll_view_article.fullScroll(View.FOCUS_DOWN) }

        txt_font_size.onClick { startActivity(Intent(Settings.ACTION_DISPLAY_SETTINGS)) }

        scroll_view_article.setOnScrollChangeListener { _, _, _, _, _ ->
            if(!isGetCommentsData){
                layout_comment_loader.visibility = View.VISIBLE
                mPresenter?.getAllCommentsOf(mArticle?.url)
            }
            isGetCommentsData = true
        }

        backPressRegister(this) {
            if (web_article_content.canGoBack()) {
                web_article_content.goBack()
            } else {
                mLoadingDialog?.dismiss()
                web_article_content.stopLoading()

                finish() // Go back to main activity
            }
        }
    }

    private fun TextView.setTopDrawable(iconId: Int){
        setCompoundDrawablesWithIntrinsicBounds(0, iconId , 0, 0)
    }

    override fun onCommentSendClicked(content: String?) {
        mPresenter?.addComment(mArticle, content)
    }

    override fun onAddCommentSuccess(comment: Comment?) {
        showLongToast(this, R.string.txt_sent_comment)

        txt_empty_comment.visibility = View.GONE
        layout_comment_loader.visibility = View.GONE

        onGetCommentsSuccess(listOf(comment!!))

        scroll_view_article.fullScroll(View.FOCUS_DOWN)
    }

    override fun onAddCommentFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onGetCommentsSuccess(comments: List<Comment>?) {
        mComments.addAll(comments!!)
        layout_comment_loader.visibility = View.GONE

        if(mComments.size == 0){
            txt_empty_comment.visibility = View.VISIBLE
            recycler_comments.visibility = View.GONE
            return
        }
        if (mCommentAdapter == null){
            mCommentAdapter = CommentsAdapter(this, mComments, mPresenter!!)
            recycler_comments.adapter = mCommentAdapter
        }else{
            mCommentAdapter?.notifyDataSetChanged()
        }

        txt_empty_comment.visibility = View.GONE
        recycler_comments.visibility = View.VISIBLE
    }

    override fun onGetCommentsFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onSuccess(data: User?) { //SignIn result
        showLongToast(this, R.string.toast_successful)
    }

    override fun onSignInCanceled() { //SignIn result
        showLongToast(this, R.string.toast_canceled)
    }

    override fun onEmailConflict() {
        Log.d("SignIn", "email conflict error")
    }

    override fun onFailure(message: String?) { //SignIn result
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onCheckSavedSuccess(wasSaved: Boolean?) {
        if(wasSaved!!){
            txt_save.setTopDrawable(R.drawable.ic_saved)
            isSaveClicked = true
        }
    }

    override fun onCheckSavedFailure(message: String?) {
        Log.d("onCheckResultFailure", message!!)
    }

    override fun onSaveSuccess() {
        txt_save.setTopDrawable(R.drawable.ic_saved)
        isSaveClicked = true
        showShortToast(this, R.string.txt_saved)
    }

    override fun onSaveFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onUnSaveSuccess() {
        txt_save.setTopDrawable(R.drawable.ic_unsaved)
        isSaveClicked = false
        showShortToast(this, R.string.txt_unsaved)
    }

    override fun onUnSaveFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun showInternetError() {
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
        mNetworkErrorDialog?.show()
    }

}