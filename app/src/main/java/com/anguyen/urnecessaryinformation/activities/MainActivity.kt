package com.anguyen.urnecessaryinformation.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.WindowInsetsCompat
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.MainViewPagerAdapter
import com.anguyen.urnecessaryinformation.commons.Default.CODE_REQUEST_ACCESS_LOCATION
import com.anguyen.urnecessaryinformation.commons.Default.STR_TEMP_UNIT
import com.anguyen.urnecessaryinformation.commons.Default.VN_DATE_FORMAT
import com.anguyen.urnecessaryinformation.commons.backPressRegister
import com.anguyen.urnecessaryinformation.commons.getLocationPermissionGranted
import com.anguyen.urnecessaryinformation.commons.getSubAdminArea
import com.anguyen.urnecessaryinformation.commons.hideWhenCollapsed
import com.anguyen.urnecessaryinformation.commons.initLoadingDialog
import com.anguyen.urnecessaryinformation.commons.insetListener
import com.anguyen.urnecessaryinformation.commons.internetWarningDialog
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.commons.onClick
import com.anguyen.urnecessaryinformation.commons.onItemClick
import com.anguyen.urnecessaryinformation.commons.showErrorDialog
import com.anguyen.urnecessaryinformation.commons.showGPSRequireDialog
import com.anguyen.urnecessaryinformation.commons.showGetLocationErrorDialog
import com.anguyen.urnecessaryinformation.commons.showLongToast
import com.anguyen.urnecessaryinformation.commons.showShortToast
import com.anguyen.urnecessaryinformation.commons.showSignInRequired
import com.anguyen.urnecessaryinformation.commons.upperCaseFirstWord
import com.anguyen.urnecessaryinformation.fragments.ArticlesFragment
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.anguyen.urnecessaryinformation.fragments.OnDialogItemClickListener
import com.anguyen.urnecessaryinformation.fragments.TempUnitFragment
import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.MainPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.MainPresenterImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.SignInCallbackResult
import com.anguyen.urnecessaryinformation.views.MainView
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_main_bottom_toolbar.*
import kotlinx.android.synthetic.main.layout_main_toolbar.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), MainView, SignInCallbackResult<User>,
    ArticlesFragment.OnArticlesListScroll, OnDialogItemClickListener{

    private lateinit var mRunnable: Runnable
    private var mIsFullyExpanded = false

    private var mPresenter: MainPresenter? = null
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null
    private var mChangeTempUnitDialog: TempUnitFragment? = null

    private var mAdapter: MainViewPagerAdapter? = null
    private var mAuthDialog: AuthSelectorFragment? = null

    private var mIsBackPressedTwice = false

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context) = Intent(from, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context, bundle: Bundle): Intent{
            return Intent(from, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                putExtras(bundle)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mLoadingDialog = initLoadingDialog()
        mLoadingDialog?.show()

        mNetworkErrorDialog = internetWarningDialog(this)

        if(isNetworkConnected(this)){
            mNetworkErrorDialog?.dismiss()
            initData()
            initView()
        }else{
            mNetworkErrorDialog?.show()
        }
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }

        //onStart()
    }

    private fun initData(){
        if(mAuthDialog == null){
            mAuthDialog = AuthSelectorFragment(mLoadingDialog, this)
        }

        if(mPresenter == null){
            mPresenter = MainPresenterImp(this, this)
        }

        if(mChangeTempUnitDialog == null){
            mChangeTempUnitDialog = TempUnitFragment(this)
        }

        mIsFullyExpanded = top_appbar.isAppBarExpanded()

        mRunnable = Runnable {
            if(!mIsFullyExpanded) {
                top_appbar.setExpanded(false, true)
                bottom_appbar_main.performHide()
            }
        }
    }

    //@SuppressLint("ObsoleteSdkInt")
    private fun initView(){
        //main_collapse_toolbar.minimumHeight = layout_main.insetTop

        // set toolbar min height
        layout_main.insetListener { _, windowInsets ->
            main_collapse_toolbar.minimumHeight = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
        }

        setAppbarAutoHide()

        //Hide weather content when appbar is collapsed
        top_appbar.hideWhenCollapsed(layout_location_success)

        mPresenter?.autoSignIn()

        if (mAdapter == null){
            mAdapter = MainViewPagerAdapter(supportFragmentManager, this)
            pager_categories.adapter = mAdapter
        }else{
            mAdapter?.notifyDataSetChanged()
        }

        tab_categories.apply {
            setupWithViewPager(pager_categories)

            //Set tab's texts
            resources.getStringArray(R.array.article_categories_title).let {
                for((idx, cate) in it.withIndex()){
                    getTabAt(idx)?.text = cate
                }
            }
        }

        floating_btn_user.onClick {
            startActivity(ProfileActivity.getLaunchIntent(this))
        }

        bottom_appbar_main.onItemClick {
            when(it){
                R.id.nav_search -> startActivity(SearchActivity.getLaunchIntent(this))

                R.id.nav_weather_refresh -> {
                    mLoadingDialog?.show()
                    mPresenter?.getWeatherConditionFromCurrentLocation()
                }

                R.id.nav_temp_unit -> {
                    mChangeTempUnitDialog?.show(supportFragmentManager, "change-unit")
                }

                R.id.nav_source -> startActivity(SourceActivity.getLaunchIntent(this))
            }
        }

        getLocationPermissionGranted(this){
            mPresenter?.getWeatherConditionFromCurrentLocation()
        }

        backPressRegister(this) {
            if(mIsBackPressedTwice){  //Exit if clicked twice
                finish()
            }

            showShortToast(this, R.string.txt_backpress_request)
            mIsBackPressedTwice = true

            // Cancel first back-press after 2 second not click twice
            Handler(mainLooper).postDelayed({ mIsBackPressedTwice = false }, 3000)
        }
    }

    private fun AppBarLayout.isAppBarExpanded(): Boolean {
        val behavior = (layoutParams as CoordinatorLayout.LayoutParams).behavior
        return if (behavior is AppBarLayout.Behavior) behavior.topAndBottomOffset == 0 else false
    }

    private fun setAppbarAutoHide(){
        Timer().schedule(object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post(mRunnable)
            }
        }, 180000, 180000)
    }

    override fun onArticlesListScroll() {
        bottom_appbar_main.performShow()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == CODE_REQUEST_ACCESS_LOCATION){
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() and (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                mPresenter?.getWeatherConditionFromCurrentLocation()
            }
        }
    }

    override fun onAutoSigningInSuccess(user: User?) {
        mLoadingDialog?.dismiss()
        Log.d("auto sign in", "successful")
    }

    override fun onAutoSigningInFailure(message: String?) {
        mLoadingDialog?.dismiss()
        if(message != null){
            showErrorDialog(this, getString(R.string.error_title), message)
        }else{ // have no last login
            Log.d("auto sign in", "have no last login")
        }
    }

    override fun setOnDialogOptionClick(position: Int) { // Change temp unit dialog callback
        mLoadingDialog?.show()
        mPresenter?.saveTempUnit(position)
        mPresenter?.getWeatherConditionFromCurrentLocation()
    }

    override fun showTokenExpireError(message: String?) {
        mLoadingDialog?.dismiss()
        showSignInRequired(this, message, mAuthDialog)
    }

    override fun onSuccess(data: User?) { //SignIn result
        mLoadingDialog?.dismiss()
        showLongToast(this, R.string.toast_successful)
    }

    override fun onSignInCanceled() { //SignIn result
        mLoadingDialog?.dismiss()
        showLongToast(this, R.string.toast_canceled)
    }

    override fun onFailure(message: String?) { //SignIn result
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onEmailConflict() {
        mLoadingDialog?.dismiss()
        Log.d("SignIn", "email conflict error")
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onGetWeatherConditionSuccess(data: RespondWeatherCondition?) {
        if(data == null){
            onGetWeatherConditionFailure("")
            return
        }

        val current = data.current!!
        val common = current.commonCondition?.get(0)
        val icon = "http://openweathermap.org/img/wn/${common?.icon}@4x.png"

        Glide.with(this).load(icon).into(img_condition_status)

        Log.d("MyLocation", "${data.location?.lat}, ${data.location?.lon}")

        //Current main
        txt_address.text = try{
            data.location?.convertToLocation()?.getSubAdminArea(this)
        } catch (ex: Exception){
            "Lỗi khi lấy địa chỉ"
        }

        txt_temp.text = "${current.temp.toInt()}$STR_TEMP_UNIT"
        txt_condition.text = common?.description?.upperCaseFirstWord()

        val date = Date(current.updateAt * 1000)
        txt_updated_at.text = "${getString(R.string.txt_update_at)} " +
                SimpleDateFormat(VN_DATE_FORMAT, Locale.getDefault()).format(date)

        // Change View
        layout_location_success.visibility = View.VISIBLE
        layout_location_failure.visibility = View.GONE

        showLongToast(this, R.string.weather_success)

        // Set top bar share view and click
        top_appbar.onClick {
            val bundle = Bundle().apply { putSerializable(WeatherActivity.EXTRA_WEATHER_DATA, data) }

            val options =  ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                Pair(img_weather_status_background, WeatherActivity.VIEW_WEATHER_STATUS_BG),
                Pair(img_condition_status, WeatherActivity.VIEW_IMAGE_WEATHER_STATUS),
                Pair(txt_address, WeatherActivity.VIEW_TXT_WEATHER_ADDRESS),
                Pair(txt_temp, WeatherActivity.VIEW_TXT_WEATHER_TEMP),
                Pair(txt_condition, WeatherActivity.VIEW_TXT_WEATHER_STATUS),
                Pair(txt_updated_at, WeatherActivity.VIEW_TXT_WEATHER_UPDATE_TIME)
            )

            startActivity(WeatherActivity.getLaunchIntent(this, bundle), options.toBundle())
        }

        mLoadingDialog?.dismiss()
    }

    override fun onGetWeatherConditionFailure(message: String?) {
        layout_location_failure.visibility = View.VISIBLE
        layout_location_success.visibility = View.GONE
        Log.d("GetWeather", message!!)
        mLoadingDialog?.dismiss()
        showLongToast(this, R.string.error_message)
    }

    override fun showGPSEnableRequest() {
        layout_location_failure.visibility = View.VISIBLE
        layout_location_success.visibility = View.GONE
        showGPSRequireDialog(this){
            mLoadingDialog?.dismiss()
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }

    override fun showLocationError() {
        mLoadingDialog?.dismiss()

        layout_location_failure.visibility = View.VISIBLE
        layout_location_success.visibility = View.GONE

        showGetLocationErrorDialog(this)
//        {
//            startActivity(Intent(Intent.ACTION_VIEW).apply { `package` =
//                Default.GOOGLE_PACKAGE_URL
//            })
//        }
    }

    override fun showInternetError() {
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
        mNetworkErrorDialog?.show()
    }

}