package com.anguyen.urnecessaryinformation.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.ArticlesListAdapter
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.SearchArticlePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.SearchArticlePresenterImp
import com.anguyen.urnecessaryinformation.views.SearchArticleView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.layout_search_appbar.*

class SearchActivity : AppCompatActivity(), SearchArticleView {

    private var mLoadingDialog: SimpleArcDialog? = null

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context): Intent {
            return Intent(from, SearchActivity::class.java)
        }
    }

    private lateinit var mPresenter: SearchArticlePresenter
    private var mNetworkErrorDialog: AlertDialog? = null

    private var mAllArticles: ArrayList<Article>? = ArrayList()
    private var mArticles: ArrayList<Article>? = null
    private var mSearchResults = ArrayList<Article>()
    private var mAdapter: ArticlesListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        mLoadingDialog = initLoadingDialog()
        mLoadingDialog?.show()
        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    private fun initData(){
        mNetworkErrorDialog = internetWarningDialog(this)
        mPresenter = SearchArticlePresenterImp(this, this)
        mPresenter.getAllArticle()
    }

    private fun initView(){
        // Change status bar color
        //StatusBar.changeColorFollowBy(this, appbar_top_search)

        recycle_articles.verticalWithDivider(this)

        //SetupSearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        search_source.apply {
            //set info
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            //set hint
            queryHint = getString(R.string.hint_search)
            //set on searching handler
            onQueryRequest(
                onQueryTextChange = { resetRecyclerView(it) },
                onQueryTextSubmit = { this.clearFocus() }
            )
        }
    }

    private fun resetRecyclerView(keyword: String?){
        mSearchResults = ArrayList() //Clear old result

        if(keyword?.isEmpty()!!){
            mSearchResults.addAll(mAllArticles!!)
        }else{
            if(mAllArticles != null){
                mAllArticles?.forEach { element ->
                    if (element.title.contains(keyword, true)){
                        mSearchResults.add(element)
                    }
                }
            }
        }
        articlesFilter(mSearchResults)
    }

    private fun articlesFilter(articles: List<Article>?){
        mArticles = ArrayList() //Init empty list
        mArticles?.addAll(articles!!) //Update list's items

        if(mArticles?.size == 0){
            recycle_articles.visibility = View.GONE
            txt_search_empty.visibility = View.VISIBLE
        }else{
            mAdapter = ArticlesListAdapter(this, mArticles!!)
            recycle_articles.adapter = mAdapter

            recycle_articles.visibility = View.VISIBLE
            txt_search_empty.visibility = View.GONE
        }
    }

    override fun onGetArticlesSuccess(articles: List<Article>?) {
        mAllArticles?.addAll(articles!!)
        if(mAdapter == null){
            mAdapter = ArticlesListAdapter(this, mAllArticles!!)
            recycle_articles.adapter = mAdapter
        }else{
            mAdapter?.notifyDataSetChanged()
        }
        recycle_articles.visibility = View.VISIBLE
        txt_search_empty.visibility = View.GONE
        mLoadingDialog?.dismiss()
    }

    override fun onGetArticlesFailure(message: String?) {
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.app_name), message!!)
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}