package com.anguyen.urnecessaryinformation.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anguyen.urnecessaryinformation.R
import kotlinx.android.synthetic.main.activity_start.*
import kotlinx.coroutines.*

class StartActivity: AppCompatActivity() {

    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        gif_logo.setBackgroundResource(R.drawable.ic_main_logo)

        activityScope.launch {
            delay(1500)
            startActivity(MainActivity.getLaunchIntent(this@StartActivity))
            finish()
        }
    }

    override fun onDestroy() {
        activityScope.cancel()
        super.onDestroy()
    }

}