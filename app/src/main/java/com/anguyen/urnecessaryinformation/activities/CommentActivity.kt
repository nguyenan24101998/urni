package com.anguyen.urnecessaryinformation.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.*
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.callback_helpers.ArticleItemTouchCallback
import com.anguyen.urnecessaryinformation.adapters.ArticlesListAdapter
import com.anguyen.urnecessaryinformation.adapters.callback_helpers.CallbackItemTouch
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.implement.CommentArticlePresenterImp
import com.anguyen.urnecessaryinformation.views.CommentArticlesView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_comment.*
import kotlinx.android.synthetic.main.activity_comment.btn_back
import kotlinx.android.synthetic.main.activity_comment.btn_clear
import kotlinx.android.synthetic.main.activity_comment.recycle_articles
import kotlinx.android.synthetic.main.activity_comment.txt_empty

class CommentActivity : AppCompatActivity(), CommentArticlesView, CallbackItemTouch {

    private var mPresenter: CommentArticlePresenterImp? = null
    private var mAdapter: ArticlesListAdapter? = null

    private val mArticles = ArrayList<Article>()
    private lateinit var mArticleIds: ArrayList<String>

    private lateinit var mItemTouchHelper: ItemTouchHelper
    private lateinit var mItemTouchHelperCallback: ItemTouchHelper.Callback

    private var mNetworkErrorDialog: AlertDialog? = null
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mCurrentUser: User? = null

    companion object{
        const val CURRENT_USER = "current-user"

        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context, bundle: Bundle): Intent {
            return Intent(from, CommentActivity::class.java).apply {
                putExtras(bundle)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)

        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    private fun initData(){
        mLoadingDialog = initLoadingDialog()
        mLoadingDialog?.show()

        val serializable = intent?.extras?.getSerializable(CURRENT_USER)
        if(serializable == null && serializable is User){
            onRequestFailure(getString(R.string.error_server))
            mLoadingDialog?.dismiss()
            return
        }else{
            mCurrentUser = serializable as User
            mPresenter = CommentArticlePresenterImp(this, this)
        }
    }

    private fun initView(){
        mNetworkErrorDialog = internetWarningDialog(this)
        mPresenter?.getCommentArticles(mCurrentUser?.userId!!)

        recycle_articles.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
        }

        mItemTouchHelperCallback = ArticleItemTouchCallback(this)
        mItemTouchHelper = ItemTouchHelper(mItemTouchHelperCallback).apply {
            attachToRecyclerView(recycle_articles)
        }

        txt_empty.visibility = View.GONE
    }

    override fun itemTouchOnMove(oldPosition: Int, newPosition: Int) {

    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
        //Get title and collapse it
        val collapsedTitle =
            mArticles[viewHolder.adapterPosition].title.apply { substring(0, 5) + "..." }

        //Backup of removed item for undo
        val deletedItem = mArticles[viewHolder.adapterPosition]

        //Remove the item from recyclerview
        mAdapter?.removeItem(position)

        layout_comment_articles_list.setUndoSnackBar(
            from = this,
            text = collapsedTitle,
            actionHandler = { mAdapter?.restoreItem(position, deletedItem) },
            dismissCallbackHandler = {
                mPresenter?.deleteUserComments(
                    mCurrentUser?.userToken!!, mCurrentUser?.userId!!, deletedItem.url
                )
            }
        )
    }

    override fun onGetCommentArticleSuccess(articles: List<Article>?) {
        mArticles.addAll(articles!!)

        mArticleIds = ArrayList()
        mArticles.forEach { mArticleIds.add(it.url) }

        btn_clear.onClick {  //Setup clear button
            if(mAdapter != null || mAdapter?.itemCount != 0){
                showWarningDialog(this,
                    R.string.txt_clear_all_title,
                    R.string.txt_clear_all_comment_message
                ){
                    mLoadingDialog?.show()
                    mPresenter?.clearAllCommentArticles(
                        mCurrentUser?.userToken!!, mCurrentUser?.userId!!
                    )
                }
            }
        }

        if(mArticles.size == 0){
            txt_empty.visibility = View.VISIBLE
            recycle_articles.visibility = View.GONE
        }else{
            if(mAdapter == null){
                mAdapter = ArticlesListAdapter(this, mArticles)
                recycle_articles.adapter = mAdapter
            }else{
                mAdapter?.notifyDataSetChanged()
            }
            txt_empty.visibility = View.GONE
            recycle_articles.visibility = View.VISIBLE
        }

        mLoadingDialog?.dismiss()
    }

    override fun onDeleteCommentsSuccess() {
        mLoadingDialog?.dismiss()
        showLongToast(this, R.string.toast_successful)
    }

    override fun clearCommentArticleSuccess() {
        mLoadingDialog?.dismiss()
        txt_empty.visibility = View.VISIBLE
        recycle_articles.visibility = View.GONE
        showLongToast(this, R.string.toast_successful)
    }

    override fun onRequestFailure(message: String?) {
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}