package com.anguyen.urnecessaryinformation.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.*
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.callback_helpers.ArticleItemTouchCallback
import com.anguyen.urnecessaryinformation.adapters.ArticlesListAdapter
import com.anguyen.urnecessaryinformation.adapters.callback_helpers.CallbackItemTouch
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.SavedArticlePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.SavedArticlePresenterImp
import com.anguyen.urnecessaryinformation.views.SavedArticlesView
import kotlinx.android.synthetic.main.activity_saved.*

class SavedActivity : AppCompatActivity(), SavedArticlesView, CallbackItemTouch {

    private var mPresenter: SavedArticlePresenter? = null
    private var mAdapter: ArticlesListAdapter? = null
    private val mArticles = ArrayList<Article>()

    private lateinit var mItemTouchHelper: ItemTouchHelper
    private lateinit var mItemTouchHelperCallback: ItemTouchHelper.Callback

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context): Intent {
            return Intent(from, SavedActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved)

        initData()
        initView()
    }

    private fun initData(){
        mPresenter = SavedArticlePresenterImp(this, this)
    }

    private fun initView(){
        recycle_articles.apply {
            layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
        }

        mItemTouchHelperCallback = ArticleItemTouchCallback(this)
        mItemTouchHelper = ItemTouchHelper(mItemTouchHelperCallback).apply {
            attachToRecyclerView(recycle_articles)
        }

        mPresenter?.getSavedArticle()

        btn_back.onClick { backPressed() }

        btn_clear.onClick {
            if(mAdapter != null || mAdapter?.itemCount != 0){
                showWarningDialog(this,
                    R.string.txt_clear_all_title,
                    R.string.txt_clear_all_saved_message
                ){
                    mPresenter?.clearAllSavedArticle()
                }
            }
        }
    }

    override fun itemTouchOnMove(oldPosition: Int, newPosition: Int) {
//        mArticles.add(newPosition, mArticles.removeAt(oldPosition))
//
//        if(mAdapter != null){
//            mAdapter?.notifyItemMoved(oldPosition, newPosition)
//        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
        //Get title and collapse it
        val collapsedTitle =
            mArticles[viewHolder.adapterPosition].title.apply { substring(0, 5) + "..." }

        //Backup of removed item for undo
        val deletedItem = mArticles[viewHolder.adapterPosition]

        //Remove the item from recyclerview
        mAdapter?.removeItem(position)

        //Init undo snackBar
        layout_saved_articles_list.setUndoSnackBar(
            from = this,
            text = collapsedTitle,
            actionHandler = { mAdapter?.restoreItem(position, deletedItem) },
            dismissCallbackHandler = { mPresenter?.deleteArticle(deletedItem.url) }
        )
    }

    override fun onGetSavedArticleSuccess(articles: List<Article>?) {
        mArticles.addAll(articles!!)

        if(mArticles.size == 0){
            recycle_articles.visibility = View.GONE
            txt_empty.visibility = View.VISIBLE
        }else{
            recycle_articles.visibility = View.VISIBLE
            txt_empty.visibility = View.GONE

            if(mAdapter == null){
                mAdapter = ArticlesListAdapter(this, mArticles)
                recycle_articles.adapter = mAdapter
            }else{
                mAdapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onRequestFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onDeleteArticleSuccess() {
        if(mAdapter?.itemCount == 0){
            onClearSavedArticleSuccess()
        }
        showShortToast(this, R.string.txt_deleted_message)
    }

    override fun onClearSavedArticleSuccess() {
        recycle_articles.visibility = View.GONE
        txt_empty.visibility = View.VISIBLE
    }
    
}