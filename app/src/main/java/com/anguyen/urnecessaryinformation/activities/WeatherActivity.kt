package com.anguyen.urnecessaryinformation.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.adapters.DailyConditionAdapter
import com.anguyen.urnecessaryinformation.adapters.HourlyConditionAdapter
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.STR_F_UNIT
import com.anguyen.urnecessaryinformation.commons.Default.VN_DATE_FORMAT
import com.anguyen.urnecessaryinformation.models.*
import com.anguyen.urnecessaryinformation.presenters.WeatherPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.WeatherPresenterImp
import com.anguyen.urnecessaryinformation.views.WeatherView
import com.bumptech.glide.Glide
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_weather.*
import kotlinx.android.synthetic.main.layout_weather_common_status.*
import kotlinx.android.synthetic.main.layout_weather_common_status.txt_address
import kotlinx.android.synthetic.main.layout_weather_current_condition.*
import kotlinx.android.synthetic.main.layout_weather_daily.*
import kotlinx.android.synthetic.main.layout_weather_hourly.*
import kotlinx.android.synthetic.main.layout_weather_toolbar.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class WeatherActivity : AppCompatActivity(), WeatherView{

    private lateinit var mWeatherData: RespondWeatherCondition
    private lateinit var mCurrentCondition: WeatherCondition
    private lateinit var mCurrentCommonInfo: CommonInfo
    private lateinit var mHourlyInfo: ArrayList<WeatherCondition>
    private lateinit var mDailyInfo: ArrayList<DailyWeatherCondition>

    private lateinit var mPresenter: WeatherPresenter
    private var isUsingFeUnit = false
    private var mNetworkErrorDialog: AlertDialog? = null

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context, bundle: Bundle): Intent {
            return Intent(from, WeatherActivity::class.java).apply {
                putExtras(bundle)
            }
        }

        // Extra name for the ID parameter
        const val EXTRA_WEATHER_DATA = "weather:status"
        // View name of the header image. Used for activity scene transitions
        const val VIEW_WEATHER_STATUS_BG = "weather:background"
        // View name of the header image. Used for activity scene transitions
        const val VIEW_IMAGE_WEATHER_STATUS = "weather:image"
        // View name of the header title. Used for activity scene transitions
        const val VIEW_TXT_WEATHER_ADDRESS = "weather:address"
        const val VIEW_TXT_WEATHER_TEMP = "weather:temperature"
        // View name of the header title. Used for activity scene transitions
        const val VIEW_TXT_WEATHER_STATUS = "weather:status"
        // View name of the header title. Used for activity scene transitions
        const val VIEW_TXT_WEATHER_UPDATE_TIME = "weather:update_at"
    }

    private var mLoadingDialog: SimpleArcDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()
        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    private fun initData(){
        mWeatherData = intent?.extras?.getSerializable(EXTRA_WEATHER_DATA) as RespondWeatherCondition
        mPresenter = WeatherPresenterImp(this, this)
        mNetworkErrorDialog = internetWarningDialog(this)

        val unit = mPresenter.getTempUnit()
        isUsingFeUnit = unit == STR_F_UNIT
    }

    private fun initView(){
        // Create Loader
        mLoadingDialog = initLoadingDialog()

//        refresh_weather.apply { //Set refresh color
//            setOnRefreshListener {
//                setColorSchemeColors(ContextCompat.getColor(this@WeatherActivity, R.color.colorGeneral))
//            }
//        }

        setContentView(R.layout.activity_weather)
        setSupportActionBar(weather_collapsed_toolbar)

        layout_weather.insetListener { _, windowInsets ->
            val top = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars()).top
            app_bar_weather.updatePadding(0, top, 0, 0)
        }

        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            elevation = 0f
        }

        // Retrieve the correct Item instance, using the ID provided in the Intent
        ViewCompat.setTransitionName(img_weather_status, VIEW_IMAGE_WEATHER_STATUS)
        ViewCompat.setTransitionName(layout_weather_top, VIEW_WEATHER_STATUS_BG)
        ViewCompat.setTransitionName(txt_address, VIEW_TXT_WEATHER_ADDRESS)
        ViewCompat.setTransitionName(txt_temp, VIEW_TXT_WEATHER_TEMP)
        ViewCompat.setTransitionName(txt_status, VIEW_TXT_WEATHER_STATUS)
        ViewCompat.setTransitionName(txt_updated_at, VIEW_TXT_WEATHER_UPDATE_TIME)

        recycle_hourly_weather.horizontal(this)
        recycle_daily_weather.horizontal(this)

        switch_temp_unit.apply {
            isChecked = isUsingFeUnit
            setOnCheckedChangeListener { _, isChecked ->
                if(isChecked){
                    mPresenter.saveTempUnit(1)
                }else{
                    mPresenter.saveTempUnit(0)
                }
            }
        }

        loadDataToUI(mWeatherData)
    }

    @SuppressLint("SetTextI18n")
    private fun loadDataToUI(data: RespondWeatherCondition){
        mCurrentCondition = data.current!!
        mCurrentCommonInfo = mCurrentCondition.commonCondition?.get(0)!!

        mHourlyInfo = ArrayList()
        mDailyInfo = ArrayList()

        // Compact hourly list
        mHourlyInfo.add(data.hourly!![1])
        var i = 5
        do {
            mHourlyInfo.add(data.hourly!![i])
            i += 4
        } while (i < data.hourly!!.size && mHourlyInfo.size != 6) // Load 6 items only

        // Read daily list data
        mDailyInfo.addAll(data.daily)

        //Current main
        val address = try{
            data.location?.convertToLocation()?.getSubAndCityArea(this)
        } catch (ex: Exception) {
            "Lỗi khi lấy địa chỉ"
        }

        if(address != null){
            txt_address.text = address
        }else{
            txt_address.visibility = View.GONE
        }

        val icon = "http://openweathermap.org/img/wn/${mCurrentCommonInfo.icon}@4x.png"
        Glide.with(this).load(icon).into(img_weather_status)

        txt_temp.text = "${mCurrentCondition.temp.toInt()}${Default.STR_TEMP_UNIT}"
        txt_status.text = mCurrentCommonInfo.description.upperCaseFirstWord()

        val date = Date(mCurrentCondition.updateAt*1000)
        txt_updated_at.text = "${getString(R.string.txt_update_at)} " +
                SimpleDateFormat(VN_DATE_FORMAT, Locale.getDefault()).format(date)

        txt_temp_max.text = "${mCurrentCondition.temp.toInt()}${Default.STR_TEMP_UNIT}"
        txt_temp_min.text = "${mCurrentCondition.temp.toInt()}${Default.STR_TEMP_UNIT}"
        txt_real_feel.text = "${mCurrentCondition.realFeel.toInt()}${Default.STR_TEMP_UNIT}"

        val appbarTitle = "${data.location?.convertToLocation()?.getSubAdminArea(this)}, " +
                "${txt_temp.text} ${mCurrentCommonInfo.description}"

        app_bar_weather.onOffSetChanged{ appBarLayout, verticalOffset ->
           when {
                abs(verticalOffset) - appBarLayout.totalScrollRange == 0 -> { //Collapsed
                    refresh_weather.isEnabled = false
                    weather_expanded_toolbar.title = appbarTitle
                }

                verticalOffset == 0 -> { //Expanded
                    //set pull refresh if only scrolling on top
                    refresh_weather.apply {
                        isEnabled = true
                        onRefresh { mPresenter.getWeatherConditionFromCurrentLocation() }
                    }
                    weather_expanded_toolbar.title = ""
                }

                else -> { //Idle
                    refresh_weather.isEnabled = false
                    weather_expanded_toolbar.title = ""
                }
            }
        }

        loadCurrentConditionInfo()
        loadHourlyCondition()
        loadDailyCondition()
    }

    @SuppressLint("SetTextI18n")
    private fun loadCurrentConditionInfo(){
        txt_sunrise.text = SimpleDateFormat(
            Default.VN_TIME_FORMAT, Locale.getDefault()).format(Date(mCurrentCondition.sunrise*1000)
        )
        txt_sunset.text = SimpleDateFormat(
            Default.VN_TIME_FORMAT, Locale.getDefault()).format(Date(mCurrentCondition.sunset*1000)
        )
        txt_wind.text = "${mCurrentCondition.wind} ${Default.STR_WIND_SPEED_UNIT}"
        txt_pressure.text = "${mCurrentCondition.pressure.toInt()} ${Default.STR_PRESSURE_UNIT}"
        txt_humidity.text = "${mCurrentCondition.humidity.toInt()}${Default.STR_PERCENT_UNIT}"
        txt_cloudiness.text = "${mCurrentCondition.clouds.toInt()}${Default.STR_PERCENT_UNIT}"
    }

    private fun loadHourlyCondition(){
        recycle_hourly_weather.adapter = HourlyConditionAdapter(this, mHourlyInfo)
    }

    private fun loadDailyCondition(){
        recycle_daily_weather.adapter = DailyConditionAdapter(this, mDailyInfo)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> backPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onGetWeatherConditionSuccess(data: RespondWeatherCondition?) {
        loadDataToUI(data!!)
        refresh_weather.isRefreshing = false
    }

    override fun onGetWeatherConditionFailure(message: String?) {
        showErrorDialog(this, getString(R.string.app_name), message!!)
        refresh_weather.isRefreshing = false
    }

    override fun showGPSEnableRequest() {
        refresh_weather.isRefreshing = false
        showGPSRequireDialog(this){
            mLoadingDialog?.dismiss()
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }

    override fun showLocationError() {
        refresh_weather.isRefreshing = false
        showGetLocationErrorDialog(this)
//        {
//            startActivity(Intent(Intent.ACTION_VIEW).apply { `package` =
//                Default.GOOGLE_PACKAGE_URL
//            })
//        }
    }

    override fun showInternetError() {
        refresh_weather.isRefreshing = false
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
        mNetworkErrorDialog?.show()
    }
}