package com.anguyen.urnecessaryinformation.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.RegisterPresenter
import com.anguyen.urnecessaryinformation.presenters.implement.RegisterPresenterImp
import com.anguyen.urnecessaryinformation.views.RegisterView
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), RegisterView {

    private lateinit var mPresenter: RegisterPresenter
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null

    private val tag = "RegisterActivity"

    companion object{
        const val CURRENT_USER_DATA = "current-register user"
        const val USER_PHONE_NUMBER = "signed up user's phone number"

        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context) = Intent(from, RegisterActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mPresenter = RegisterPresenterImp(this, this)
        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    override fun onPause() {
        super.onPause()
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }
    }

    private fun initView(){
        mLoadingDialog = initLoadingDialog()
        mNetworkErrorDialog = internetWarningDialog(this)

        //Switch Handler
        switch_using_username.onSwitched { isChecked ->
            mPresenter.onSwitchButtonChange(isChecked)

            if(isChecked){
                layout_username.animate()
                    .translationY(0f)
                    .alpha(0.0f)
                    .setListener(object: AnimatorListenerAdapter(){
                        //override fun onAnimationEnd(animation: Animator?) { layout_username.visibility = View.GONE }
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            layout_username.visibility = View.GONE
                        }
                    })
            }else{
                layout_username.animate()
                    .translationY(0f)
                    .alpha(1.0f)
                    .setListener(object: AnimatorListenerAdapter(){
                        //override fun onAnimationEnd(animation: Animator?) { layout_username.visibility = View.VISIBLE }
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            layout_username.visibility = View.VISIBLE
                        }
                    })
            }
        }

        edt_first_name.onTextChanged { mPresenter.onFirstNameChange(it) }
        edt_last_name.onTextChanged { mPresenter.onLastNameChange(it) }
        edt_username.onTextChanged { mPresenter.onUsernameChange(it) }
        edt_phoneNumber.onTextChanged { mPresenter.onPhoneNumberChange(it) }

        //val genderDialog = GenderBottomDialogFragment(edt_gender)
        val genders = resources.getStringArray(R.array.arr_gender)
        val adapter = ArrayAdapter(this, R.layout.item_gender, genders)
//        var isMenuDropDown = false

        edt_gender.apply {
            showSoftInputOnFocus = false

            setAdapter(adapter)

            onFocusChange { _, isFocus ->
                if(isFocus) {
                    showDropDown()
                    this@RegisterActivity.closeKeyBoard()
                    //isMenuDropDown = !isMenuDropDown
                }
            }

            onTextChanged { mPresenter.onGenderChange(it) }
        }

//        if (!isMenuDropDown and edt_gender.text.isEmpty()) {
//            edt_gender.setText(genders[0])
//        }

        edt_email.onTextChanged { mPresenter.onEmailChange(it) }
        edt_password.onTextChanged { mPresenter.onPasswordChange(it)}
        edt_password_confirm.onTextChanged { mPresenter.onRepeatPasswordChange(it) }

        btn_sign_up.onClick {
            mLoadingDialog?.show()
            mPresenter.onSignUpButtonClicked()
        }

        //layout_of_register_content.onClick { isMenuDropDown = false }
    }

    private fun changeErrorColor(field: List<TextView>, colorId: Int){
        field.forEach{ it.setTextColor(ContextCompat.getColor(this, colorId)) }
    }

    private fun onAuthConflict() {
        //Hide phone number
        var resultMessage = edt_phoneNumber.text.toString().phoneNumberHide()

        if(resultMessage != null){
            resultMessage = "${getString(R.string.error_uni_auth_conflict_message_1)} " +
                    "$resultMessage ${getString(R.string.error_uni_auth_conflict_message_2)}"
        }

        //Close loading dialog
        mLoadingDialog?.dismiss()

        //Show waring dialog
        showWarningDialog(this,getString(R.string.error_auth_conflict_title) , resultMessage!!){
            val bundle = Bundle()
            bundle.putString(USER_PHONE_NUMBER, edt_phoneNumber.text.toString())
            startActivity(UNIAuthActivity.getLaunchIntent(this, bundle))
        }
    }

    override fun onSignUpSuccess(userDetail: User?) {
        Log.d(tag, userDetail!!.toString())
        mLoadingDialog?.dismiss()
        AuthSelectorFragment.isUniAccountSignedIn = true
        //onBackPressed()
        finish()
    }

    override fun onSignUpFail(message: String?) {
        mLoadingDialog?.dismiss()
        if(message != null){
            showErrorDialog(this, getString(R.string.error_title), message)
        }else{  // Sign up conflict error
            onAuthConflict()
        }
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

    override fun showUsernameError(count: Int) {
        txt_username_count.text = count.toString()

        if (isUsernameValid(edt_username.text.toString())) {
            changeErrorColor(
                listOf(txt_username_count, txt_username_min_val_required)
                , R.color.colorHintDefault
            )

        }else {
            changeErrorColor(
                listOf(txt_username_count, txt_username_min_val_required)
                , R.color.colorFireBrick
            )
        }
    }

    override fun onEmailValid() {
        layout_edt_email.setError(this, false)
    }

    override fun showEmailError() {
        layout_edt_email.setError(this, true, R.string.invalid_email)
    }

    override fun onPhoneValid() {
        layout_edt_phone_number.setError(this, false)
    }

    override fun showPhoneNumberFormatError() {
        layout_edt_phone_number.setError(this, true, R.string.invalid_phone_number)
    }

    override fun onPasswordLengthValid(count: Int) {
        txt_register_password_count.text = count.toString()

        layout_edt_password.setError(this, false)

        changeErrorColor(
            listOf(txt_register_password_count, txt_password_min_val_required),
            R.color.colorHintDefault
        )
    }

    override fun showPasswordLengthError(count: Int) {
        txt_register_password_count.text = count.toString()

        layout_edt_password.setError(this, true)

        changeErrorColor(
            listOf(txt_register_password_count, txt_password_min_val_required),
            R.color.colorFireBrick
        )
    }

    override fun onPasswordsAreMatched() {
        layout_edt_password_confirm.setError(this, false)
    }

    override fun showPasswordsMatchingError() {
        layout_edt_password_confirm.setError(this, true, R.string.password_matching_error)
    }

}