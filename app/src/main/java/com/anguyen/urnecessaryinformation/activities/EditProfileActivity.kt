package com.anguyen.urnecessaryinformation.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.fragments.GenderCustomizeDialogFragment
import com.anguyen.urnecessaryinformation.fragments.OnDialogItemClickListener
import com.anguyen.urnecessaryinformation.fragments.PickImageDialogFragment
import com.anguyen.urnecessaryinformation.presenters.EditProfilePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.EditProfilePresenterImp
import com.anguyen.urnecessaryinformation.views.EditProfileView
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.IOException


class EditProfileActivity : AppCompatActivity(), EditProfileView {

    private lateinit var mPresenter: EditProfilePresenter
    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null
    private var mDeleteAccountDialog: AlertDialog? = null
    private var mPickImgDialog: PickImageDialogFragment? = null
    private lateinit var mImagePathFromCamera: String

    private var isChangeAvatar = false
    private lateinit var mAvatarFilePath: String

    companion object{
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context): Intent {
            return Intent(from, EditProfileActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initView()
    }

    override fun onResume() {
        super.onResume()

        if(mLoadingDialog != null){
            mLoadingDialog?.dismiss()
        }

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }
    }

    private fun initView(){
        mLoadingDialog = initLoadingDialog()
        mNetworkErrorDialog = internetWarningDialog(this)

        mPresenter = EditProfilePresenterImp(this, this)

        mPickImgDialog = PickImageDialogFragment(object : OnDialogItemClickListener {
            override fun setOnDialogOptionClick(position: Int) {
                when (position) {
                    0 -> {
                        startActivityForResult(
                            Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                            ),
                            Default.CODE_PICK_FROM_GALLERY
                        )
                    }

                    1 -> startCamera()
                }
            }
        })

        txt_cancel_edit.onClick { backPressed() }
        txt_save_edit.onClick {
            showWarningDialog(this, R.string.warning_edit_tittle, R.string.warning_edit_message){
                mLoadingDialog?.show()

                if(isChangeAvatar){
                    mPresenter.changeAvatarAndUpdateProfile(mAvatarFilePath)
                }else{
                    mPresenter.updateProfile()
                }
            }
        }

        val userData =  mPresenter.getCurrentUser()
        if(userData != null){

            if(userData.avatarUrl.isNotEmpty()){
                img_view_default.visibility = View.GONE
                cir_img_view_avatar.visibility = View.VISIBLE
                Glide.with(this).load(userData.avatarUrl).into(cir_img_view_avatar)
                cir_img_view_avatar.onClick {
                    mPickImgDialog?.show(supportFragmentManager, "pick")
                }
            }

            txt_change_ava.onClick { mPickImgDialog?.show(supportFragmentManager, "pick") }

            edt_edit_username.apply {
                setText(userData.username)
                onTextChanged { mPresenter.onUsernameChange(it) }
            }
            edt_edit_fullname.apply {
                setText(userData.fullName)
                onTextChanged { mPresenter.onFullNameChange(it) }
            }
            edt_edit_email.apply {
                setText(userData.email)
                onTextChanged { mPresenter.onEmailChange(it) }
            }

            val genderDialog = GenderCustomizeDialogFragment(edt_edit_gender)

            edt_edit_gender.apply {

                setText(userData.gender)

                showSoftInputOnFocus = false

                onFocusChange { view, isFocus ->
                    if(this.id == view.id  && isFocus){
                        genderDialog.show(supportFragmentManager, "gender_dialog")
                    }
                }

                onClick { genderDialog.show(supportFragmentManager, "gender_dialog") }
                onTextChanged { mPresenter.onGenderChange(it) }

            }

            //txt_edit_phone_number.text = userData.phoneNumber.phoneNumberHide()
            txt_delete_account.onClick {
                if(mDeleteAccountDialog == null){
                    mDeleteAccountDialog = MaterialAlertDialogBuilder(this).apply {
                        setTitle(R.string.txt_delete_acc_title)
                        setMessage(R.string.txt_delete_acc_message)
                        setIcon(R.drawable.ic_wondering_emoji)
                        setPositiveButton(R.string.general_positive_button){ dialog, _ ->
                            dialog.dismiss()
                            mLoadingDialog?.show()
                            mPresenter.deleteAccount()
                        }
                        setNegativeButton(R.string.general_negative_button){ dialog, _ ->
                            dialog.dismiss()
                        }
                    }.create()
                }

                mDeleteAccountDialog?.show()
            }
        }else{
            showErrorDialog(this, R.string.error_title, R.string.error_server)
        }

    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun startCamera(){
        //val currentPhotoPath = createImageFile().absolutePath
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile = try {
                    createImageTempFile().apply { mImagePathFromCamera = absolutePath }
                } catch (ex: IOException) {
                    ex.printStackTrace()
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI = FileProvider.getUriForFile(
                        this, "com.anguyen.urnecessaryinformation.fileprovider", it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    this@EditProfileActivity.startActivityForResult(
                        takePictureIntent,
                        Default.CODE_PICK_FROM_CAMERA
                    )
                }
            }
        }
    }

    private fun updateAvatarOnUI(filePath: String){
        val newAvatar = BitmapFactory.decodeFile(filePath)
        cir_img_view_avatar.setImageBitmap(newAvatar)
        isChangeAvatar = true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Default.CODE_PICK_IMG_PERMISSION){
            mPickImgDialog?.show(supportFragmentManager, "PickImageDialogFragment")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Default.CODE_PICK_IMG_PERMISSION -> {
                if (resultCode == Activity.RESULT_OK) {
                    mPickImgDialog?.show(supportFragmentManager, "PickImageDialogFragment")
                }
            }

            Default.CODE_PICK_FROM_GALLERY -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val filePath = data.data?.getPath(this)!!
                    //mPresenter.changeAvatar(filePath)
                    updateAvatarOnUI(filePath)
                    mAvatarFilePath = filePath
                } else {
                    showLongToast(this, R.string.toast_canceled)
                }
            }

            Default.CODE_PICK_FROM_CAMERA -> {
                //showWarningFromDeveloper(this)
                if (resultCode == Activity.RESULT_OK) {
                    //val filePath = data.data?.getPath(this)
                    //mPresenter.changeAvatar(mImagePathFromCamera)
                    updateAvatarOnUI(mImagePathFromCamera)
                    mAvatarFilePath = mImagePathFromCamera
                } else {
                    showLongToast(this, R.string.toast_canceled)
                }
            }
        }

        if(mLoadingDialog != null && mLoadingDialog?.isShowing!!){
            mLoadingDialog?.dismiss()
        }

        if(mPickImgDialog != null && mPickImgDialog?.isVisible!!){
            mPickImgDialog?.dismiss()
        }
    }

    override fun onEditSuccess() {
        mLoadingDialog?.dismiss()
        //backPressed()
        finish()
    }

    override fun onEditFailure(message: String?) {
        mLoadingDialog?.dismiss()
        if(message != null) {
            showErrorDialog(this, getString(R.string.error_title), message)
        }
    }

    override fun onDeleteAccountSuccess() {
        mLoadingDialog?.dismiss()
        showShortToast(this, R.string.toast_successful)
        finish()
    }

    override fun onDeleteAccountFailure(message: String?) {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}