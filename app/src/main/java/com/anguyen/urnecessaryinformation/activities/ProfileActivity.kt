package com.anguyen.urnecessaryinformation.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.ABOUT_APP_URL
import com.anguyen.urnecessaryinformation.commons.Default.APP_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.CODE_PICK_FROM_CAMERA
import com.anguyen.urnecessaryinformation.commons.Default.CODE_PICK_FROM_GALLERY
import com.anguyen.urnecessaryinformation.commons.Default.CODE_PICK_IMG_PERMISSION
import com.anguyen.urnecessaryinformation.commons.Default.GUIDE_APP_URL
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.anguyen.urnecessaryinformation.fragments.OnDialogItemClickListener
import com.anguyen.urnecessaryinformation.fragments.PickImageDialogFragment
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.ProfilePresenter
import com.anguyen.urnecessaryinformation.presenters.implement.ProfilePresenterImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.SignInCallbackResult
import com.anguyen.urnecessaryinformation.views.ProfileView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.leo.simplearcloader.SimpleArcDialog
import kotlinx.android.synthetic.main.activity_profile.*
import java.io.IOException

class ProfileActivity : AppCompatActivity(), ProfileView, SignInCallbackResult<User> {

    private var mPresenter: ProfilePresenter? = null

    private var mLoadingDialog: SimpleArcDialog? = null
    private var mNetworkErrorDialog: AlertDialog? = null
    private var mRatingDialog: AlertDialog? = null

    private var mCurrentUser: User? = null
    private var mAuthDialog: AuthSelectorFragment? = null

    private var mPickImgDialog: PickImageDialogFragment? = null
    //private var isChangeAvatar = false

    private lateinit var mBundle: Bundle
    private lateinit var mImagePathFromCamera: String


    companion object {
        @JvmStatic // In case calling from Java
        fun getLaunchIntent(from: Context): Intent {
            return Intent(from, ProfileActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }

    override fun onStart() {
        super.onStart()
        initData()
        initView()
        setupUserInfoView()
    }

    override fun onResume() {
        super.onResume()

        if(mPresenter != null){
            mPresenter?.checkUserTokenValid()
        }

        if(mNetworkErrorDialog != null){
            mNetworkErrorDialog?.dismiss()
        }

        initData()
        setupUserInfoView()
    }

    private fun initData() {
        if(mPresenter == null){
            mPresenter = ProfilePresenterImp(this, this)
        }
        mCurrentUser = mPresenter?.getCurrentUser()
    }

    private fun initView() {
        mNetworkErrorDialog = internetWarningDialog(this)
        mLoadingDialog = initLoadingDialog()
        mLoadingDialog?.show()

        mAuthDialog = AuthSelectorFragment(mLoadingDialog, this)

        mPickImgDialog = PickImageDialogFragment(object : OnDialogItemClickListener {
            override fun setOnDialogOptionClick(position: Int) {
                when (position) {
                    0 -> {
                        startActivityForResult(
                            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                            CODE_PICK_FROM_GALLERY
                        )
                    }

                    1 -> startCamera()
                }
            }
        })

        layout_fav.onClick {
            startActivity(SavedActivity.getLaunchIntent(this))
        }

        layout_comment_sent.onClick {
            mBundle = Bundle()
            mBundle.putSerializable(CommentActivity.CURRENT_USER, mCurrentUser)
            startActivity(CommentActivity.getLaunchIntent(this, mBundle))
        }

        layout_change_font.onClick { startActivity(Intent(Settings.ACTION_DISPLAY_SETTINGS)) }

        layout_help.onClick {
            //mLoadingDialog?.show()
            startActivity(Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(GUIDE_APP_URL) })
        }

        layout_about.onClick {
            //mLoadingDialog?.show()
            startActivity(Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(ABOUT_APP_URL) })
        }

        rating_bar.setOnRatingBarChangeListener { _, _, _ ->
            if(mRatingDialog == null){
                mRatingDialog = MaterialAlertDialogBuilder(this).apply {
                    setTitle(R.string.error_rating_title)
                    setMessage(R.string.error_rating_message)
                    setIcon(R.drawable.ic_emoji_smiling)
                    setPositiveButton(R.string.general_positive_button){ dialog, _ -> dialog.dismiss()}
                }.create()
            }
            mRatingDialog?.show()
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun startCamera(){
        //val currentPhotoPath = createImageFile().absolutePath
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile = try {
                    createImageTempFile().apply { mImagePathFromCamera = absolutePath }
                } catch (ex: IOException) {
                    ex.printStackTrace()
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI = FileProvider.getUriForFile(
                        this, "com.anguyen.urnecessaryinformation.fileprovider", it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    this@ProfileActivity.startActivityForResult(takePictureIntent, CODE_PICK_FROM_CAMERA)
                }
            }
        }
    }

    private fun setupUserInfoView() {
        //Check previous login
        if (mCurrentUser != null) {
            layout_auth.visibility = View.VISIBLE
            layout_logged_profile.visibility = View.VISIBLE
            layout_not_logged_profile.visibility = View.GONE

            img_profile_avatar.visibility = View.GONE
            progress_avatar.visibility = View.VISIBLE
            if (mCurrentUser?.avatarUrl!!.isNotEmpty()) {
                updateAvatarOnUI(mCurrentUser?.avatarUrl!!)
            }

            txt_profile_username.text = mCurrentUser?.username
            txt_profile_email.text = mCurrentUser?.email
            txt_profile_phone.text = mCurrentUser?.phoneNumber?.phoneNumberHide()

            img_profile_avatar.onClick {
                if(mCurrentUser?.type == APP_ACCOUNT_TYPE){
                    if (checkPickImagePermissionGranted(this)) {
                        mPickImgDialog?.show(supportFragmentManager, "PickImageDialogFragment")
                    }
                }else{
                    showErrorDialog(this, R.string.error_title, R.string.txt_avatar_change_warning)
                }
            }

            layout_edit_profile.apply { //Check User's type
                if(mCurrentUser?.type == APP_ACCOUNT_TYPE){
                    visibility = View.VISIBLE
                    onClick { startActivity(EditProfileActivity.getLaunchIntent(context)) }
                }else{
                    visibility = View.GONE
                }
            }

            layout_edit_password.apply {
                if(mCurrentUser?.type == APP_ACCOUNT_TYPE){ //Check User's type
                    visibility = View.VISIBLE
                    onClick { mPresenter?.sendChangePasswordRequest() }
                }else{
                    visibility = View.GONE
                }
            }

            layout_sign_out.onClick {
                showWarningDialog(
                    this,
                    R.string.warning_sign_out_tittle,
                    R.string.warning_sign_out_message
                ) {
                    mLoadingDialog?.show()
                    mPresenter?.signOut()
                }
            }

        } else { // haven't logged in
            layout_auth.visibility = View.GONE
            layout_logged_profile.visibility = View.GONE
            layout_not_logged_profile.visibility = View.VISIBLE

            layout_not_logged_profile.onClick {
                mAuthDialog?.show(supportFragmentManager, "Auth selector")
            }
        }
        mLoadingDialog?.dismiss()
    }

    private fun updateAvatarOnUI(url: String?){
        Glide.with(this).load(url)
            .listener(object : RequestListener<Drawable?> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progress_avatar.visibility = View.GONE
                    img_profile_avatar.visibility = View.VISIBLE
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    img_profile_avatar.setImageDrawable(
                        ContextCompat.getDrawable(this@ProfileActivity, R.drawable.img_broken_image)
                    )
                    return false
                }
            })
            .into(img_profile_avatar)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == CODE_PICK_IMG_PERMISSION){
            mPickImgDialog?.show(supportFragmentManager, "PickImageDialogFragment")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CODE_PICK_IMG_PERMISSION -> {
                if (resultCode == Activity.RESULT_OK) {
                    mPickImgDialog?.show(supportFragmentManager, "PickImageDialogFragment")
                }
            }

            CODE_PICK_FROM_GALLERY -> {
                progress_avatar.visibility = View.VISIBLE

                if (resultCode == Activity.RESULT_OK && data != null) {
                    val filePath = data.data?.getPath(this)
                    mPresenter?.changeAvatar(filePath)
                } else {
                    showLongToast(this, R.string.toast_canceled)
                }
            }

            CODE_PICK_FROM_CAMERA -> {
                progress_avatar.visibility = View.VISIBLE

                //showWarningFromDeveloper(this)
                if(resultCode == Activity.RESULT_OK){
                    //val filePath = data.data?.getPath(this)
                    mPresenter?.changeAvatar(mImagePathFromCamera)
                }else{
                    showLongToast(this, R.string.toast_canceled)
                }
            }
        }

        if(mLoadingDialog != null && mLoadingDialog?.isShowing!!){
            mLoadingDialog?.dismiss()
        }

        if(mPickImgDialog != null && mPickImgDialog?.isVisible!!){
            mPickImgDialog?.dismiss()
        }
    }

    override fun onSuccess(data: User?) { //SignIn result
        initData()
        setupUserInfoView()
        showLongToast(this, R.string.toast_successful)
    }

    override fun onSignInCanceled() { //SignIn result
        showLongToast(this, R.string.toast_canceled)
    }

    override fun onFailure(message: String?) { //SignIn result
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onEmailConflict() {
        Log.d("SignIn", "email conflict error")
    }

    override fun onChangePasswordSuccess() {
        showChangePasswordReply(this){ it.dismiss() }
    }

    override fun onChangePasswordFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onSignOutSuccess() {
        mLoadingDialog?.dismiss()

        initData()
        setupUserInfoView()
    }

    override fun onSignOutFailure(message: String?) {
        mLoadingDialog?.dismiss()
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun onChangeAvatarSuccess(url: String?) {
        updateAvatarOnUI(url)
        showLongToast(this@ProfileActivity, R.string.toast_successful)
    }

    override fun onChangeAvatarFailure(message: String?) {
        showErrorDialog(this, getString(R.string.error_title), message!!)
    }

    override fun showTokenExpireError(message: String?) {
        showSignInRequired(this, message, mAuthDialog)
    }

    override fun showInternetError() {
        mLoadingDialog?.dismiss()
        mNetworkErrorDialog?.show()
    }

}