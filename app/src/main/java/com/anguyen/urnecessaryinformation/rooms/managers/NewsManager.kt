package com.anguyen.urnecessaryinformation.rooms.managers

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.rooms.daos.NewsDao
import com.anguyen.urnecessaryinformation.rooms.databases.NewsDatabase
import com.anguyen.urnecessaryinformation.utils.CallbackData
import java.lang.RuntimeException

class NewsManager(private var mContext: Context) {

    private val newsDatabase = NewsDatabase.getRiderInstance(mContext)!!
    private var mNewsDao = newsDatabase.newsDao()

//    interface OnDataCallBack <T>{
//        fun onDataSuccess(articles: T?)
//        fun onDataFailure(message: String?)
//    }

    fun insertSavedArticle(article: Article, listener: CallbackData<List<Long>>){
        val insertSavedArticleAsync = InsertSavedArticleAsync(mContext, mNewsDao,listener)
        insertSavedArticleAsync.execute(article)
    }

    private class InsertSavedArticleAsync(
        private val mContext: Context,
        private val mNewsDao: NewsDao,
        private val mListener: CallbackData<List<Long>>
    ): AsyncTask<Article, Unit, Unit>() {

        private var callbackResult: List<Long>? = null

        override fun doInBackground(vararg article: Article?) {
            callbackResult = try {
                mNewsDao.insertSavedArticle(*article)
            }catch (ex: RuntimeException){
                ex.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            if(callbackResult != null){
                callbackResult?.forEach { Log.d("insert-article", it.toString()) }
                mListener.onSuccess(callbackResult)
            }else{
                //mListener.onDataFailure(message)
                mListener.onFailure(mContext.getString(R.string.error_server))
            }
        }

    }

    fun getAllSavedArticles(listener: CallbackData<List<Article>>){
        val getAllSavedArticlesAsync = GetSavedArticlesAsync(mContext, mNewsDao,listener)
        getAllSavedArticlesAsync.execute()
    }

    private class GetSavedArticlesAsync(
        private val mContext: Context,
        private val mNewsDao: NewsDao,
        private val mListener: CallbackData<List<Article>>
    ): AsyncTask<Article, Unit, Unit>(){
        private var callbackResult: List<Article>? = null

        override fun doInBackground(vararg p0: Article?) {
            callbackResult = try{
                mNewsDao.getSavedArticles()
            }catch (ex: RuntimeException){
                ex.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            if(callbackResult != null){
                mListener.onSuccess(callbackResult)
            }else{
                mListener.onFailure(mContext.getString(R.string.error_server))
            }
        }

    }

    fun getSavedArticle(url:String?, listener: CallbackData<List<Article>>){
        val getArticleAsync = GetArticleAsync(mContext, mNewsDao,listener)
        getArticleAsync.execute(url)
    }

    private class GetArticleAsync(
        private val mContext: Context,
        private val mNewsDao: NewsDao,
        private val mListener: CallbackData<List<Article>>,
    ): AsyncTask<String?, Unit, Unit>(){

        private var callbackResult: List<Article>? = null

        override fun doInBackground(vararg url: String?) {
            callbackResult = try{
                mNewsDao.getSavedArticle(*url)
            }catch (ex: RuntimeException){
                ex.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            if(callbackResult != null){
                mListener.onSuccess(callbackResult)
            }else{
                mListener.onFailure(mContext.getString(R.string.error_server))
            }
        }

    }

    fun deleteSelectedArticle(url:String?, listener: CallbackData<Int>){
        val deleteArticleAsync = DeleteArticleAsync(mContext, mNewsDao,listener)
        deleteArticleAsync.execute(url)
    }

    private class DeleteArticleAsync(
        private val mContext: Context,
        private val mNewsDao: NewsDao,
        private val mListener: CallbackData<Int>,
    ): AsyncTask<String?, Unit, Unit>(){

        private var callbackResult: Int? = null

        override fun doInBackground(vararg url: String?) {
            callbackResult = try{
                mNewsDao.deleteArticle(*url)
            }catch (ex: RuntimeException){
                ex.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            if(callbackResult != null){
                mListener.onSuccess(callbackResult)
            }else{
                mListener.onFailure(mContext.getString(R.string.error_server))
            }
        }

    }

    fun clearAllSavedArticles(listener: CallbackData<Int>){
        val clearAllSavedArticlesAsync = ClearSavedArticlesAsync(mContext, mNewsDao,listener)
        clearAllSavedArticlesAsync.execute()
    }

    private class ClearSavedArticlesAsync(
        private val mContext: Context,
        private val mNewsDao: NewsDao,
        private val mListener: CallbackData<Int>
    ): AsyncTask<Unit, Unit, Unit>(){
        private var callbackResult: Int? = null

        override fun doInBackground(vararg p0: Unit?) {
            callbackResult = try{
                mNewsDao.clearAllSavedArticles()
            }catch (ex: RuntimeException){
                ex.printStackTrace()
                null
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            if(callbackResult != null){
                mListener.onSuccess(callbackResult)
            }else{
                mListener.onFailure(mContext.getString(R.string.error_server))
            }
        }

    }
}