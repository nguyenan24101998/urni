package com.anguyen.urnecessaryinformation.rooms.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.anguyen.urnecessaryinformation.models.*
import com.anguyen.urnecessaryinformation.rooms.daos.NewsDao
import com.anguyen.urnecessaryinformation.rooms.databases.NewsDatabase.Companion.DATABASE_VERSION

@Database(entities = [Article::class], exportSchema = false, version = DATABASE_VERSION)
@TypeConverters(SourceConverter::class)
abstract class NewsDatabase: RoomDatabase(){
    abstract fun newsDao(): NewsDao

    companion object{
        const val DATABASE_VERSION = 10
        private const val DATABASE_NAME = "news-database"
        private var INSTANCE: NewsDatabase? = null

        @JvmStatic
        fun getRiderInstance(from: Context): NewsDatabase?{
            if(INSTANCE == null){
                synchronized (NewsDao::class){
                    INSTANCE = Room.databaseBuilder(from.applicationContext,
                        NewsDatabase::class.java,
                        DATABASE_NAME
                    ).fallbackToDestructiveMigration().build()
                }
            }

            return INSTANCE
        }
    }

}