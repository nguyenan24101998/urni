package com.anguyen.urnecessaryinformation.rooms.daos
import androidx.room.*
import com.anguyen.urnecessaryinformation.models.Article

@Dao
interface NewsDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    @Throws(RuntimeException::class)
    fun insertSavedArticle(vararg article: Article?): List<Long>

    @Query("SELECT * FROM `saved-article`")
    fun getSavedArticles(): List<Article>

    @Query("SELECT * FROM `saved-article` WHERE url = :url")
    fun getSavedArticle(vararg url: String?): List<Article>

    @Query("DELETE FROM `saved-article` WHERE url = :url")
    fun deleteArticle(vararg url: String?): Int

    @Query("DELETE FROM `saved-article`")
    fun clearAllSavedArticles(): Int
}