package com.anguyen.urnecessaryinformation.presenters

interface SavedArticlePresenter {
    fun getSavedArticle()
    fun deleteArticle(url: String?)
    fun clearAllSavedArticle()
}