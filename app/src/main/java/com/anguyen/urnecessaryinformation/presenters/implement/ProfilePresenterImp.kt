package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.FACEBOOK_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.GOOGLE_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.models.FileUrl
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.BasePresenter
import com.anguyen.urnecessaryinformation.presenters.ProfilePresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.FacebookManager
import com.anguyen.urnecessaryinformation.repositories.login_providers.FacebookManagerImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.GoogleManager
import com.anguyen.urnecessaryinformation.repositories.login_providers.GoogleManagerImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.ProfileView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ProfilePresenterImp(
    private val mContext: Context,
    private val mView: ProfileView?
): ProfilePresenter {

    private val mUsersRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mGoogleManger: GoogleManager = GoogleManagerImp(mContext)
    private val mFacebookManger: FacebookManager = FacebookManagerImp()
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)
    private var mCurrentUser: User? = null
    private val mBasePresenter = BasePresenter(mSharePref)

    override fun getCurrentUser(): User? {
        mCurrentUser = mBasePresenter.getCurrentUser()
        return mCurrentUser
    }

    override fun sendChangePasswordRequest() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(mCurrentUser == null){
            mView?.onSignOutFailure(mContext.getString(R.string.error_server))
            return
        }

        mUsersRepository.changePassword(mCurrentUser?.phoneNumber, object: CallbackData<String>{
            override fun onSuccess(data: String?) {
                Log.d("changePassword", data!!)
                mView?.onChangePasswordSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onChangePasswordFailure(message)
            }
        })
    }

    override fun checkUserTokenValid() {
        if(mCurrentUser != null) {
            mBasePresenter.checkIfUserTokenValid(
                userService = mUsersRepository,
                userToken = mCurrentUser?.userToken,

                onCheckSuccess = { isTokenValid ->
                    if (!isTokenValid!!) {
                        mSharePref.delete(LAST_LOGIN_ACCOUNT)  //remove current user data
                        mView?.showTokenExpireError(mContext.getString(R.string.error_token_expired))

                    }
                },

                onCheckFailure = {
                    Log.d("token expired", it!!)
                    mSharePref.delete(LAST_LOGIN_ACCOUNT) //remove current user data
                    if(mCurrentUser?.type == GOOGLE_ACCOUNT_TYPE){
                        mGoogleManger.revokeAccount()
                    }
                    mView?.showTokenExpireError(mContext.getString(R.string.error_token_expired))
                }
            )
        }
    }

    override fun signOut() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(mCurrentUser == null){
            mView?.onSignOutFailure(mContext.getString(R.string.error_server))
            return
        }

        when(mCurrentUser?.type){
            GOOGLE_ACCOUNT_TYPE -> mGoogleManger.revokeAccount()
            FACEBOOK_ACCOUNT_TYPE -> mFacebookManger.revokeAccount()
        }
        val mToken = mCurrentUser?.userToken!!
        mUsersRepository.signOut(mToken, object: CallbackData<Any>{
            override fun onSuccess(data: Any?) {
                if(removeCurrentUserData()){
                    mView?.onSignOutSuccess()
                }
            }

            override fun onFailure(message: String?) {
                mView?.onSignOutFailure(message)
            }
        })
    }

    override fun changeAvatar(filePath: String?) {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(mCurrentUser == null){
            mView?.onChangeAvatarFailure(mContext.getString(R.string.error_server))
            return
        }

        val mToken = mCurrentUser?.userToken!!
        val mUserId = mCurrentUser?.userId!!

        mUsersRepository.uploadFile(mToken, mUserId, filePath, object: CallbackData<FileUrl>{
            override fun onSuccess(data: FileUrl?) {
                Log.d("changeAvatar", data?.fileURL!!)

                val imageUrl = data.fileURL

                updateAvatarUrlToDatabase(imageUrl, mToken, mUserId) //Save respond url to database
                mView?.onChangeAvatarSuccess(imageUrl)
            }

            override fun onFailure(message: String?) {
                mView?.onChangeAvatarFailure(message)
            }
        })
    }

    private fun removeCurrentUserData() = mSharePref.delete(LAST_LOGIN_ACCOUNT)

    private fun updateAvatarUrlToDatabase(imageUrl: String?, token: String, userId: String){
        mUsersRepository.updateAvatarUrl(token, userId, imageUrl, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                Log.d("updateAvatarUrl", data.toString())

                //Update current user data to local storage
                data?.userToken = token
                val type = object: TypeToken<User>(){ }.type
                val currentUser = Gson().toJson(data, type)
                mSharePref.put(LAST_LOGIN_ACCOUNT, currentUser)
            }

            override fun onFailure(message: String?) {
                mView?.onChangeAvatarFailure(message)
            }
        })
    }

//    private fun deleteTempUser(){
//        mUsersRepository.deleteUser(mCurrentUser?.userId!!, object: CallbackData<Int>{
//            override fun onSuccess(data: Int?) {
//                Log.d("delete-user", data!!.toString())
//            }
//
//            override fun onFailure(message: String?) {
//                Log.d("delete-user", message!!)
//            }
//        })
//    }

}