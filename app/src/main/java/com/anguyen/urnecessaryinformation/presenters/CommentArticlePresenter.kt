package com.anguyen.urnecessaryinformation.presenters

interface CommentArticlePresenter {
    fun getCommentArticles(userId: String)
    fun deleteUserComments(userToken: String, userId: String, articleId: String)
    fun clearAllCommentArticles(userToken: String, userId: String)
}