package com.anguyen.urnecessaryinformation.presenters.implement

import android.app.Activity
import android.content.Context
import android.location.Location
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.FACEBOOK_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.GOOGLE_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.LAST_UNIT_SELECTION
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.commons.Default.STR_C_UNIT
import com.anguyen.urnecessaryinformation.commons.Default.STR_F_UNIT
import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.BasePresenter
import com.anguyen.urnecessaryinformation.presenters.MainPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.*
import com.anguyen.urnecessaryinformation.repositories.openweather.OpenWeatherRepository
import com.anguyen.urnecessaryinformation.repositories.openweather.OpenWeatherRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.MainView
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson

class MainPresenterImp(
    private val mContext: Context,
    private val mView: MainView?,
): MainPresenter {

    private val mFusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(mContext)

    private val mUsersRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mWeatherRepository: OpenWeatherRepository = OpenWeatherRepositoryImp(mContext)

    private lateinit var mFacebookManger: FacebookManager
    private val mGoogleManger: GoogleManager = GoogleManagerImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    private val mBasePresenter = BasePresenter(mSharePref)
    private var mCurrentUser: User? = null
    private var mToken: String? = null

    override fun autoSignIn() {
        if(!wasSignedInBefore()){
            mView?.onAutoSigningInFailure(null)
            return
        }

        mBasePresenter.checkIfUserTokenValid(
            userService = mUsersRepository,
            userToken = mCurrentUser?.userToken!!,

            onCheckSuccess = { isTokenValid ->
                if(isTokenValid!!){
                    getLastLoginUserData()
                }else{  // token is expired
                    mSharePref.delete(LAST_LOGIN_ACCOUNT)  //remove current user data
                    mView?.showTokenExpireError(mContext.getString(R.string.error_token_expired))
                }
            },

            onCheckFailure = {
                Log.d("token expired", it!!)
                mSharePref.delete(LAST_LOGIN_ACCOUNT) //remove current user data
                if(mCurrentUser?.type == GOOGLE_ACCOUNT_TYPE){
                    mGoogleManger.revokeAccount()
                }
                mView?.showTokenExpireError(mContext.getString(R.string.error_token_expired))
            }
        )
    }

    override fun getWeatherConditionFromCurrentLocation() {
        if(!isGPSEnabled(mContext)){
            mView?.showGPSEnableRequest()
            return
        }

        val tempUnit = mSharePref.get(LAST_UNIT_SELECTION, STR_C_UNIT)!!
        try {
            mFusedLocationProviderClient.lastLocation.addOnCompleteListener {
                if (it.isSuccessful) {
                    //Log.d("Location", it.result.toString())
                    if(it.result == null){
                        mView?.showLocationError()
                        return@addOnCompleteListener
                    }

                    getWeatherCondition(it.result, tempUnit)
                }
            }

        } catch (e: SecurityException) {
            Log.e("Location: %s", e.message!!)
        }
    }

    private fun getWeatherCondition(location: Location, unit: String) {
        mWeatherRepository.currentWeather(location, unit, object: CallbackData<RespondWeatherCondition> {
            override fun onSuccess(data: RespondWeatherCondition?) {
                mView?.onGetWeatherConditionSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onGetWeatherConditionFailure(message)
            }
        })
    }

    private fun wasSignedInBefore(): Boolean{
        mCurrentUser = BasePresenter(mSharePref).getCurrentUser()
        return mCurrentUser != null
    }

    private fun getLastLoginUserData(){
        when(mCurrentUser?.type){
            FACEBOOK_ACCOUNT_TYPE -> {
                mFacebookManger = FacebookManagerImp()
                mFacebookManger.signInWithReadPermissions(mContext as Activity,
                    CallbackManager.Factory.create(),
                    object: SignInCallbackResult<LoginResult>{
                        override fun onSuccess(data: LoginResult?) {
                            mToken = data?.accessToken?.token.toString()
                            Log.d("registerFB", mToken!!)
                            signInToBELFacebookService()
                        }

                        override fun onSignInCanceled() {
                            mView?.onAutoSigningInFailure(null)
                        }

                        override fun onFailure(message: String?) {
                            mView?.onAutoSigningInFailure(message!!)
                        }

                        override fun onEmailConflict() {
                            Log.d("registerFB", "email conflict error")
                        }
                })
            }

            else -> {
                mUsersRepository.getUserData(mCurrentUser?.userId!!, object: CallbackData<User>{
                    override fun onSuccess(data: User?) {
                        mView?.onAutoSigningInSuccess(data)
                        mSharePref.put(LAST_LOGIN_ACCOUNT, data) //update if user's data have any change
                    }

                    override fun onFailure(message: String?) {
                        mView?.onAutoSigningInFailure(message)
                    }
                })
            }
        }
    }

    private fun signInToBELFacebookService(){
        if(mToken == null || mToken == ""){
            mView?.onAutoSigningInFailure(mContext.getString(R.string.error_server))
            return
        }

        Log.d("signInToBEL", mToken!!)

        mUsersRepository.signInByFacebookAccount(mToken, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                val userData = data!!.apply { type = FACEBOOK_ACCOUNT_TYPE }
                val lastUser = Gson().toJson(userData, User::class.java)
                Log.d("signInByFacebook", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)

                mView?.onAutoSigningInSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onAutoSigningInFailure(message)
            }
        })
    }

    override fun saveTempUnit(selectedIndex: Int) {
        val unit = if(selectedIndex == 0) STR_C_UNIT else STR_F_UNIT
        mSharePref.put(LAST_UNIT_SELECTION, unit)
    }

}