package com.anguyen.urnecessaryinformation.presenters

interface UrNIAuthPresenter {
    fun onSignInButtonClicked()
    fun onPhoneNumberChange(phoneNumber: String)
    fun onPasswordChange(password: String)
}