package com.anguyen.urnecessaryinformation.presenters.implement

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.APP_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.FACEBOOK_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.GOOGLE_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.LAST_UNIT_SELECTION
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.commons.Default.STR_C_UNIT
import com.anguyen.urnecessaryinformation.commons.Default.STR_F_UNIT
import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.BasePresenter
import com.anguyen.urnecessaryinformation.presenters.MainPresenter
import com.anguyen.urnecessaryinformation.presenters.WeatherPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.*
import com.anguyen.urnecessaryinformation.repositories.openweather.OpenWeatherRepository
import com.anguyen.urnecessaryinformation.repositories.openweather.OpenWeatherRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.MainView
import com.anguyen.urnecessaryinformation.views.WeatherView
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson

class WeatherPresenterImp(
    private val mContext: Context,
    private val mView: WeatherView?,
): WeatherPresenter {

    private val mFusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(mContext)

    private val mWeatherRepository: OpenWeatherRepository = OpenWeatherRepositoryImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    override fun getWeatherConditionFromCurrentLocation() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(!isGPSEnabled(mContext)){
            mView?.showGPSEnableRequest()
            return
        }

        val tempUnit = mSharePref.get(LAST_UNIT_SELECTION, STR_C_UNIT)!!
        try {
            mFusedLocationProviderClient.lastLocation.addOnCompleteListener {
                if (it.isSuccessful) {
                    if(it.result == null){
                        mView?.showLocationError()
                        return@addOnCompleteListener
                    }

                    getWeatherCondition(it.result, tempUnit)
                }
            }

        } catch (e: SecurityException) {
            Log.e("Location: %s", e.message!!)
        }
    }

    override fun getTempUnit(): String {
        return mSharePref.get(LAST_UNIT_SELECTION, STR_C_UNIT)!!
    }

    override fun saveTempUnit(selectedIndex: Int) {
        val unit = if(selectedIndex == 0) STR_C_UNIT else STR_F_UNIT
        mSharePref.put(LAST_UNIT_SELECTION, unit)
        getWeatherConditionFromCurrentLocation() //Update UI
    }

    private fun getWeatherCondition(location: Location, unit: String) {
        mWeatherRepository.currentWeather(location, unit, object: CallbackData<RespondWeatherCondition> {
            override fun onSuccess(data: RespondWeatherCondition?) {
                mView?.onGetWeatherConditionSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onGetWeatherConditionFailure(message)
            }
        })
    }

}