package com.anguyen.urnecessaryinformation.presenters

import com.anguyen.urnecessaryinformation.models.User

interface EditProfilePresenter {
    fun getCurrentUser(): User?
    fun updateProfile()
    fun changeAvatarAndUpdateProfile(filePath: String?)
    fun onFullNameChange(fullname: String)
    fun onUsernameChange(username: String)
    fun onEmailChange(email: String)
    fun onGenderChange(gender: String)
    fun deleteAccount()
}