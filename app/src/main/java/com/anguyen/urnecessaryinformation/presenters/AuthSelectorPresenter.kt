package com.anguyen.urnecessaryinformation.presenters

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.GoogleApiClient

interface AuthSelectorPresenter {
    fun getGoogleApiClient(): GoogleSignInClient
    fun signInByGoogleAccount(account: GoogleSignInAccount?)
    fun signInByFacebookAccount()
}