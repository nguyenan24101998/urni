package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.presenters.ForgotPasswordPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.ForgotPasswordView

class ForgotPasswordPresenterImp(
    private val mContext: Context,
    private val mView: ForgotPasswordView?
): ForgotPasswordPresenter {

    private val mBackEndLessRepository: UsersRepository = UsersRepositoryImp(mContext)

    override fun sendChangePasswordRequest(phoneNumber: String?) {
        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(phoneNumber?.isEmpty()!!){
            mView?.showEmptyError()
            return
        }

        mBackEndLessRepository.changePassword(phoneNumber, object:
            CallbackData<String> {
            override fun onSuccess(data: String?) {
                Log.d("changePassword", data!!)
                mView?.onChangePasswordSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onChangePasswordFailure(message)
            }
        })
    }
}