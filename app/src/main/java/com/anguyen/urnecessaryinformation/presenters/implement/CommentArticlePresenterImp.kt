package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.CommentArticlePresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.articles.ArticlesRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.articles.ArticlesRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.CommentArticlesView

class CommentArticlePresenterImp(
    private val mContext: Context,
    private val mView: CommentArticlesView?,
): CommentArticlePresenter {

    private val mArticlesRepository: ArticlesRepository = ArticlesRepositoryImp(mContext)

    override fun getCommentArticles(userId: String) {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        mArticlesRepository.getUsersComments(userId, object: CallbackData<List<Article>>{
            override fun onSuccess(data: List<Article>?) {
                mView?.onGetCommentArticleSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onRequestFailure(message)
            }
        })
    }

    override fun deleteUserComments(userToken: String, userId: String, articleId: String) {
        mArticlesRepository.deleteUsersComments(userToken, userId, articleId,
            object: CallbackData<Int>{
                override fun onSuccess(data: Int?) {
                    Log.d("deleteUserComments", "$data rows")
                    mView?.onDeleteCommentsSuccess()
                }

                override fun onFailure(message: String?) {
                    mView?.onRequestFailure(message)
                }
            }
        )
    }

    override fun clearAllCommentArticles(userToken: String, userId: String) {
        mArticlesRepository.clearUsersComments(userToken, userId, object: CallbackData<Int>{
            override fun onSuccess(data: Int?) {
                Log.d("clearAllCommentArticles", "$data rows")
                mView?.clearCommentArticleSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onRequestFailure(message)
            }
        })
    }

}