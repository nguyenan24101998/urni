package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.ArticlePresenter
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepository
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.ArticleView

class ArticlePresenterImp(
    private val mContext: Context,
    private val mView: ArticleView?
): ArticlePresenter {

    private val mNewsRepository: NewsRepository = NewsRepositoryImp(mContext)

    override fun getArticles(typeId: Int) {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        mNewsRepository.getArticles(typeId, object: CallbackData<List<Article>>{
            override fun onSuccess(data: List<Article>?) {
                mView?.onGetArticlesSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onGetArticlesFailure(message)
            }
        })
    }

}