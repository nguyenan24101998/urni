package com.anguyen.urnecessaryinformation.presenters

import com.anguyen.urnecessaryinformation.models.User

interface ProfilePresenter {
    fun getCurrentUser(): User?
    fun checkUserTokenValid()
    fun sendChangePasswordRequest()
    fun signOut()
    fun changeAvatar(filePath: String?)
}