package com.anguyen.urnecessaryinformation.presenters

interface ArticlePresenter {
    fun getArticles(typeId: Int)
}