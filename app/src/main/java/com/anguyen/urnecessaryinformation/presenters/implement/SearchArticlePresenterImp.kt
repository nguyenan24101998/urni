package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.SearchArticlePresenter
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepository
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.SearchArticleView

class SearchArticlePresenterImp(
    private val mContext: Context,
    private val mView: SearchArticleView?
): SearchArticlePresenter {

    private val mNewsRepository: NewsRepository = NewsRepositoryImp(mContext)

    override fun getAllArticle() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }
        for(i in 0..9){
            mNewsRepository.getArticles(i, object: CallbackData<List<Article>>{
                override fun onSuccess(data: List<Article>?) {
                    mView?.onGetArticlesSuccess(data)
                }

                override fun onFailure(message: String?) {
                    mView?.onGetArticlesFailure(message)
                }
            })
        }

    }
}