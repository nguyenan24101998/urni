package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.commons.get
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.Comment
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.ArticleContentPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.articles.ArticlesRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.articles.ArticlesRepositoryImp
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.rooms.managers.NewsManager
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.ArticleContentView
import com.google.gson.Gson

class ArticleContentPresenterImp(
    private val mContext: Context,
    private val mView: ArticleContentView?
): ArticleContentPresenter {

    private val mNewsManager = NewsManager(mContext)
    private val mArticleRepository: ArticlesRepository = ArticlesRepositoryImp(mContext)
    private val mUserRepository: UsersRepository = UsersRepositoryImp(mContext)

    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)
    private var mCurrentUser: User? = null

    private lateinit var mComment: Comment

    override fun wasUserSignedIn(): Boolean {
        mCurrentUser = getCurrentUser()
        return mSharePref != null && mCurrentUser != null
    }

    override fun checkIfArticleWasSaved(url: String?) {
        mNewsManager.getSavedArticle(url, object: CallbackData<List<Article>> {
            override fun onSuccess(data: List<Article>?) {
                mView?.onCheckSavedSuccess(data?.isNotEmpty())
            }

            override fun onFailure(message: String?) {
                mView?.onCheckSavedFailure(message)
            }
        })
    }

    override fun saveArticle(article: Article) {
        mNewsManager.insertSavedArticle(article, object : CallbackData<List<Long>> {
            override fun onSuccess(data: List<Long>?) {
                Log.d("art-inserted", "${data?.get(0)} rows")
                mView?.onSaveSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onSaveFailure(message)
            }
        })
    }

    override fun unSaveArticle(article: Article) {
        mNewsManager.deleteSelectedArticle(article.url, object: CallbackData<Int>{
            override fun onSuccess(data: Int?) {
                Log.d("art-unsaved", "$data rows")
                mView?.onUnSaveSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onUnSaveFailure(message)
            }
        })
    }

    override fun addComment(article: Article?, content: String?) {
        mComment = Comment(
            content = content!!,
            articleId = article?.url!!,
            creatorId = mCurrentUser?.userId!!
        )
        mArticleRepository.checkIfArticleExist(mCurrentUser?.userToken, article.url,
            object: CallbackData<Boolean>
            {
                override fun onSuccess(data: Boolean?) {
                    val isExist = data!!
                    if(!isExist){
                        addNewArticle(article)
                    }else{
                        startAddCommentService(article.url, mComment)
                    }
                }

                override fun onFailure(message: String?) {
                    mView?.onAddCommentFailure(message)
                }
            })
    }

    override fun getAllCommentsOf(articleId: String?) {
        mArticleRepository.getAllComments(articleId, object: CallbackData<List<Comment>>{
            override fun onSuccess(data: List<Comment>?) {
                mView?.onGetCommentsSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onGetCommentsFailure(message)
            }
        })
    }

    override fun getCommentsCreator(creatorId: String?, onGetCreator: (User?) -> Unit) {
        mUserRepository.getUserData(creatorId!!, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                onGetCreator(data)
            }

            override fun onFailure(message: String?) {
                Log.d("getCommentsCreator", message!!)
                onGetCreator(null)
            }
        })
    }

    private fun getCurrentUser(): User? {
        val userString = mSharePref.get(LAST_LOGIN_ACCOUNT, "")!!
        if(userString.isEmpty()){
            return null
        }
        return Gson().fromJson(userString, User::class.java)
    }

    private fun startAddCommentService(url:String?, comment: Comment){
        mArticleRepository.addCommentToArticle(mCurrentUser?.userToken, url, comment,
            object: CallbackData<Comment>
            {
                override fun onSuccess(data: Comment?) {
                    mView?.onAddCommentSuccess(data)
                }

                override fun onFailure(message: String?) {
                    mView?.onAddCommentFailure(message)
                }
            })
    }

    private fun addNewArticle(article: Article?){
        mArticleRepository.addNewArticle(mCurrentUser?.userToken, article!!, object: CallbackData<Article>{
            override fun onSuccess(data: Article?) {
                Log.d("addNewArticle", data.toString())
                startAddCommentService(article.url, mComment)
            }

            override fun onFailure(message: String?) {
                mView?.onAddCommentFailure(message)
            }
        })
    }

}