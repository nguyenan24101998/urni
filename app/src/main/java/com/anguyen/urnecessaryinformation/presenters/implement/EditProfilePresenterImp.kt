package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.models.FileUrl
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.BasePresenter
import com.anguyen.urnecessaryinformation.presenters.EditProfilePresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.EditProfileView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class EditProfilePresenterImp(
    private val mContext: Context,
    private val mView: EditProfileView?
): EditProfilePresenter{

    private val mUsersRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    private var mUser: User? = null

    override fun getCurrentUser(): User? {
        mUser = BasePresenter(mSharePref).getCurrentUser()!!
        return mUser
    }

    override fun onFullNameChange(fullname: String) {
        mUser!!.fullName = fullname
    }

    override fun onUsernameChange(username: String) {
        mUser!!.username = username
    }

    override fun onEmailChange(email: String) {
        mUser!!.email = email
    }

    override fun onGenderChange(gender: String) {
        mUser!!.gender = gender
    }

    override fun updateProfile() {
        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(mUser!!.areFieldsEmpty()){
            mView?.onEditFailure(mContext.getString(R.string.empty_field_required))
            return
        }

        callUpdateService()
    }

    override fun changeAvatarAndUpdateProfile(filePath: String?) {
        val mToken = mUser!!.userToken
        val mUserId = mUser!!.userId

        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        if(mUser!!.areFieldsEmpty()){
            mView?.onEditFailure(mContext.getString(R.string.empty_field_required))
            return
        }

        mUsersRepository.uploadFile(mToken, mUserId, filePath, object: CallbackData<FileUrl>{
            override fun onSuccess(data: FileUrl?) {
                Log.d("changeAvatar", data?.fileURL!!)

                mUser!!.avatarUrl = data.fileURL
                updateProfile()
            }

            override fun onFailure(message: String?) {
                Log.d("changeAvatar", message!!)
            }
        })
    }

//    private fun updateAvatarUrlToDatabase(imageUrl: String?, token: String, userId: String){
//        mUsersRepository.updateAvatarUrl(token, userId, imageUrl, object: CallbackData<User>{
//            override fun onSuccess(data: User?) {
//                Log.d("updateAvatarUrl", data.toString())
//
//                //Update current user data to local storage
//                data?.userToken = token
//                val type = object: TypeToken<User>(){ }.type
//                val currentUser = Gson().toJson(data, type)
//                mSharePref.put(LAST_LOGIN_ACCOUNT, currentUser)
//            }
//
//            override fun onFailure(message: String?) {
//                Log.d("changeAvatar", message!!)
//            }
//        })
//    }

    override fun deleteAccount() {
        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        mUsersRepository.signOut(mUser!!.userToken, object: CallbackData<Any>{
            override fun onSuccess(data: Any?) {
                Log.d("deleteUser", data.toString())

                mUsersRepository.deleteUser(mUser!!.userId, object: CallbackData<Int>{
                    override fun onSuccess(data: Int?) {
                        Log.d("deleteUser", data.toString())
                        mView?.onDeleteAccountSuccess()
                    }

                    override fun onFailure(message: String?) {
                        mView?.onDeleteAccountFailure(message)
                    }
                })
            }

            override fun onFailure(message: String?) {
                mView?.onDeleteAccountFailure(message)
            }
        })
    }

    private fun callUpdateService(){
        mUsersRepository.updateProfile(mUser, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                mView?.onEditSuccess()

                //Update current user data to local storage
                val type = object: TypeToken<User>(){ }.type
                val currentUser = Gson().toJson(mUser, type)
                mSharePref.put(LAST_LOGIN_ACCOUNT, currentUser)
            }

            override fun onFailure(message: String?) {
                mView?.onEditFailure(message)
            }
        })
    }

}