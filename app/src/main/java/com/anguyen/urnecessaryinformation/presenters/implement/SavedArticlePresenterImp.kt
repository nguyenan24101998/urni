package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.presenters.SavedArticlePresenter
import com.anguyen.urnecessaryinformation.rooms.managers.NewsManager
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.SavedArticlesView

class SavedArticlePresenterImp(
    mContext: Context,
    private val mView: SavedArticlesView?
): SavedArticlePresenter {

    private val mNewsManager = NewsManager(mContext)

    override fun getSavedArticle() {
        mNewsManager.getAllSavedArticles(object: CallbackData<List<Article>>{
            override fun onSuccess(data: List<Article>?) {
                mView?.onGetSavedArticleSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onRequestFailure(message)
            }
        })
    }

    override fun deleteArticle(url: String?) {
        mNewsManager.deleteSelectedArticle(url, object: CallbackData<Int>{
            override fun onSuccess(data: Int?) {
                Log.d("art-deleted", "$data rows")
                mView?.onDeleteArticleSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onRequestFailure(message)
            }
        })
    }

    override fun clearAllSavedArticle() {
        mNewsManager.clearAllSavedArticles(object: CallbackData<Int>{
            override fun onSuccess(data: Int?) {
                Log.d("art-clear", "$data rows")
                mView?.onClearSavedArticleSuccess()
            }

            override fun onFailure(message: String?) {
                mView?.onRequestFailure(message)
            }
        })
    }

}