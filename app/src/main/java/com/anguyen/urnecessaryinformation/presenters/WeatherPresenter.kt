package com.anguyen.urnecessaryinformation.presenters

interface WeatherPresenter {
    fun getWeatherConditionFromCurrentLocation()
    fun getTempUnit(): String
    fun saveTempUnit(selectedIndex: Int)
}