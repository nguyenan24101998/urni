package com.anguyen.urnecessaryinformation.presenters

import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.User

interface ArticleContentPresenter {
    fun checkIfArticleWasSaved(url: String?)
    fun saveArticle(article: Article)
    fun unSaveArticle(article: Article)

    fun wasUserSignedIn(): Boolean
    fun addComment(article: Article?, content: String?)

    fun getAllCommentsOf(articleId: String?)
    fun getCommentsCreator(creatorId: String?, onGetCreator: (User?) -> Unit)
}