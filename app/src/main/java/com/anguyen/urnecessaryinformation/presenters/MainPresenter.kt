package com.anguyen.urnecessaryinformation.presenters

interface MainPresenter {
    fun autoSignIn()
    fun getWeatherConditionFromCurrentLocation()
    fun saveTempUnit(selectedIndex: Int)
}