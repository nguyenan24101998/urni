package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.RegisterPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.RegisterView
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.APP_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.google.gson.Gson

class RegisterPresenterImp (
    private val mContext: Context,
    private val mView: RegisterView?
): RegisterPresenter {

    private var mFirstName = ""
    private var mLastName = ""

    private var mIsSwitchChecked = false
    private val mUserDetail = User()

    private val mBackEndLessRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    override fun onSignUpButtonClicked() {
        mUserDetail.fullName = "${mFirstName.trim()} ${mLastName.trim()}"

        if(mIsSwitchChecked){
            mUserDetail.username = mUserDetail.fullName
        }

        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        //Empty components checking
        if(mUserDetail.areFieldsEmpty() or mUserDetail.password.isEmpty()
            or mUserDetail.repeatPassword.isEmpty())
        {
            mView?.onSignUpFail(mContext.getString(R.string.empty_field_required)) //Empty fields error
            return
        }

        if(!mUserDetail.isInfoValid() or !mUserDetail.isPasswordValid()){
            mView?.onSignUpFail(mContext.getString(R.string.error_user))
            return
        }

        startSignUpService(mUserDetail)
    }

    override fun onFirstNameChange(firstName: String) {
        mFirstName = firstName
    }

    override fun onLastNameChange(lastName: String) {
        mLastName =lastName
    }

    override fun onSwitchButtonChange(isChecked: Boolean) {
        mIsSwitchChecked = isChecked
    }

    override fun onUsernameChange(username: String){
        mUserDetail.username = username
        mView?.showUsernameError(username.length)
    }

    override fun onPhoneNumberChange(phoneNumber: String) {
        mUserDetail.phoneNumber = phoneNumber.trim()

        if(isPhoneNumberValid(phoneNumber)){
            mView?.onPhoneValid()
        }else{
            mView?.showPhoneNumberFormatError()
        }
    }

    override fun onGenderChange(gender: String){
        mUserDetail.gender = gender
    }

    override fun onEmailChange(email: String){
        mUserDetail.email = email
        //mRegisterDetail.email = email

        if(isEmailValid(email)) {
            mView?.onEmailValid()
        }else{
            mView?.showEmailError()
        }
    }

    override fun onPasswordChange(password: String){
        mUserDetail.password = password.trim()

        if (password.isEmpty() or isPasswordValid(password)){
            mView?.onPasswordLengthValid(password.length)
        }else{
            mView?.showPasswordLengthError(password.length)
        }
    }

    override fun onRepeatPasswordChange(repeatPassword: String){
        mUserDetail.repeatPassword = repeatPassword

        if (arePasswordsSame(mUserDetail.password, repeatPassword)) {
            mView?.onPasswordsAreMatched()
        }else{
            mView?.showPasswordsMatchingError()
        }
    }

    private fun startSignUpService(user: User?){
        mBackEndLessRepository.registerAccount(user!!, object: CallbackData<User> {
            override fun onSuccess(data: User?) {
                mView?.onSignUpSuccess(data)

                val userData = data!!.apply { type = APP_ACCOUNT_TYPE }
                val lastUser = Gson().toJson(userData, User::class.java)
                Log.d("RegisterPresenterImp", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)
            }

            override fun onFailure(message: String?) {
                mView?.onSignUpFail(message)
            }
        })
    }

}