package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import com.anguyen.urnecessaryinformation.commons.isNetworkConnected
import com.anguyen.urnecessaryinformation.models.Source
import com.anguyen.urnecessaryinformation.presenters.SourcePresenter
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepository
import com.anguyen.urnecessaryinformation.repositories.news.NewsRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.SourceView

class SourcePresenterImp(
    private val mContext: Context,
    private val mView: SourceView?
): SourcePresenter{
    private val mNewsRepository: NewsRepository = NewsRepositoryImp(mContext)

    override fun getSources() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        mNewsRepository.getSources(object: CallbackData<List<Source>>{
            override fun onSuccess(data: List<Source>?) {
                mView?.onGetSourcesSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onGetSourcesFailure(message)
            }
        })
    }
}