package com.anguyen.urnecessaryinformation.presenters

interface SourcePresenter {
    fun getSources()
}