package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.FACEBOOK_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.GOOGLE_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.AuthSelectorPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.repositories.login_providers.*
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.AuthSelectorView
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.gson.Gson

class AuthSelectorPresenterImp(
    private val mFbCallbackManager: CallbackManager,
    private val mContext: Context,
    private val mFragment: Fragment,
    private val mView: AuthSelectorView?
): AuthSelectorPresenter{

    private val mFacebookManger: FacebookManager = FacebookManagerImp()
    private val mGoogleManger: GoogleManager = GoogleManagerImp(mContext)
    private val mUserRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    private var mToken: String? = null

    override fun getGoogleApiClient() = mGoogleManger.buildGoogleApiClient()

    override fun signInByFacebookAccount() {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
        }

        mFacebookManger.signInWithReadPermissions(mFragment, mFbCallbackManager,
            object: SignInCallbackResult<LoginResult>
            {
                override fun onSuccess(data: LoginResult?) {
                    mToken = data?.accessToken?.token.toString()
                    Log.d("registerFB", mToken!!)
                    signInToBELFacebookService()
                }

                override fun onSignInCanceled() {
                    mView?.onSignInCanceled()
                }

                override fun onFailure(message: String?) {
                    Log.d("registerFB", message!!)
                    mView?.onSignInFailure(message)
            }

            override fun onEmailConflict() {
                Log.d("registerFB", "email conflict error")
            }

        })
    }

    override fun signInByGoogleAccount(account: GoogleSignInAccount?) {
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
        }

        if(account == null){
            mView?.onSignInFailure(mContext.getString(R.string.error_server))
            return
        }

        mUserRepository.checkGuestAccountExisted(account.email!!, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                if(data == null){
                    createGuestAccount(account) // Create google account to BEL by convert from Guest
                }else{
                    val lastUser = Gson().toJson(data, User::class.java)
                    Log.d("signInByGoogle", lastUser)
                    mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)

                    mView?.onSignInSuccess(data)
                }
            }

            override fun onFailure(message: String?) {
                mGoogleManger.revokeAccount()
                mView?.onSignInFailure(message)
            }
        })
    }

    private fun createGuestAccount(account: GoogleSignInAccount){
        mUserRepository.createGuestAccount(object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                if(data == null){
                    onFailure(mContext.getString(R.string.error_user))
                    return
                }

                data.apply {
                    email = account.email!!
                    avatarUrl = account.photoUrl.toString()
                    username = account.displayName!!
                    type = GOOGLE_ACCOUNT_TYPE
                }

                convertGuestToGoogleAccount(data)
            }

            override fun onFailure(message: String?) {
                mGoogleManger.revokeAccount()
                mView?.onSignInFailure(message)
            }
        })
    }

    private fun convertGuestToGoogleAccount(user: User){
        mUserRepository.registerAccount(user, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                data?.apply {
                    userToken = user.userToken
                    phoneNumber = ""
                }
                val lastUser = Gson().toJson(data, User::class.java)
                Log.d("signInByGoogle", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)

                mView?.onSignInSuccess(data)
            }

            override fun onFailure(message: String?) {
                mUserRepository.deleteUser(user.userId, object: CallbackData<Int>{
                    override fun onSuccess(data: Int?) {
                        Log.d("delete-user", data!!.toString())
                    }

                    override fun onFailure(message: String?) {
                        Log.d("delete-user", message!!)
                    }
                })
                mGoogleManger.revokeAccount()
                mView?.onSignInFailure(message)
            }
        })
        mUserRepository.updateProfile(user, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                data?.apply {
                    userToken = user.userToken
                    phoneNumber = ""
                }
                val lastUser = Gson().toJson(data, User::class.java)
                Log.d("signInByGoogle", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)

                mView?.onSignInSuccess(data)
            }

            override fun onFailure(message: String?) {
                mUserRepository.deleteUser(user.userId, object: CallbackData<Int>{
                    override fun onSuccess(data: Int?) {
                        Log.d("delete-user", data!!.toString())
                    }

                    override fun onFailure(message: String?) {
                        Log.d("delete-user", message!!)
                    }
                })
                mGoogleManger.revokeAccount()
                mView?.onSignInFailure(message)
            }
        })
    }

    private fun signInToBELFacebookService(){
        if(mToken == null || mToken == ""){
            mView?.onSignInFailure(mContext.getString(R.string.error_server))
            return
        }

        Log.d("signInToBEL", mToken!!)

        mUserRepository.signInByFacebookAccount(mToken, object: CallbackData<User>{
            override fun onSuccess(data: User?) {
                val userData = data!!.apply { type = FACEBOOK_ACCOUNT_TYPE }
                val lastUser = Gson().toJson(userData, User::class.java)
                Log.d("signInByFacebook", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)

                mView?.onSignInSuccess(data)
            }

            override fun onFailure(message: String?) {
                mView?.onSignInFailure(message)
            }
        })
    }
}