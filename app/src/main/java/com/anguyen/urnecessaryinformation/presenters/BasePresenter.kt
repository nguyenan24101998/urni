package com.anguyen.urnecessaryinformation.presenters

import android.content.SharedPreferences
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.get
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.google.gson.Gson

class BasePresenter(private val mSharePref: SharedPreferences) {

    fun getCurrentUser(): User? {
        val userString = mSharePref.get(LAST_LOGIN_ACCOUNT, "")!!
        if(userString.isEmpty()){
            return null
        }
        return Gson().fromJson(userString, User::class.java)
    }

    fun checkIfUserTokenValid(
        userService: UsersRepository,
        userToken: String?,
        onCheckSuccess: (Boolean?) -> Unit,
        onCheckFailure: (String?) -> Unit
    ){

        userService.checkIfUserTokenValid(userToken!!, object: CallbackData<Boolean> {
            override fun onSuccess(data: Boolean?) {
                onCheckSuccess(data)
            }

            override fun onFailure(message: String?) {
                onCheckFailure(message)
            }
        })
    }

}