package com.anguyen.urnecessaryinformation.presenters

interface ForgotPasswordPresenter {
    fun sendChangePasswordRequest(phoneNumber: String?)
}