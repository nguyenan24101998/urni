package com.anguyen.urnecessaryinformation.presenters.implement

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.commons.*
import com.anguyen.urnecessaryinformation.commons.Default.APP_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.LAST_LOGIN_ACCOUNT
import com.anguyen.urnecessaryinformation.commons.Default.SHARE_PREF
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.presenters.UrNIAuthPresenter
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepository
import com.anguyen.urnecessaryinformation.repositories.backendless.users.UsersRepositoryImp
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.views.UrNIAuthView
import com.google.gson.Gson

class UNIAuthPresenterImp (
    private val mContext: Context,
    private val mView: UrNIAuthView?
): UrNIAuthPresenter {

    private val mUserDetail = User()

    private val mBackEndLessRepository: UsersRepository = UsersRepositoryImp(mContext)
    private val mSharePref = mContext.getSharedPreferences(SHARE_PREF, Context.MODE_PRIVATE)

    override fun onSignInButtonClicked() {
        //Internet checking
        if(!isNetworkConnected(mContext)){
            mView?.showInternetError()
            return
        }

        //Empty components checking
        val components = listOf(mUserDetail.phoneNumber, mUserDetail.password)
        if(components.any { it.isEmpty() }){
            mView?.onEmptyFieldsError()
            return
        }

        startSignInService(mUserDetail.phoneNumber, mUserDetail.password)
    }

    override fun onPhoneNumberChange(phoneNumber: String) {
        mUserDetail.phoneNumber = phoneNumber.trim()
    }

    override fun onPasswordChange(password: String){
        mUserDetail.password = password.trim()
    }

    private fun startSignInService(phoneNumber: String, password: String){
        mBackEndLessRepository.signInByUniAccount(phoneNumber, password, object: CallbackData<User> {
            override fun onSuccess(data: User?) {
                mView?.onSignInSuccess(data)
                val userData = data!!.apply { type = APP_ACCOUNT_TYPE }
                val lastUser = Gson().toJson(userData, User::class.java)

                Log.d("UrNIAuthPresenterImp", lastUser)
                mSharePref.put(LAST_LOGIN_ACCOUNT, lastUser)
            }

            override fun onFailure(message: String?) {
                mView?.onSignInFail(message)
            }
        })
    }

}