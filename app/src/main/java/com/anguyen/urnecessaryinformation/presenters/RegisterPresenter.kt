package com.anguyen.urnecessaryinformation.presenters

interface RegisterPresenter {

    fun onSignUpButtonClicked()

    fun onFirstNameChange(firstName: String)

    fun onLastNameChange(lastName: String)

    fun onSwitchButtonChange(isChecked: Boolean)

    fun onUsernameChange(username: String)

    fun onPhoneNumberChange(phoneNumber: String)

    fun onGenderChange(gender: String)

    fun onEmailChange(email: String)

    fun onPasswordChange(password: String)

    fun onRepeatPasswordChange(repeatPassword: String)

}