package com.anguyen.urnecessaryinformation.commons

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.fragments.AuthSelectorFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * Error Dialog Functions
 */
fun showErrorDialog(from: Context, titleStringId: Int, messageId: Int) {

    MaterialAlertDialogBuilder(from).apply {

        setTitle(context.getString(titleStringId))

        setMessage(context.getString(messageId))

        setIcon(R.drawable.ic_emoji_sad)

        setPositiveButton(from.getString(R.string.general_positive_button)){ dialog, _ -> dialog.dismiss() }

    }.show()

}


fun showErrorDialog(from: Context, title: String, message: String) {

    MaterialAlertDialogBuilder(from).apply {

        setTitle(title)

        setMessage(message)

        setIcon(R.drawable.ic_emoji_sad)

        setPositiveButton(from.getString(R.string.general_positive_button)){ dialog, _ -> dialog.dismiss() }

    }.show()

}

fun showEmptyFieldsRequired(from: Context){
    showErrorDialog(from, R.string.error_title, R.string.empty_field_required)
}

/**
 * Warning Dialog Functions
 */
fun showWarningDialog(from: Context, titleId: Int, messageId: Int, confirmFunction: ()-> Unit)  {

    MaterialAlertDialogBuilder(from).apply {

        setTitle (context.getString(titleId))

        setMessage(context.getString(messageId))

        setIcon(R.drawable.ic_wondering_emoji)

        setPositiveButton(from.getString(R.string.general_positive_button)){ _, _ -> confirmFunction() }

        setNegativeButton(from.getString(R.string.general_negative_button)){ dialog, _ -> dialog.cancel() }

    }.show()
}

fun showWarningDialog(from: Context, titleId: String, messageId: String, confirmFunction: ()-> Unit)  {

    MaterialAlertDialogBuilder(from).apply {

        setTitle (titleId)

        setMessage(messageId)

        setIcon(R.drawable.ic_emoji_sad)

        setPositiveButton(from.getString(R.string.general_positive_button)){ _, _ -> confirmFunction() }

        setNegativeButton(from.getString(R.string.general_negative_button)){ dialog, _ -> dialog.cancel() }

    }.show()
}

fun internetWarningDialog(from: Context): AlertDialog {
    return MaterialAlertDialogBuilder(from).apply {
        setTitle ( R.string.internet_error_title)
        setMessage(R.string.internet_error_message)
        setIcon(R.drawable.ic_emoji_sad)

        setPositiveButton(from.getString(R.string.general_positive_button)){ _, _ ->
            from.startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
        }

        setNegativeButton(from.getString(R.string.general_negative_button)){
                dialog, _ -> dialog.cancel()
        }
    }.create()
}

fun showWarningFromDeveloper(from: Context){
    MaterialAlertDialogBuilder(from).apply {

        setMessage(context.getString(R.string.developing_feature_content))

        setPositiveButton(from.getString(R.string.general_positive_button)){ dialog, _ -> dialog.dismiss() }

    }.show()
}

//fun showWarningFromDeveloper(from: Context, onDialogDismiss: () -> Unit){
//    MaterialAlertDialogBuilder(from).apply {
//
//        setMessage(context.getString(R.string.developing_feature_content))
//
//        setPositiveButton(from.getString(R.string.general_positive_button)){ dialog, _ -> dialog.dismiss() }
//
//        setOnDismissListener { onDialogDismiss() }
//
//    }.show()
//}

fun showSignInRequired(from: AppCompatActivity, message: String?, loginDialog: AuthSelectorFragment?){
    MaterialAlertDialogBuilder(from).apply {
        setTitle (R.string.error_title)
        setMessage(message)
        setIcon(R.drawable.ic_wondering_emoji)
        setPositiveButton(from.getString(R.string.txt_to_login_positive_button)){ dialog, _ ->
            dialog.dismiss()
            loginDialog?.show(from.supportFragmentManager, "auth selector")
        }
        setNegativeButton(from.getString(R.string.txt_to_login_negative_button)){ dialog, _ ->
            dialog.cancel()
        }
    }.show()
}

/**
 * Success Dialog Functions
 */
fun showChangePasswordReply(from: Context, onPositiveClicked: (DialogInterface) -> Unit){
    MaterialAlertDialogBuilder(from).apply {
        setTitle(R.string.txt_request_change_pass_title)
        setMessage(R.string.txt_request_change_pass_message)
        setCancelable(true)
        setPositiveButton(R.string.general_positive_button){ dialog, _ -> onPositiveClicked(dialog) }
    }.show()
}

fun showGPSRequireDialog(from: Context, confirmFunction: ()-> Unit) {
    MaterialAlertDialogBuilder(from).apply {
        setTitle (R.string.location_error_title)
        setMessage(R.string.gps_require_message)
        setIcon(R.drawable.ic_wondering_emoji)
        setPositiveButton(R.string.general_positive_button){ _, _ -> confirmFunction() }
        setNegativeButton(R.string.txt_to_login_negative_button){ dialog, _ -> dialog.cancel() }
    }.show()
}


fun showGetLocationErrorDialog(from: Context) {
    MaterialAlertDialogBuilder(from).apply {
        setTitle (R.string.error_title)
        setMessage(R.string.location_error_message)
        setIcon(R.drawable.ic_wondering_emoji)
        setPositiveButton(R.string.general_positive_button){ _, _ ->  }
        //setNegativeButton(R.string.txt_to_login_negative_button){ dialog, _ -> dialog.cancel() }
    }.show()
}