package com.anguyen.urnecessaryinformation.commons

import android.app.Activity
import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.WindowInsets
import android.view.WindowMetrics
import android.view.inputmethod.InputMethodManager
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.anguyen.urnecessaryinformation.R
import com.leo.simplearcloader.ArcConfiguration
import com.leo.simplearcloader.SimpleArcDialog
import com.leo.simplearcloader.SimpleArcLoader

inline fun ComponentActivity.backPressRegister(from: LifecycleOwner, crossinline callbackHandler: () -> Unit) {
    return onBackPressedDispatcher.addCallback(from, object : OnBackPressedCallback(true){
        override fun handleOnBackPressed() {
            callbackHandler()
        }
    })
}

data class Screen (
    var width: Int = 0,
    var height: Int = 0
)


fun Activity.metrics(): Screen {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        val windowMetrics: WindowMetrics = windowManager.currentWindowMetrics
        val insets = windowMetrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
        Screen(windowMetrics.bounds.width() - insets.left - insets.right,
            windowMetrics.bounds.height() - insets.top - insets.bottom)
    } else {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        Screen(displayMetrics.widthPixels, displayMetrics.heightPixels)
    }
}

fun Activity.openKeyBoard() {
    getSystemService(InputMethodManager::class.java).showSoftInput(currentFocus, InputMethodManager.SHOW_IMPLICIT)
}

fun Activity.closeKeyBoard() {
    getSystemService(InputMethodManager::class.java).hideSoftInputFromWindow(currentFocus?.windowToken, 0)
}

fun ComponentActivity.backPressed() = onBackPressedDispatcher.onBackPressed()

fun Context.initLoadingDialog() = SimpleArcDialog(this).apply {

    setConfiguration(ArcConfiguration(this@initLoadingDialog).apply {
        loaderStyle = SimpleArcLoader.STYLE.COMPLETE_ARC
        animationSpeed = SimpleArcLoader.SPEED_FAST
        text = getString(R.string.app_loader_dialog_title)
        colors = intArrayOf(getColor(R.color.colorGeneral), getColor(R.color.colorPurple))
        setCancelable(false)
    })
}

fun Context.getColorById(id: Int) = ContextCompat.getColor(this, id)