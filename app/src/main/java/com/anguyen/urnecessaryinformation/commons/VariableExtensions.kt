package com.anguyen.urnecessaryinformation.commons

import android.content.res.Resources
import java.util.*

fun String.phoneNumberHide(): String?{
    var index = 0
    var result = ""
    return if(isPhoneNumberValid(this)){ forEach {
        if(index in 2..7) { result += "x" } else { result += it }
        ++index
    }
        result
    }else{ null }
}

fun String.upperCaseFirstWord(): String{
    return if(isEmpty()) "" else "${substring(0, 1).toUpperCase(Locale.ROOT)}${substring(1, length)}"
}

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).toInt()

//fun Int.toDp() = (this / Resources.getSystem().displayMetrics.density).toInt()