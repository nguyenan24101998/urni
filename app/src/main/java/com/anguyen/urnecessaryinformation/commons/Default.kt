package com.anguyen.urnecessaryinformation.commons
import com.anguyen.urnecessaryinformation.BuildConfig

object Default {
    const val GUIDE_APP_URL = "https://drive.google.com/file/d/1J7R-9qpyZe0qwspzpRlsJbhJRS2wVuAn/view?fbclid=IwAR1QU0w0SylTDSGDBeFniT4-UMxUTy1WnO72Liua2gnpvRnsTfN_6JFj1kw"
    const val ABOUT_APP_URL = "https://anguyen24101998.wixsite.com/website"
    //BackendLess
    const val BEL_APP_ID = BuildConfig.BEL_APP_ID
    const val BEL_REST_KEY = BuildConfig.BEL_REST_KEY
    //News API key
    const val NEWS_API_KEY = BuildConfig.NEWS_API_KEY
    //const val NEWS_API_KEY = BuildConfig.NEWS_API_KEY
    const val REMOVED_STATUS = "[Removed]"
    //Weather API key
    const val WEATHER_API_KEY = BuildConfig.WEATHER_API_KEY
    //Google
    //const val GOOGLE_API_KEY = BuildConfig.GOOGLE_API_KEY
    //Date format
    const val VN_DATE_FORMAT = "EE dd/MM HH:mm"
    const val VN_DATE_FORMAT2 = "EE"
    const val VN_TIME_FORMAT = "HH:mm"
    const val STR_TODAY = "Hôm nay"
    //Share preference
    const val SHARE_PREF = "SHARE_PREF"
    const val LAST_LOGIN_ACCOUNT = "last-login-user"
    const val LAST_UNIT_SELECTION = "temp-unit"
    //User type
    const val APP_ACCOUNT_TYPE = "BACKENDLESS"
    const val FACEBOOK_ACCOUNT_TYPE = "FACEBOOK"
    const val GOOGLE_ACCOUNT_TYPE = "GOOGLE_PLUS"
    //Intent result code
    const val CODE_PICK_IMG_PERMISSION = 1020
    const val CODE_PICK_FROM_CAMERA = 10001
    const val CODE_PICK_FROM_GALLERY = 10002
    const val CODE_REQUEST_ACCESS_LOCATION = 10003
    const val CODE_GOOGLE_SIGN_IN_RESULT = 10004
    const val GOOGLE_PACKAGE_URL = "com.google.android.apps.maps"

    //Http code
    const val CODE_SUCCESSFUL = 200
    const val BEL_CODE_EMAIL_CONFLICT = 1155
    //Weather
    //const val icon = "http://openweathermap.org/img/wn/50n@2x.png"
    const val STR_TEMP_UNIT = "°"
    const val STR_WIND_SPEED_UNIT = "m/s"
    const val STR_PRESSURE_UNIT = "hpa"
    const val STR_PERCENT_UNIT = "%"
    const val STR_C_UNIT = "metric"
    const val STR_F_UNIT = "imperial"
}