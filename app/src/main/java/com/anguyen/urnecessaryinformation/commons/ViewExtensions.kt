package com.anguyen.urnecessaryinformation.commons

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.anguyen.urnecessaryinformation.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.radiobutton.MaterialRadioButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.kyleduo.switchbutton.SwitchButton
import kotlin.math.abs

inline fun View.onClick(crossinline onClickHandler: () -> Unit){
    setOnClickListener { onClickHandler() }
}

inline fun View.insetListener(crossinline  handler: (view: View, windowInsets: WindowInsetsCompat) -> Unit){
    ViewCompat.setOnApplyWindowInsetsListener(this) { view, windowInsets ->
        handler(view, windowInsets)
        WindowInsetsCompat.CONSUMED
    }
}

fun View.setUndoSnackBar(from: Context, text: String, actionHandler: () -> Unit,
                         dismissCallbackHandler: () -> Unit): Snackbar {
    //Undo check
    var isUndo = false

    return Snackbar.make(this, "${R.string.txt_delete_snackbar_message} $text",
        Snackbar.LENGTH_LONG).apply {

        setAction(R.string.txt_undo) {
            actionHandler()
            isUndo = true
        }
        setActionTextColor(ContextCompat.getColor(from, R.color.colorGeneral2))

        (from as Activity).setFinishOnTouchOutside(true)

        show()

        //Remove from database after undo bar dismiss
        setOnDismissed { if(!isUndo){ dismissCallbackHandler() } }
    }
}

fun View.rootView(): View? {
    var root: View? = parent as View
    var x = root?.left
    var y = root?.top

    while (root != null) {
        x = x?.plus(root.left)
        y = y?.plus(root.top)
        root = root.parent as View
    }

    return root
}

//inline fun View.onDragFromLeftToRight(crossinline onTouchHandler: () -> Unit){
//    setOnTouchListener { view, motionEvent ->
//        performClick()
//
//        return@setOnTouchListener true
//    }
//}

inline fun RadioButton.onChecked(crossinline onCheckedHandler: (checkBox: MaterialRadioButton)  -> Unit){
    setOnCheckedChangeListener { compoundButton, _ ->  onCheckedHandler(compoundButton as MaterialRadioButton) }
}

inline fun SwitchButton.onSwitched(crossinline onCheckedHandler: (Boolean)  -> Unit){
    setOnCheckedChangeListener { _, isChecked ->  onCheckedHandler(isChecked) }
}

inline fun EditText.onTextChanged(crossinline onTextChangeHandler: (String) -> Unit){
    addTextChangedListener(object : TextWatcher{
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChangeHandler(s?.toString() ?: "")
        }
    })
}

inline fun EditText.onFocusChange(crossinline onFocusChangeListener: (View, Boolean) -> Unit){
    setOnFocusChangeListener { view, b ->
        onFocusChangeListener(view, b)
    }
}

fun TextInputLayout.setError(from: Context, isError: Boolean, vararg strResource: Int){
    if(isError){
        if(strResource.isNotEmpty()){
            helperText = from.getString(strResource[0])
        }
        setHelperTextColor(from.getColorStateList(R.color.colorFireBrick))
        setBoxStrokeColorStateList(from.getColorStateList(R.color.colorFireBrick))

    }else{
        setBoxStrokeColorStateList(from.getColorStateList(R.color.colorGeneral))
    }

    isHelperTextEnabled = isError

}

inline fun SwipeRefreshLayout.onRefresh(crossinline onSwipeRefreshHandler: () -> Unit){
    setOnRefreshListener {
        Handler(Looper.getMainLooper()).postDelayed({
            onSwipeRefreshHandler()
        }, 1000)
    }
}

fun ImageView.setTint(from: Context, colorId: Int){
    imageTintList = ColorStateList.valueOf(ContextCompat.getColor(from, colorId))
}

inline fun Snackbar.setOnDismissed(crossinline dismissCallbackHandler: () -> Unit) {
    val dismissEvents = arrayOf(
        Snackbar.Callback.DISMISS_EVENT_SWIPE,
        Snackbar.Callback.DISMISS_EVENT_ACTION,
        Snackbar.Callback.DISMISS_EVENT_TIMEOUT,
        Snackbar.Callback.DISMISS_EVENT_MANUAL,
        Snackbar.Callback.DISMISS_EVENT_CONSECUTIVE
    )
    addCallback(object: Snackbar.Callback(){
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            if(dismissEvents.any { event == it }){
                dismissCallbackHandler()
            }
        }
    })
}

inline fun SearchView.onQueryRequest(crossinline onQueryTextChange: (String?) -> Unit,
                                     crossinline onQueryTextSubmit: (String?) -> Unit){
    setOnQueryTextListener( object : SearchView.OnQueryTextListener{

        override fun onQueryTextSubmit(query: String?): Boolean {
            onQueryTextSubmit(query)
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            onQueryTextChange(newText)
            return false
        }

    })
}

inline fun BottomAppBar.onItemClick(crossinline onItemClickHandler: (Int) -> Unit){
    setOnMenuItemClickListener{
        onItemClickHandler(it.itemId)
        return@setOnMenuItemClickListener true
    }
}

inline fun AppBarLayout.onOffSetChanged(crossinline onOffSetChangedHandler: (AppBarLayout, Int) -> Unit){
    addOnOffsetChangedListener (AppBarLayout.OnOffsetChangedListener {appBarLayout, verticalOffset ->
        onOffSetChangedHandler(appBarLayout, verticalOffset)
    })
}

fun AppBarLayout.hideWhenCollapsed(hiddenView: View) {
    onOffSetChanged { appBarLayout, verticalOffset ->
        if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0){
            hiddenView.visibility = View.INVISIBLE
        } else {
            hiddenView.visibility = View.VISIBLE
        }
    }
}

inline fun MaterialToolbar.onNavigationClick(crossinline  onClickHandler: () -> Unit) {
    setNavigationOnClickListener { onClickHandler() }
}

fun RecyclerView.verticalWithDivider(from: Context) {
    val divider = DividerItemDecoration(from, DividerItemDecoration.VERTICAL)
    divider.setDrawable(ContextCompat.getDrawable(from, R.drawable.line_devider)!!)

    this.apply {
        layoutManager = LinearLayoutManager(from, RecyclerView.VERTICAL, false)
        addItemDecoration(divider)
        itemAnimator = DefaultItemAnimator()
    }
}

fun RecyclerView.vertical(from: Context) {
    layoutManager = LinearLayoutManager(from, RecyclerView.VERTICAL, false)
}

fun RecyclerView.horizontal(from: Context) {
    layoutManager = LinearLayoutManager(from, RecyclerView.HORIZONTAL, false)
}

//inline fun EditText.onFocusChanged(crossinline onFocusChangeHandler: (modeId: Int) -> Unit){
//    val resizeMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
//    val panMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
//
//    setOnFocusChangeListener{_, hasFocus ->
//        if(hasFocus) {
//            onFocusChangeHandler(resizeMode)
//        } else {
//            onFocusChangeHandler(panMode)
//        }
//    }
//}
//
//inline fun BottomNavigationView.onItemSelected(crossinline onItemSelectedHandler
//                                               : (menuItem: MenuItem) -> Unit){
//    setOnNavigationItemSelectedListener{
//        onItemSelectedHandler(it)
//        return@setOnNavigationItemSelectedListener true
//    }
//}
//
//inline fun RadioGroup.onCheckedChangeListener(crossinline onItemClickHandler: () -> Unit){
//    setOnCheckedChangeListener{ _, _ ->  onItemClickHandler()}
//}
//
//fun Fragment.setup(from: FragmentActivity, id: Int, bundle: Bundle) {
//    this.arguments = bundle
//    from.supportFragmentManager
//        .beginTransaction()
//        .replace(id, this)
//        .commit()
//}
//
//fun AppCompatActivity.backToPrevious() {
//    if(supportFragmentManager.backStackEntryCount > 0){
//        supportFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
//    }
//}
//
//inline fun Toolbar.onItemClick(crossinline onItemClickHandler: (menuItem: MenuItem) -> Unit){
//    setOnMenuItemClickListener {
//        onItemClickHandler (it)
//        true
//    }
//}
