package com.anguyen.urnecessaryinformation.commons

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.util.Log
import java.util.*

private fun Location.getArea(from: Context): Address? {
    val geoCoder = Geocoder(from, Locale.getDefault())
    // Here 1 represent max location result to returned, by documents it recommended 1 to 5
    return geoCoder.getFromLocation(latitude, longitude, 1)?.get(0)
}
fun Location.getSubAdminArea(from: Context)= getArea(from)?.subAdminArea

fun Location.getSubAndCityArea(from: Context): String? {
    val area = getArea(from)
    return if(area != null) "${area.subAdminArea}, ${area.adminArea}" else null
}