package com.anguyen.urnecessaryinformation.commons

import android.content.Context
import android.widget.Toast

fun showLongToast(from: Context, messageId: Int) = Toast.makeText(from
    , from.getString(messageId)
    , Toast.LENGTH_LONG).show()

//fun showLongToast(from: Context, messageId: String) = Toast.makeText(from
//    , messageId, Toast.LENGTH_LONG).show()

fun showShortToast(from: Context, messageId: Int) = Toast.makeText(from
    , from.getString(messageId)
    , Toast.LENGTH_SHORT).show()