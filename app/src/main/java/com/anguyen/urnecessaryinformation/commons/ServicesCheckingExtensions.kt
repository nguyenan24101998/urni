package com.anguyen.urnecessaryinformation.commons

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.anguyen.urnecessaryinformation.commons.Default.CODE_PICK_IMG_PERMISSION
import com.anguyen.urnecessaryinformation.commons.Default.CODE_REQUEST_ACCESS_LOCATION


fun isGPSEnabled(from: Context): Boolean {
    val mLocationManager = from.getSystemService(Context.LOCATION_SERVICE)
            as LocationManager
    return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
            mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
}
/**
 * Request location permission, so that we can get the location of the
 * device. The result of the permission request is handled by a callback,
 * onRequestPermissionsResult.
 */
fun isAccessFineLocationGranted(from: Context): Boolean{
    return ContextCompat.checkSelfPermission(
        from,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}

fun isAccessCoarseLocationGranted(from: Context): Boolean{
    return ContextCompat.checkSelfPermission(
        from,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}

fun isLocationPermissionGranted(from: Context): Boolean {
    return (isAccessFineLocationGranted(from) and (isAccessCoarseLocationGranted(from)))
}

fun getLocationPermissionGranted(from: Context, permissionGrantedFunc:() -> Unit){
    if(!isLocationPermissionGranted(from)) {
        ActivityCompat.requestPermissions(
            from as Activity, arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
            ), CODE_REQUEST_ACCESS_LOCATION
        )
    }else{
        permissionGrantedFunc()
    }
}

fun isNetworkConnected(from: Context): Boolean{
    (from.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).apply {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getNetworkCapabilities(activeNetwork)?.run {
                when{
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            } ?: false
        } else {
            activeNetworkInfo != null && activeNetworkInfo!!.isConnected
        }
    }
}
fun isPermissionGranted(from: Context, vararg permission: String): Boolean{
    return permission.all {
        ContextCompat.checkSelfPermission(from, it) == PackageManager.PERMISSION_GRANTED
    }
}

fun checkPickImagePermissionGranted(from: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (isPermissionGranted(
                from,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
            )) {
            true
        } else {
            //Log.v(TAG, "Permission is revoked")
            ActivityCompat.requestPermissions(
                from as Activity,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ),
                CODE_PICK_IMG_PERMISSION
            )
            false
        }
    } else {
        true //permission is automatically granted on sdk<23 upon installation
    }
}
