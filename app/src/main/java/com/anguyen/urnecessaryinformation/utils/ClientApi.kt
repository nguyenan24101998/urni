package com.anguyen.urnecessaryinformation.utils

import com.anguyen.urnecessaryinformation.repositories.backendless.users.UserService
import com.anguyen.urnecessaryinformation.repositories.openweather.OpenWeatherService
import com.anguyen.urnecessaryinformation.repositories.backendless.articles.ArticlesService
import com.anguyen.urnecessaryinformation.repositories.news.NewsService

class ClientApi: BaseApi() {
    fun BELUserService() = this.getService(UserService::class.java, ConfigApi.BEL_BASE_URL)
    fun BELArticleService() = this.getService(ArticlesService::class.java, ConfigApi.BEL_BASE_URL)

    fun OpenWeatherService() = this.getService(OpenWeatherService::class.java, ConfigApi.OPEN_WEATHER_BASE_URL)
    fun NewsService() = this.getService(NewsService::class.java, ConfigApi.NEWS_BASE_URL)
}