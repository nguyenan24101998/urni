package com.anguyen.urnecessaryinformation.utils

class ConfigApi {

    companion object{
        const val BEL_BASE_URL = "https://api.backendless.com/"
            const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val NEWS_BASE_URL = "https://newsapi.org/v2/"
    }

    interface BackendLessApi{
        companion object {
            //User
            const val REGISTER = "{application-id}/{REST-api-key}/users/register"
            const val REGISTER_GUEST = "{application-id}/{REST-api-key}/users/register/guest"
            const val LOGIN = "{application-id}/{REST-api-key}/users/login"
            const val LOGIN_BY_FACEBOOK = "{application-id}/{REST-api-key}/users/social/facebook/sdk/login"
            const val USER_BY_TYPE = "{application-id}/{REST-api-key}/data/users"
            const val LOGOUT = "{application-id}/{REST-api-key}/users/logout"
            const val USER_DATA = "{application-id}/{REST-api-key}/data/users/{ownerId}"
            const val VALIDATING_TOKEN = "{application-id}/{REST-api-key}/users/isvalidusertoken/{token}"
            const val UPLOAD_FILE = "{application-id}/{REST-api-key}/files/{path}/{filename}?overwrite=true"
            const val UPDATE_PROFILE = "{application-id}/{REST-api-key}/users/{userId}"
            const val CHANGE_PASSWORD = "{application-id}/{REST-api-key}/users/restorepassword/{phoneNumber}"
            const val DELETE_USER = "{application-id}/{REST-api-key}/data/bulk/Users"
            //Article
            const val ARTICLE_COUNT = "{application-id}/{REST-api-key}/data/article/count?"
            const val ARTICLE_WITH_COMMENT = "{application-id}/{REST-api-key}/data/article"
            const val COMMENTS = "{application-id}/{REST-api-key}/data/comment"
            const val DELETE_COMMENTS = "{application-id}/{REST-api-key}/data/bulk/comment"
        }
    }

    interface OpenWeatherApi{
        companion object{
            //const val LOCATION_KEY = "locations/v1/cities/geoposition/search?apikey=gmIiV1eVtAGQr0f5fO5XPA34lVx1vnA4&?q={latLng}&details=true"
            const val ONE_CALL_WEATHER= "onecall?exclude=minutely&lang=vi"
        }
    }

    interface NewsApi{
        companion object{
            const val ARTICLES = "everything?language=vi"
            const val SOURCES = "top-headlines/sources?"
        }
    }

}