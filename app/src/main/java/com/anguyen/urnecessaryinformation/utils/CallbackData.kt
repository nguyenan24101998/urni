package com.anguyen.urnecessaryinformation.utils

interface CallbackData<T>{
    fun onSuccess(data: T?)
    fun onFailure (message: String?)
}