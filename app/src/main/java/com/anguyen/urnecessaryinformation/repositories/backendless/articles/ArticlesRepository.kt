package com.anguyen.urnecessaryinformation.repositories.backendless.articles

import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.Comment
import com.anguyen.urnecessaryinformation.utils.CallbackData

interface ArticlesRepository {
    fun checkIfArticleExist(userToken: String?, url: String?, callBackData: CallbackData<Boolean>)

    fun addNewArticle(userToken: String?, article: Article, callBackData: CallbackData<Article>)

    fun addCommentToArticle(userToken: String?, url:String?, comment: Comment, callBackData: CallbackData<Comment>)

    fun getAllComments(articleId: String?, callBackData: CallbackData<List<Comment>>)

    fun getUsersComments(userId: String?, callBackData: CallbackData<List<Article>>)

    fun deleteUsersComments(userToken: String?, userId: String?, articleId: String?, callBackData: CallbackData<Int>)

    fun clearUsersComments(userToken: String?, userId: String?, callBackData: CallbackData<Int>)
    //fun getArticles(urls: List<String>, callBackData: CallbackData<Article>)
}