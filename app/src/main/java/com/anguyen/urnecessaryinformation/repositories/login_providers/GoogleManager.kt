package com.anguyen.urnecessaryinformation.repositories.login_providers

import com.anguyen.urnecessaryinformation.models.User
import com.backendless.BackendlessUser
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient

interface GoogleManager {
    fun buildGoogleApiClient(): GoogleSignInClient
    fun revokeAccount()
}