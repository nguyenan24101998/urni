package com.anguyen.urnecessaryinformation.repositories.news

import com.anguyen.urnecessaryinformation.models.Article
import com.anguyen.urnecessaryinformation.models.Source
import com.anguyen.urnecessaryinformation.utils.CallbackData

interface NewsRepository {
    fun getArticles(cateIndex: Int, callBackData: CallbackData<List<Article>>)
    fun getSources(callBackData: CallbackData<List<Source>>)
}