package com.anguyen.urnecessaryinformation.repositories.backendless.users

import com.anguyen.urnecessaryinformation.models.RespondData
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

data class FacebookProfilePicRespond(
    @SerializedName("url")
    var url: String = ""
){
    companion object{
        fun getUrl(string: String): String {
            var data: FacebookProfilePicRespond?  = null
            try{
                val type = object: TypeToken<RespondData<FacebookProfilePicRespond>>(){ }.type
                val respondData = Gson().fromJson<RespondData<FacebookProfilePicRespond>>(string, type)
                data = respondData.data
            }catch (ex: Exception){
                ex.printStackTrace()
            }
            return data?.url ?: "https://backendlessappcontent.com/8B556215-5D7B-5C16-FFCD-0FB88C400700/console/begbmydrbexsnoqbhpesxzmheganweytjdqb/files/view/defaultUser.png"
        }
    }
}