package com.anguyen.urnecessaryinformation.repositories.login_providers

import android.content.Intent
import com.anguyen.urnecessaryinformation.utils.CallbackData

interface SignInCallbackResult<T>: CallbackData<T> {
    fun onSignInCanceled()
    fun onEmailConflict()
}