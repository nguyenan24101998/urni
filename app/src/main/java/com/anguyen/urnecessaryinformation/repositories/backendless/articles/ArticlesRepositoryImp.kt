package com.anguyen.urnecessaryinformation.repositories.backendless.articles

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default.BEL_APP_ID
import com.anguyen.urnecessaryinformation.commons.Default.BEL_REST_KEY
import com.anguyen.urnecessaryinformation.commons.Default.CODE_SUCCESSFUL
import com.anguyen.urnecessaryinformation.models.*
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.utils.ClientApi
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticlesRepositoryImp(private val mContext: Context): ArticlesRepository {

    override fun checkIfArticleExist(
        userToken: String?,
        url: String?,
        callBackData: CallbackData<Boolean>
    ) {
        val clientApi = ClientApi()
        val whereClause = "url='$url'"
        val bodyCall = clientApi.BELArticleService()?.getArticlesCount(
            userToken, BEL_APP_ID, BEL_REST_KEY, whereClause
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("checkIfArticleExist", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("checkIfArticleExist", response.body()!!.string())
                        val result = response.body()!!.string()
                        val countResult: Int? = Gson().fromJson(result, Int::class.java)

                        if(countResult != null){
                            if(countResult > 0){
                                callBackData.onSuccess(true)
                            }else{
                                callBackData.onSuccess(false)
                            }
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("checkIfArticleExist", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })

    }

    override fun addCommentToArticle(
        userToken: String?,
        url: String?,
        comment: Comment,
        callBackData: CallbackData<Comment>
    ) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        try {
            jsonObject.put("content", comment.content)
            jsonObject.put("ownerId", comment.creatorId)
            jsonObject.put("articleId", comment.articleId)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELArticleService()?.addComment(
            userToken, BEL_APP_ID, BEL_REST_KEY, body
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("addCommentToArticle", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("SignUp", response.body()!!.string())
                        val result = response.body()!!.string()
                        val commentData = Gson().fromJson(result, Comment::class.java)

                        if(commentData != null){
                            callBackData.onSuccess(commentData)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("addCommentToArticle", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })

    }


    override fun addNewArticle(
        userToken: String?,
        article: Article,
        callBackData: CallbackData<Article>
    ) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        try {
            jsonObject.put("url", article.url)
            jsonObject.put("sourceName",article.source?.name)
            jsonObject.put("author", article.author)
            jsonObject.put("title", article.title)
            jsonObject.put("description", article.description)
            jsonObject.put("urlToImage", article.image)
            jsonObject.put("publishedAt", article.publishedAt)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELArticleService()?.addNewArticle(
            userToken, BEL_APP_ID, BEL_REST_KEY, body
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("addNewArticle", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("addNewArticle", response.body()!!.string())
                        val result = response.body()!!.string()
                        val articleData = Gson().fromJson(result, Article::class.java)

                        if(articleData != null){
                            callBackData.onSuccess(articleData)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("addNewArticle", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun getAllComments(articleId: String?, callBackData: CallbackData<List<Comment>>) {
        val clientApi = ClientApi()

        val whereClause = "articleId='$articleId'"
        val sortClause = "created DESC"

        val queryMap = mapOf<String?, String?>(Pair("where", whereClause), Pair("sortBy", sortClause))
        val bodyCall = clientApi.BELArticleService()?.getAllCommentsOf(
            BEL_APP_ID, BEL_REST_KEY, queryMap
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("getAllComments", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("getAllComments", response.body()!!.string())
                        val result = response.body()!!.string()
                        val type = object: TypeToken<List<Comment>>(){ }.type
                        val comments = Gson().fromJson<List<Comment>>(result, type)

                        if(comments != null){
                            callBackData.onSuccess(comments)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("getAllComments", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun getUsersComments(userId: String?, callBackData: CallbackData<List<Article>>) {
        val clientApi = ClientApi()
        val whereClause = "url in (comment[ownerId = '$userId'].articleId)"
        val bodyCall = clientApi.BELArticleService()?.getUsersCommentedArticles(
            BEL_APP_ID, BEL_REST_KEY, whereClause
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("getUsersComments", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("getUsersComments", response.body()!!.string())
                        val result = response.body()!!.string()
                        val type = object: TypeToken<List<Article>>(){ }.type
                        val articles = Gson().fromJson<List<Article>>(result, type)

                        if(articles != null){
                            callBackData.onSuccess(articles)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("getUsersComments", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun deleteUsersComments(
        userToken: String?,
        userId: String?,
        articleId: String?,
        callBackData: CallbackData<Int>
    ) {
        val clientApi = ClientApi()
        val whereClause = "articleId='$articleId' AND ownerId='$userId'"
        val bodyCall = clientApi.BELArticleService()?.deleteUserComments(
            userToken, BEL_APP_ID, BEL_REST_KEY, whereClause
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("deleteUsersComments", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("deleteUsersComments", response.body()!!.string())
                        val result = response.body()!!.string()
                        val deletedResult = Gson().fromJson(result, Int::class.java)

                        if(deletedResult != null){
                            callBackData.onSuccess(deletedResult)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("deleteUsersComments", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun clearUsersComments(
        userToken: String?,
        userId: String?,
        callBackData: CallbackData<Int>
    ) {
        val clientApi = ClientApi()
        val whereClause = "ownerId='$userId'"
        val bodyCall = clientApi.BELArticleService()?.deleteUserComments(
            userToken, BEL_APP_ID, BEL_REST_KEY, whereClause
        )

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("clearUsersComments", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("clearUsersComments", response.body()!!.string())
                        val result = response.body()!!.string()
                        val clearResult = Gson().fromJson(result, Int::class.java)

                        if(clearResult != null){
                            callBackData.onSuccess(clearResult)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("clearUsersComments", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

}