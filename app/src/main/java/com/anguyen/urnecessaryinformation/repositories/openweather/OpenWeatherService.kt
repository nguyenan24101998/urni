package com.anguyen.urnecessaryinformation.repositories.openweather

import com.anguyen.urnecessaryinformation.utils.ConfigApi
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface OpenWeatherService {

//    @GET(ConfigApi.OpenWeatherApi.LOCATION_KEY)
//    fun getLocationKey(@Query("latLng") strLatLng: String): Call<ResponseBody>

    @GET(ConfigApi.OpenWeatherApi.ONE_CALL_WEATHER)
    fun currentWeather(@QueryMap stringQueries: Map<String, String>,
                       @QueryMap coordinate: Map<String, Double>): Call<ResponseBody>

}