package com.anguyen.urnecessaryinformation.repositories.backendless.articles

import com.anguyen.urnecessaryinformation.utils.ConfigApi
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ArticlesService {
    @GET(ConfigApi.BackendLessApi.ARTICLE_COUNT)
    fun getArticlesCount(@Header("user-token") userToken: String?,
                         @Path("application-id") appId: String,
                         @Path("REST-api-key") restKey: String,
                         @Query("where") whereClause: String?): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.ARTICLE_WITH_COMMENT)
    fun addNewArticle(@Header("user-token") userToken: String?,
                      @Path("application-id") appId: String,
                      @Path("REST-api-key") restKey: String,
                      @Body requestBody: RequestBody): Call<ResponseBody>

//    @GET(ConfigApi.BackendLessApi.ARTICLE_WITH_COMMENT)
//    fun getArticles(@Query("where") whereClause: String?): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.COMMENTS)
    fun addComment(@Header("user-token") userToken: String?,
                   @Path("application-id") appId: String,
                   @Path("REST-api-key") restKey: String,
                   @Body requestBody: RequestBody): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.COMMENTS)
    fun getAllCommentsOf(@Path("application-id") appId: String,
                         @Path("REST-api-key") restKey: String,
                         @QueryMap queries: Map<String?, String?>): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.ARTICLE_WITH_COMMENT)
    fun getUsersCommentedArticles(@Path("application-id") appId: String,
                                  @Path("REST-api-key") restKey: String,
                                  @Query("where") whereClause: String?): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @DELETE(ConfigApi.BackendLessApi.DELETE_COMMENTS)
    fun deleteUserComments(@Header("user-token") userToken: String?,
                           @Path("application-id") appId: String,
                           @Path("REST-api-key") restKey: String,
                           @Query("where") whereClause: String?): Call<ResponseBody>
}