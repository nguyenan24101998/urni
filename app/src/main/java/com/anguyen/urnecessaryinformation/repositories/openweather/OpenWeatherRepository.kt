package com.anguyen.urnecessaryinformation.repositories.openweather

import android.location.Location
import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.utils.CallbackData

interface OpenWeatherRepository {
    fun currentWeather(location: Location, unit: String,
                       callBackData: CallbackData<RespondWeatherCondition>)
}