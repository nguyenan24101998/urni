package com.anguyen.urnecessaryinformation.repositories.login_providers

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.core.app.ActivityCompat.startActivityForResult
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.activities.MainActivity
import com.anguyen.urnecessaryinformation.commons.Default.BEL_CODE_EMAIL_CONFLICT
import com.anguyen.urnecessaryinformation.models.User
import com.backendless.Backendless
import com.backendless.BackendlessUser
import com.backendless.async.callback.AsyncCallback
import com.backendless.exceptions.BackendlessFault
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.UserRecoverableAuthException
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes


class GoogleManagerImp(private val mContext: Context): GoogleManager {

    private val mGoogleOptions = GoogleSignInOptions
        .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail().requestProfile().requestId()
        .requestIdToken(mContext.getString(R.string.default_web_client_id))
        .build()!!

    private val mGoogleApiClient = GoogleSignIn.getClient(mContext as Activity, mGoogleOptions)

    override fun buildGoogleApiClient() = mGoogleApiClient!!

    override fun revokeAccount(){
        mGoogleApiClient.revokeAccess()
    }
}