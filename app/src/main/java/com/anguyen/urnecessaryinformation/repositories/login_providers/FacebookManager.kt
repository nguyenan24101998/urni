package com.anguyen.urnecessaryinformation.repositories.login_providers

import com.facebook.CallbackManager
import com.facebook.login.LoginResult

interface FacebookManager {
    fun signInWithReadPermissions(from: Any, callbackManager: CallbackManager,
                                  signInCallbackResult: SignInCallbackResult<LoginResult>)
    fun revokeAccount()
}