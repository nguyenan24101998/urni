package com.anguyen.urnecessaryinformation.repositories.login_providers

import android.app.Activity
import android.util.Log
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult

class FacebookManagerImp: FacebookManager {

    private val mLoginManager = LoginManager.getInstance()

    override fun signInWithReadPermissions(from: Any,
                                           callbackManager: CallbackManager,
                                           signInCallbackResult: SignInCallbackResult<LoginResult>) {
        mLoginManager.apply {
            registerCallback(callbackManager, object: FacebookCallback<LoginResult>{
                override fun onSuccess(result: LoginResult?) {
                    Log.d("FacebookManager", result?.accessToken?.token.toString())
                    signInCallbackResult.onSuccess(result)
                }

                override fun onCancel() {
                    Log.d("FacebookManager", "canceled")
                    signInCallbackResult.onSignInCanceled()
                }

                override fun onError(error: FacebookException?) {
                    Log.d("FacebookManager", error?.localizedMessage!!)
                    signInCallbackResult.onFailure(error.message)
                }
            })

            when(from){
                is Fragment -> logInWithReadPermissions(from, arrayListOf("email", "public_profile"))
                is Activity -> logInWithReadPermissions(from, arrayListOf("email", "public_profile"))
                else -> Log.d("FacebookManager", "Wrong type")
            }

        }
    }

    override fun revokeAccount() {
        LoginManager.getInstance().logOut()
    }

}