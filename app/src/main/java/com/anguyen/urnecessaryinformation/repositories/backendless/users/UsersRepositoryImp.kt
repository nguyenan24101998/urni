package com.anguyen.urnecessaryinformation.repositories.backendless.users

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default.APP_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.commons.Default.BEL_APP_ID
import com.anguyen.urnecessaryinformation.commons.Default.BEL_CODE_EMAIL_CONFLICT
import com.anguyen.urnecessaryinformation.commons.Default.BEL_REST_KEY
import com.anguyen.urnecessaryinformation.commons.Default.CODE_SUCCESSFUL
import com.anguyen.urnecessaryinformation.commons.Default.GOOGLE_ACCOUNT_TYPE
import com.anguyen.urnecessaryinformation.models.FileUrl
import com.anguyen.urnecessaryinformation.models.ServiceErrorBody
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.utils.ClientApi
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class UsersRepositoryImp(private val mContext: Context): UsersRepository {

    companion object{
        const val CODE_AUTH_CONFLICT_ERROR = 3033
    }

    private fun signInAfterSigningUp(userData: User, onSuccess: (User?) -> Unit,
                                     onFailure: (String?) -> Unit){
        signInByUniAccount(userData.phoneNumber, userData.password, object: CallbackData<User> {
            override fun onSuccess(data: User?) {
                Log.d("loginAfterSigningUp", "successful")
                onSuccess(data)
            }

            override fun onFailure(message: String?) {
                Log.d("loginAfterSigningUp", "failure")
                onFailure(message)
            }
        } )
    }

    private fun updateFacebookAvatarUrlAfterSignIn(userData: User){
        updateAvatarUrl(userData.userToken,
            userData.userId, userData.avatarUrl,
            object: CallbackData<User>{
                override fun onSuccess(data: User?) {
                    Log.d("updateFbAvatarUrl", data?.avatarUrl!!)
                }

                override fun onFailure(message: String?) {
                    Log.d("updateFbAvatarUrl", message!!)
                }
            })
    }

    override fun registerAccount(user: User, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        try {
            jsonObject.put("fullName", user.fullName)
            jsonObject.put("username", user.username)
            jsonObject.put("phoneNumber", user.phoneNumber)
            jsonObject.put("email", user.email)
            jsonObject.put("password", user.password)
            jsonObject.put("gender", user.gender)
            jsonObject.put("type", APP_ACCOUNT_TYPE)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELUserService()?.register(BEL_APP_ID, BEL_REST_KEY, body)

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("SignUp", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("SignUp", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){  //Login after signing up successfully
                            signInAfterSigningUp(user,
                                onSuccess = { callBackData.onSuccess(it) },
                                onFailure = { callBackData.onFailure(it) }
                            )
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    try {
                        val errorBody = response.errorBody()!!.string()

                        val type = object: TypeToken<ServiceErrorBody<Int>>(){ }.type
                        val error = Gson().fromJson<ServiceErrorBody<Int>>(errorBody, type)

                        if(error != null && error.code == CODE_AUTH_CONFLICT_ERROR){
                            callBackData.onFailure(null) // Catch conflict user error
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_user))
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })

    }

    override fun createGuestAccount(callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val bodyCall = clientApi.BELUserService()?.registerGuest(BEL_APP_ID, BEL_REST_KEY)

        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("SignUpGuest", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("SignUpGuest", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){  //Login after signing up successfully
                            callBackData.onSuccess(userData)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    try {
                        val errorBody = response.errorBody()!!.string()

                        val type = object: TypeToken<ServiceErrorBody<Int>>(){ }.type
                        val error = Gson().fromJson<ServiceErrorBody<Int>>(errorBody, type)

                        if(error != null && error.code == CODE_AUTH_CONFLICT_ERROR){
                            callBackData.onFailure(null) // Catch conflict user error
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_user))
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })

    }

    override fun checkGuestAccountExisted(email: String, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val whereClause = "email='$email'"
        val bodyCall = clientApi.BELUserService()?.getGoogleUsers(BEL_APP_ID, BEL_REST_KEY, whereClause)

        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Log.d("checkGuestAccountExisted", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("checkGuestAccountExisted", response.body()!!.string())
                        val result = response.body()!!.string()
                        val type = object: TypeToken<List<User>>(){ }.type
                        val users = Gson().fromJson<List<User>>(result, type)

                        if(users != null){
                            if(users.isEmpty() || users.all { it.email != email }){
                                callBackData.onSuccess(null)
                            }else{
                                callBackData.onSuccess(users[0])
                            }
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("checkGuestAccountExisted", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun signInByUniAccount(phoneNumber: String, password: String, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        try {
            jsonObject.put("login", phoneNumber)
            jsonObject.put("password", password)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELUserService()?.loginByUniAccount(BEL_APP_ID, BEL_REST_KEY, body)
        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("SignIn", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("SignIn", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){
                            callBackData.onSuccess(userData)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("SignIn", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_user))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun signInByFacebookAccount(accessToken: String?, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val fieldsJson = JSONObject()
        val bodySon = JSONObject()

        try {
            fieldsJson.put("name", "username")
            fieldsJson.put("picture", "avatar")
            fieldsJson.put("email", "email")
            fieldsJson.put("id", "facebookId")
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        try {
            bodySon.put("accessToken", accessToken)
            bodySon.put("fieldsMapping", fieldsJson)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            bodySon.toString()
        )

        val bodyCall = clientApi.BELUserService()?.loginByFacebook(BEL_APP_ID, BEL_REST_KEY, body)
        bodyCall?.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("signInByFacebook", response.code().toString())

                if (response.code() == CODE_SUCCESSFUL) {

                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try {
                        //Log.d("signInByFacebook", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if (userData != null) {
                            userData.apply {
                                avatarUrl = if (userData.avatarUrl != null) {
                                    Log.d("fbAvatar", userData.avatarUrl)
                                    FacebookProfilePicRespond.getUrl(userData.avatarUrl)
                                } else {
                                    ""
                                }
                            }
                            callBackData.onSuccess(userData)
                            updateFacebookAvatarUrlAfterSignIn(userData)
                        } else {
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                } else {
                    try {
                        val errorBody = response.errorBody()!!.string()

                        val type = object : TypeToken<ServiceErrorBody<Int>>() {}.type
                        val error = Gson().fromJson<ServiceErrorBody<Int>>(errorBody, type)

                        if (error != null && error.code == BEL_CODE_EMAIL_CONFLICT) {
                            callBackData.onFailure(null) // Catch conflict user error
                        } else {
                            Log.d("signInByFacebook", "${error.code} - ${error.message}")
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun signOut(userToken: String, callBackData: CallbackData<Any>) {
        val clientApi = ClientApi()
        val bodyCall = clientApi.BELUserService()?.logout(userToken, BEL_APP_ID, BEL_REST_KEY)
        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("SignOut", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){
                    callBackData.onSuccess(null)
                }else{
                    if (response.errorBody() != null){
                        Log.d("SignOut", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }

        })
    }

    override fun uploadFile(
        userToken: String?,
        urlPath: String?,
        filePath: String?,
        callBackData: CallbackData<FileUrl>
    ) {
        val clientApi = ClientApi()

        //Create a file object using file path
        val file = File(filePath!!)

        // Create a request body with file and image media type
        val fileReqBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)

        // Create MultipartBody.Part using file request-body,file name and part name
        val part = MultipartBody.Part.createFormData("file", file.name, fileReqBody)!!

        val bodyCall = clientApi.BELUserService()?.uploadFile(
            userToken!!, BEL_APP_ID, BEL_REST_KEY,
            urlPath!!, file.name, part
        )
        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("uploadFile", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("uploadFile", response.body()!!.string())
                        val result = response.body()!!.string()
                        val fileUrl = Gson().fromJson(result, FileUrl::class.java)

                        if(fileUrl != null){
                            callBackData.onSuccess(fileUrl)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("uploadFile", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun getUserData(userId: String, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val bodyCall = clientApi.BELUserService()?.getUserData(BEL_APP_ID, BEL_REST_KEY, userId)
        bodyCall?.enqueue(object: Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("getCurrentUserData", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("getCurrentUserData", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){
                            callBackData.onSuccess(userData)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("getCurrentUserData", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun checkIfUserTokenValid(userToken: String, callBackData: CallbackData<Boolean>) {
        val clientApi = ClientApi()
        val bodyCall = clientApi.BELUserService()?.checkIfUserTokenValid(
            BEL_APP_ID, BEL_REST_KEY, userToken
        )
        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("checkIfUserTokenValid", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {

                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("checkIfUserTokenValid", response.body()!!.string())
                        val result = response.body()!!.string()
                        val isTokenValid = Gson().fromJson(result, Boolean::class.java)

                        if(isTokenValid != null){
                            callBackData.onSuccess(isTokenValid)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    if (response.errorBody() != null){
                        Log.d("checkIfUserTokenValid", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun updateProfile(user: User?, callBackData: CallbackData<User>) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        val token = user?.userToken!!
        val userId = user.userId

        try {
            jsonObject.put("fullName", user.fullName)
            jsonObject.put("username", user.username)
            jsonObject.put("email", user.email)
            jsonObject.put("gender", user.gender)
            jsonObject.put("avatar", user.avatarUrl)
            jsonObject.put("type", user.type)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELUserService()?.updateProfile(
            token, BEL_APP_ID, BEL_REST_KEY, userId, body
        )
        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("updateProfile", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {
                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("updateProfile", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){
                            callBackData.onSuccess(userData)
                        }else {
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }
                }else{
                    try {
                        val errorBody = response.errorBody()!!.string()

                        val type = object: TypeToken<ServiceErrorBody<Int>>(){ }.type
                        val error = Gson().fromJson<ServiceErrorBody<Int>>(errorBody, type)

                        if(error != null && error.code == BEL_CODE_EMAIL_CONFLICT){
                            callBackData.onFailure(null) // Catch conflict user error
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_user))
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun updateAvatarUrl(
        userToken: String?,
        userId: String?,
        imageUrl: String?,
        callBackData: CallbackData<User>
    ) {
        val clientApi = ClientApi()
        val jsonObject = JSONObject()

        try {
            jsonObject.put("avatar", imageUrl)
        }catch (ex: Exception){
            ex.printStackTrace()
        }

        val body = RequestBody.create(
            MediaType.parse("application/json, charset=utf-8"),
            jsonObject.toString()
        )
        val bodyCall = clientApi.BELUserService()?.updateProfile(
            userToken!!, BEL_APP_ID, BEL_REST_KEY, userId!!, body
        )
        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("changeAvatar", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {
                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    try{
                        //Log.d("changeAvatar", response.body()!!.string())
                        val result = response.body()!!.string()
                        val userData = Gson().fromJson(result, User::class.java)

                        if(userData != null){
                            callBackData.onSuccess(userData)
                        }else {
                            callBackData.onFailure(mContext.getString(R.string.error_user))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }
                }else{
                    if (response.errorBody() != null){
                        Log.d("changeAvatar", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun changePassword(phoneNumber: String?, callBackData: CallbackData<String>) {
        val clientApi = ClientApi()

        val bodyCall = clientApi.BELUserService()?.changePassword(
            BEL_APP_ID, BEL_REST_KEY, phoneNumber!!
        )
        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("changePassword", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {
                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    callBackData.onSuccess(response.body()!!.string())
                }else{
                    if (response.errorBody() != null){
                        Log.d("changePassword", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_user))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun deleteUser(userId: String, callBackData: CallbackData<Int>) {
        val clientApi = ClientApi()
        val whereClause = "objectId='$userId'"
        val bodyCall = clientApi.BELUserService()?.deleteUser(BEL_APP_ID, BEL_REST_KEY, whereClause)

        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("deleteUser", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {
                    if (response.body() == null) {
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                        return
                    }

                    callBackData.onSuccess(response.body()!!.string().toInt())
                }else{
                    if (response.errorBody() != null){
                        Log.d("deleteUser", "${response.errorBody()?.string()}")
                    }
                    callBackData.onFailure(mContext.getString(R.string.error_user))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

}