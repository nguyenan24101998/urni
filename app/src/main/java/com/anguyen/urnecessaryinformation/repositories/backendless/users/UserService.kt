package com.anguyen.urnecessaryinformation.repositories.backendless.users

import com.anguyen.urnecessaryinformation.utils.ConfigApi
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface UserService {

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.REGISTER)
    fun register(@Path("application-id") appId: String,
                 @Path("REST-api-key") restKey: String,
                 @Body requestBody: RequestBody): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.REGISTER_GUEST)
    fun registerGuest(@Path("application-id") appId: String,
                      @Path("REST-api-key") restKey: String): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.LOGIN)
    fun loginByUniAccount(@Path("application-id") appId: String,
                          @Path("REST-api-key") restKey: String,
                          @Body requestBody: RequestBody): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @POST(ConfigApi.BackendLessApi.LOGIN_BY_FACEBOOK)
    fun loginByFacebook(@Path("application-id") appId: String,
                        @Path("REST-api-key") restKey: String,
                        @Body requestBody: RequestBody): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.USER_BY_TYPE)
    fun getGoogleUsers(@Path("application-id") appId: String,
                       @Path("REST-api-key") restKey: String,
                       @Query("where") whereClause: String): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.LOGOUT)
    fun logout(@Header("user-token") userToken: String,
               @Path("application-id") appId: String,
               @Path("REST-api-key") restKey: String,): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.USER_DATA)
    fun getUserData(@Path("application-id") appId: String,
                    @Path("REST-api-key") restKey: String,
                    @Path("ownerId") userId: String): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.VALIDATING_TOKEN)
    fun checkIfUserTokenValid(@Path("application-id") appId: String,
                              @Path("REST-api-key") restKey: String,
                              @Path("token") userToken: String): Call<ResponseBody>

    @POST(ConfigApi.BackendLessApi.UPLOAD_FILE)
    @Multipart
    fun uploadFile(@Header("user-token") userToken: String,
                   @Path("application-id") appId: String,
                   @Path("REST-api-key") restKey: String,
                   @Path("path") urlPath: String,
                   @Path("filename") urlName: String, @Part file: MultipartBody.Part): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @PUT(ConfigApi.BackendLessApi.UPDATE_PROFILE)
    fun updateProfile(@Header("user-token") userToken: String,
                      @Path("application-id") appId: String,
                      @Path("REST-api-key") restKey: String,
                      @Path("userId") userId: String,
                      @Body requestBody: RequestBody): Call<ResponseBody>

    @GET(ConfigApi.BackendLessApi.CHANGE_PASSWORD)
    fun changePassword(@Path("application-id") appId: String,
                       @Path("REST-api-key") restKey: String,
                       @Path("phoneNumber") phoneNumber: String): Call<ResponseBody>

    @Headers("Content-Type:application/json")
    @DELETE(ConfigApi.BackendLessApi.DELETE_USER)
    fun deleteUser(@Path("application-id") appId: String,
                   @Path("REST-api-key") restKey: String,
                   @Query("where") whereClause: String): Call<ResponseBody>

}