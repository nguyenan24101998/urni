package com.anguyen.urnecessaryinformation.repositories.news

import com.anguyen.urnecessaryinformation.utils.ConfigApi
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface NewsService {
    @GET(ConfigApi.NewsApi.ARTICLES)
    fun getArticles(@QueryMap queries: Map<String, String>): Call<ResponseBody>

    @GET(ConfigApi.NewsApi.SOURCES)
    fun getSources(@Query("apiKey") apiKey: String): Call<ResponseBody>
}