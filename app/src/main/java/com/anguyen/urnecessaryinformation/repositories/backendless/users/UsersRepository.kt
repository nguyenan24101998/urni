package com.anguyen.urnecessaryinformation.repositories.backendless.users

import com.anguyen.urnecessaryinformation.models.FileUrl
import com.anguyen.urnecessaryinformation.models.User
import com.anguyen.urnecessaryinformation.utils.CallbackData

interface UsersRepository {
    fun registerAccount(user: User, callBackData: CallbackData<User>)
    fun createGuestAccount(callBackData: CallbackData<User>)
    fun signInByUniAccount(phoneNumber: String, password: String, callBackData: CallbackData<User>)
    fun checkGuestAccountExisted(email: String, callBackData: CallbackData<User>)
    fun signInByFacebookAccount(accessToken: String?, callBackData: CallbackData<User>)
    fun signOut(userToken: String, callBackData: CallbackData<Any>)
    fun uploadFile(userToken: String?, urlPath: String?, filePath: String?,
                   callBackData: CallbackData<FileUrl>)
    fun getUserData(userId: String, callBackData: CallbackData<User>)
    fun updateProfile(user: User?, callBackData: CallbackData<User>)
    fun updateAvatarUrl(userToken: String?, userId: String?, imageUrl: String?,
                        callBackData: CallbackData<User>)
    fun checkIfUserTokenValid(userToken: String, callBackData: CallbackData<Boolean>)
    fun changePassword(phoneNumber: String?, callBackData: CallbackData<String>)
    fun deleteUser(userId: String, callBackData: CallbackData<Int>)
}