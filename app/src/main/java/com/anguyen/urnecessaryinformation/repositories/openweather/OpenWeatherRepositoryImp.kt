package com.anguyen.urnecessaryinformation.repositories.openweather

import android.content.Context
import android.location.Location
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default.CODE_SUCCESSFUL
import com.anguyen.urnecessaryinformation.commons.Default.WEATHER_API_KEY
import com.anguyen.urnecessaryinformation.models.MyLocation
import com.anguyen.urnecessaryinformation.models.RespondWeatherCondition
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.utils.ClientApi
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OpenWeatherRepositoryImp(private val mContext: Context): OpenWeatherRepository {

    override fun currentWeather(
        location: Location,
        unit: String,
        callBackData: CallbackData<RespondWeatherCondition>
    ) {
        val clientApi = ClientApi()
        val stringQueries = mapOf(Pair("apiKey", WEATHER_API_KEY), Pair("units", unit))
        val coordinate = mapOf(Pair("lat", location.latitude), Pair("lon", location.longitude))

        val bodyCall = clientApi.OpenWeatherService()?.currentWeather(stringQueries, coordinate)
        bodyCall?.enqueue(object: Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("currentWeather", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL){

                    if(response.body() == null){
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                    try{
                        //Log.d("currentWeather", response.body()!!.string())
                        val result = response.body()!!.string()

                        val type = object: TypeToken<RespondWeatherCondition>(){ }.type
                        val condition = Gson().fromJson<RespondWeatherCondition>(result, type)

                        if(condition != null){
                            condition.location = MyLocation(location.latitude, location.longitude)
                            callBackData.onSuccess(condition)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }
}