package com.anguyen.urnecessaryinformation.repositories.news

import android.content.Context
import android.util.Log
import com.anguyen.urnecessaryinformation.R
import com.anguyen.urnecessaryinformation.commons.Default.CODE_SUCCESSFUL
import com.anguyen.urnecessaryinformation.commons.Default.NEWS_API_KEY
import com.anguyen.urnecessaryinformation.models.*
import com.anguyen.urnecessaryinformation.utils.CallbackData
import com.anguyen.urnecessaryinformation.utils.ClientApi
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepositoryImp(private val mContext: Context): NewsRepository {

    override fun getArticles(cateIndex: Int, callBackData: CallbackData<List<Article>>) {
        val clientApi = ClientApi()

        val articleCategories = mContext.resources.getStringArray(R.array.article_categories)
        val articleType = articleCategories[cateIndex]
        val queries = mapOf(Pair("apiKey", NEWS_API_KEY), Pair("q", articleType))

        val bodyCall = clientApi.NewsService()?.getArticles(queries)

        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("getArticles", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {

                    if (response.body() == null) {
                        return
                    }

                    try{
                        //Log.d("getArticles", response.body()!!.string())
                        val result = response.body()!!.string()
                        //callBackData.onSuccess(result)

                        val type = object: TypeToken<RespondNews>(){ }.type
                        val respondNews = Gson().fromJson<RespondNews>(result, type)
                        val articles = respondNews.articles

                        if(articles != null){
                            callBackData.onSuccess(articles)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                    try { //Print error message to log
                        val errorBody = response.errorBody()!!.string()
                        val type = object: TypeToken<ServiceErrorBody<String>>(){ }.type
                        val error = Gson().fromJson<ServiceErrorBody<String>>(errorBody, type)
                        Log.d("getSources", error.message)
                    }catch (ex: Exception){
                        ex.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

    override fun getSources(callBackData: CallbackData<List<Source>>) {
        val clientApi = ClientApi()
        val bodyCall = clientApi.NewsService()?.getSources(NEWS_API_KEY)

        bodyCall?.enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                Log.d("getSources", response.code().toString())

                if(response.code() == CODE_SUCCESSFUL) {

                    if (response.body() == null) {
                        return
                    }

                    try{
                        //Log.d("getSources", response.body()!!.string())
                        val result = response.body()!!.string()
                        //callBackData.onSuccess(result)

                        val type = object: TypeToken<RespondSources>(){ }.type
                        val respondData = Gson().fromJson<RespondSources>(result, type)
                        val sources = respondData.sources

                        if(sources != null){
                            callBackData.onSuccess(sources)
                        }else{
                            callBackData.onFailure(mContext.getString(R.string.error_server))
                        }
                    }catch (ex: Exception){
                        ex.printStackTrace()
                        callBackData.onFailure(mContext.getString(R.string.error_server))
                    }

                }else{
                    callBackData.onFailure(mContext.getString(R.string.error_server))
                    try { //Print error message on log
                        val errorBody = response.errorBody()!!.string()
                        val type = object: TypeToken<ServiceErrorBody<String>>(){ }.type
                        val error = Gson().fromJson<ServiceErrorBody<String>>(errorBody, type)

                        Log.d("getSources", error.message)
                    }catch (ex: Exception){
                        ex.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                callBackData.onFailure(mContext.getString(R.string.error_server))
            }
        })
    }

}